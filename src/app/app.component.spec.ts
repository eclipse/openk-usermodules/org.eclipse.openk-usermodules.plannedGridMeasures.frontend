/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { TestBed, async } from '@angular/core/testing';
import { ActivatedRoute, Router } from '@angular/router';
import { AppComponent } from './app.component';
import { SessionContext } from './common/session-context';
import { HttpResponseInterceptorService } from './services/http-response-interceptor.service';
import { BaseDataLoaderService } from './services/jobs/base-data-loader.service';
import { ReminderCallerJobService } from './services/jobs/reminder-caller-job.service';
import { AbstractMockObservableService } from './testing/abstract-mock-observable.service';
import { MockComponent } from './testing/mock.component';
import { HttpClient } from '@angular/common/http';
import { ToasterMessageService } from './services/toaster-message.service';
import { MessageService } from 'primeng/api';

class FakeRouter {
  navigate(commands: any[]) {
    return commands[0];
  }
}

class MockHttp extends AbstractMockObservableService {
  get() {
    return this;
  }
}

class MockHttpRes {
  public content: any;
  json() { return this.content; }
}

describe('AppComponent', () => {
  const mockService = new MockHttp();
  let sessionContext: SessionContext;
  let routerStub: FakeRouter;
  let messageService;
  let toasterMessageService;

  beforeEach(() => {
    messageService = new MessageService();
    sessionContext = new SessionContext();
    toasterMessageService = new ToasterMessageService(sessionContext, messageService);
    routerStub = {
      navigate: jasmine.createSpy('navigate').and.callThrough()
    };

    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        MockComponent({ selector: 'app-main-navigation' }),
        MockComponent({ selector: 'app-toaster' }),
        MockComponent({ selector: 'router-outlet' })
      ],
      providers: [
        { provide: HttpClient, useValue: mockService },
        { provide: Router, useValue: routerStub },
        { provide: BaseDataLoaderService, useValue: {} },
        { provide: ActivatedRoute },
        { provide: HttpResponseInterceptorService },
        { provide: SessionContext, useValue: sessionContext },
        { provide: ToasterMessageService, useValue: toasterMessageService },
        { provide: ReminderCallerJobService }
      ],
    });
    TestBed.compileComponents();

    const httpRes = new MockHttpRes();
    httpRes.content = {};
    mockService.content = httpRes;


  });

  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));


  it('should extract the user data from the JWT-AccessToken', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;

    const accessToken = 'eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJodVl0e' +
      'VByUEVLQ1phY3FfMW5sOGZscENETnFHdmZEZHctYUxGQXNoWHZVIn0.eyJqdGkiOiI4ZmY5NTlhZC' +
      '02ODQ1LTRlOGEtYjRiYi02ODQ0YjAwMjU0ZjgiLCJleHAiOjE1MDY2MDA0NTAsIm5iZiI6MCwiaWF' +
      '0IjoxNTA2NjAwMTUwLCJpc3MiOiJodHRwOi8vZW50amF2YTAwMjo4MDgwL2F1dGgvcmVhbG1zL2Vs' +
      'b2dib29rIiwiYXVkIjoiZWxvZ2Jvb2stYmFja2VuZCIsInN1YiI6IjM1OWVmOWM5LTc3ZGYtNGEzZ' +
      'C1hOWM5LWY5NmQ4MzdkMmQ1NyIsInR5cCI6IkJlYXJlciIsImF6cCI6ImVsb2dib29rLWJhY2tlbm' +
      'QiLCJhdXRoX3RpbWUiOjAsInNlc3Npb25fc3RhdGUiOiI5NjVmNzM1MS0yZThiLTQ1MjgtOWYzZC1' +
      'lZTYyODNhOTViMTYiLCJhY3IiOiIxIiwiYWxsb3dlZC1vcmlnaW5zIjpbIioiXSwicmVhbG1fYWNj' +
      'ZXNzIjp7InJvbGVzIjpbImVsb2dib29rLXN1cGVydXNlciIsImVsb2dib29rLW5vcm1hbHVzZXIiL' +
      'CJ1bWFfYXV0aG9yaXphdGlvbiJdfSwicmVzb3VyY2VfYWNjZXNzIjp7InJlYWxtLW1hbmFnZW1lbn' +
      'QiOnsicm9sZXMiOlsidmlldy11c2VycyIsInF1ZXJ5LWdyb3VwcyIsInF1ZXJ5LXVzZXJzIl19LCJ' +
      'hY2NvdW50Ijp7InJvbGVzIjpbIm1hbmFnZS1hY2NvdW50IiwibWFuYWdlLWFjY291bnQtbGlua3Mi' +
      'LCJ2aWV3LXByb2ZpbGUiXX19LCJuYW1lIjoiQWRtaW5pc3RyYXRvciBBZG1pbmlzdHJhdG93aWNoI' +
      'iwicHJlZmVycmVkX3VzZXJuYW1lIjoiYWRtaW4iLCJnaXZlbl9uYW1lIjoiQWRtaW5pc3RyYXRvci' +
      'IsImZhbWlseV9uYW1lIjoiQWRtaW5pc3RyYXRvd2ljaCIsImVtYWlsIjoic2VyZ2VqLmtlcm5AcHRh' +
      'LmRlIiwicm9sZXN0ZXN0IjoiW2Vsb2dib29rLXN1cGVydXNlciwgZWxvZ2Jvb2stbm9ybWFsdXNlc' +
      'iwgdW1hX2F1dGhvcml6YXRpb24sIG9mZmxpbmVfYWNjZXNzLCB1bWFfYXV0aG9yaXphdGlvbiwgZW' +
      'xvZ2Jvb2stbm9ybWFsdXNlcl0ifQ.o94Bl43oqyLNzZRABvIq9z-XI8JQjqj2FSDdUUEZGZPTN4uw' +
      'D5fyi0sONbDxmTFvgWPh_8ZhX6tlDGiupVDBY4eRH43Eettm-t4CDauL7FzB3w3dDPFMB5DhP4rrp' +
      'k_kATwnY2NKLRbequnh8Z6wLXjcmQNLgrgknXB_gogWAqH29dqKexwceMNIbq-kjaeLsmHSXM9TE9' +
      'q7_Ln9el04OlkpOVspVguedfINcNFg0DmYLJWyD2ORkOHLmYigN6YnyB9P2NFOnKGlLuQ87GjosI0' +
      '0zBniRGi3PhE9NGd51Qggdbcsm0aM8GiMaZ7SO5i8iQWL10TRFRFyTEfy6hSO8g';

    const incognito: any = app;
    incognito.processAccessToken(accessToken);

    expect(sessionContext.getCurrUser().name).toBe('Administrator Administratowich');
    expect(sessionContext.getAccessToken()).toBe(accessToken);
  }));

  it('read the param access token', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    const token = 'AJDHDND';
    const uri = 'a=X&b=y&accessToken=' + token;
    const inkognito: any = app;
    const token2 = inkognito.readParamAccessToken(uri, 'accessToken');

    expect(token2).toBe(token);
  });

  it('RC ==== 401', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    const inkognito: any = app;
    inkognito.onRcFromHttpService(401);

    expect(routerStub.navigate).toHaveBeenCalledWith(['/logout']);
  });

  it('RC !=== 401', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    const inkognito: any = app;
    inkognito.onRcFromHttpService(402);

    expect(routerStub.navigate).not.toHaveBeenCalledWith(['/logout']);
  });

  it('should go into both IFs of procesAcessToken', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;

    const accessToken = 'eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJIYlI3Z' +
      '2pobmE2eXJRZnZJTWhUSV9tY2g3ZmtTQWVFX3hLTjBhZVl0bjdjIn0.eyJqdGkiOiI1MmM1ZmMxOS' +
      '04ZGVkLTRmZDktODVmOC1kNzBkZDY1YWZlZDMiLCJleHAiOjE1MzA3MDY3NTEsIm5iZiI6MCwiaWF' +
      '0IjoxNTMwNzA2NDUxLCJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjgwODAvYXV0aC9yZWFsbXMvRWxv' +
      'Z2Jvb2siLCJhdWQiOiJlbG9nYm9vay1iYWNrZW5kIiwic3ViIjoiOWE2OGQwYmQtMDMzNS00YjNiL' +
      'WIxMjgtN2E1ZTc0N2QzZmY5IiwidHlwIjoiQmVhcmVyIiwiYXpwIjoiZWxvZ2Jvb2stYmFja2VuZC' +
      'IsImF1dGhfdGltZSI6MCwic2Vzc2lvbl9zdGF0ZSI6IjI1OWE3OTQ2LWQ0OTMtNGFmZC04YWI5LTg' +
      '1NGRhZGE3NDkxMSIsImFjciI6IjEiLCJhbGxvd2VkLW9yaWdpbnMiOlsiKiJdLCJyZWFsbV9hY2Nl' +
      'c3MiOnsicm9sZXMiOlsicGxhbm5lZC1wb2xpY2llcy1hY2Nlc3MiLCJwbGFubmVkLXBvbGljaWVzL' +
      'W5vcm1hbHVzZXIiLCJwbGFubmVkLXBvbGljaWVzLW1lYXN1cmVhcHBsaWNhbnQiXX0sInJlc291cm' +
      'NlX2FjY2VzcyI6e30sInByZWZlcnJlZF91c2VybmFtZSI6InRlc3R1c2VyMSJ9.YGDE--qV8XiRas' +
      'R7S3_QOC34gz_eMLx9MWhUHM2KcckY79YcknDQZ9rBaqjLTfw8AEbsAyTuiL8zaw17u1ge-1TVmPh' +
      'VaUgvbctb1lUnn-4ZMyX8JObYuTxMnPxZSYVQjwqgE80q15aHd_TtXG8OXNbR6941Ymoel2B-lWuo' +
      '3UJR2Aoiq-bVX4OSn1_C_4Ca4EbWSIdN85lfV1xiXKu97l8nv1MjaxuET7LrcFT4z12Qsq7EEXcFC' +
      '7KsJRVpOLEW2xBRgRuWV_wvIyIrA2vacQRvlfbOHmrltqjfAuOEZArQsLwNtunL4duDh4U8qefiXL' +
      'JFPnEN6SpCh2pjGEdgBg';

    const incognito: any = app;
    incognito.processAccessToken(accessToken);

    expect(sessionContext.getAccessToken()).toBe(accessToken);
  }));

  it('getParametersFromURL is entered', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    const inkognito: any = app;
    const param1 = inkognito.getParametersFromUrl();
    // ist das richtig? Soll das so? Was steht im window.location.search.substr(1)?
    expect(param1).toBe(null);
  });

});
