/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { async, fakeAsync, ComponentFixture, TestBed, tick } from '@angular/core/testing';
import { AbstractMockObservableService } from '../../../testing/abstract-mock-observable.service';
import { AbstractListComponent } from './abstract-list.component';
import { FormsModule } from '@angular/forms';
import { StringToDatePipe } from '../../../common-components/pipes/string-to-date.pipe';
import { FormattedDatePipe } from '../../../common-components/pipes/formatted-date.pipe';
import { FormattedTimestampPipe } from '../../../common-components/pipes/formatted-timestamp.pipe';
import { SessionContext } from '../../../common/session-context';
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import { MockComponent } from '../../../testing/mock.component';
import { GridMeasureService } from '../../../services/grid-measure.service';
import { ReminderService } from '../../../services/reminder.service';
import { LockService } from '../../../services/lock.service';
import { Router } from '@angular/router';
import { UserSettingsService } from '../../../services/user-settings.service';
import { UserSettings } from '../../../model/user-settings';
import { USERS } from '../../../test-data/users';
import { RoleAccessHelperService } from '../../../services/jobs/role-access-helper.service';
import { ModeValidator } from '../../../custom_modules/helpers/mode-validator';
import { OnInit, Component } from '../../../../../node_modules/@angular/core';
import { Globals } from './../../../common/globals';
import { GridOptions } from 'ag-grid/dist/lib/entities/gridOptions';
import { ToasterMessageService } from '../../../services/toaster-message.service';
import { MessageService } from 'primeng/api';

export class AbstractListMocker {
  public static getComponentMocks() {
    return [
      MockComponent({
        selector: 'app-abstract-list',
        inputs: ['withCheckboxes', 'withEditButtons', 'isCollapsible', 'stayCollapsedGridMeasures', 'gridId', 'enforceShowReadOnly']
      })
    ];
  }
}

class MockUserSettingService extends AbstractMockObservableService {
  savedUserSettings: UserSettings;
  getUserSettings(gridId: string) {
    return this;
  }
  setUserSettings(userSettings: UserSettings) {
    this.savedUserSettings = userSettings;
    return this;
  }
}

@Component({
  selector: 'app-abstract-list',
  templateUrl: './abstract-list.component.html',
  styleUrls: ['./abstract-list.component.css']
})
class MockConcreteListComponent extends AbstractListComponent implements OnInit {
  Globals = Globals;
  listItems: any;
  currentDate = new Date();

  initAgGrid() {
    const localGridOptions = <GridOptions>{
      columnDefs: [
        {
          headerName: 'Test1',
          field: 'test1',
          colId: 'test1',
          width: 58
        },
        {
          headerName: 'Test2',
          field: 'test1',
          colId: 'test1',
          width: 58
        }]
    };
    this.gridOptions = Object.assign(this.globalGridOptions, localGridOptions);

  }

  retrieveData() { }

  changeAllSelection() { }
}

describe('AbstractListComponent', () => {
  let component: MockConcreteListComponent;
  let fixture: ComponentFixture<MockConcreteListComponent>;

  class MockService extends AbstractMockObservableService {
    getGridMeasures() {
      return this;
    }
  }

  class MockReminderService extends AbstractMockObservableService {
    getCurrentReminders() {
      return this;
    }
  }

  let sessionContext;
  let mockService;
  let mockReminderService;
  let mockUserSettingService;
  let roleAccessHelper: RoleAccessHelperService;
  let toasterMessageService: ToasterMessageService;
  let messageService: MessageService;
  beforeEach(async(() => {
    sessionContext = new SessionContext();
    mockService = new MockService();
    mockReminderService = new MockReminderService();
    mockUserSettingService = new MockUserSettingService();
    roleAccessHelper = new RoleAccessHelperService();
    messageService = new MessageService;
    toasterMessageService = new ToasterMessageService(sessionContext, messageService);
    TestBed.configureTestingModule({
      imports: [FormsModule],
      declarations: [
        // AbstractListComponent,
        MockConcreteListComponent,
        StringToDatePipe,
        FormattedDatePipe,
        FormattedTimestampPipe,
        MockComponent({ selector: 'input', inputs: ['options', 'gridId'] })
      ],
      providers: [
        ModeValidator,
        { provide: UserSettingsService, userValue: mockService },
        { provide: GridMeasureService, useValue: mockService },
        { provide: ReminderService, useValue: mockReminderService },
        { provide: LockService, useValue: mockService },
        { provide: SessionContext, useClass: SessionContext },
        { provide: DaterangepickerConfig, useClass: DaterangepickerConfig },
        { provide: UserSettingsService, useValue: mockUserSettingService },
        { provide: Router },
        { provide: RoleAccessHelperService, useValue: roleAccessHelper },
        { provide: ToasterMessageService, useValue: toasterMessageService }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MockConcreteListComponent);
    component = fixture.componentInstance;
  });



  it('should return a new ListHelperTool', () => {
    expect(component.getListHelperTool()).toBeTruthy();
  });

  it('should init correctly error in user settings service', () => {
    spyOn(console, 'log').and.callThrough();
    spyOn(component, 'retrieveData').and.callThrough();
    mockUserSettingService.content = {};
    mockUserSettingService.error = 'Error in UserSettingsService';
    component.ngOnInit();
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(console.log).toHaveBeenCalled();
      expect(component.retrieveData).toHaveBeenCalled();
    });
  });


  it('should init correctly with empty user settings', () => {
    mockUserSettingService.content = {};
    component.ngOnInit();
  });

  it('should init correctly with non empty user settings', async(() => {
    mockUserSettingService.content = { userName: '', settingType: '', value: { sorting: 'abc', filter: 'xyz' } };
    component.ngOnInit();
    fixture.detectChanges();

    fixture.whenStable().then(() => {
      expect(component.userSettings.value.sorting).toBe('abc');
      expect(component.userSettings.value.filter).toBe('xyz');

    });
  }));


  it('should store new user settings correctly', fakeAsync(() => {
    const abstractComp: any = component; // used to access privates
    component.user = USERS[1];
    component.gridId = 'Gridd';
    component.sortingState = {
      column: 'shorty',
      counter: 1,
      defaultState: true,
      isDesc: true
    };
    component.filteringSearchText = {
      branchId: 'filty',
      title: 'fix',
      statusId: 'foxy'
    };
    mockUserSettingService.savedUserSettings = {};

    abstractComp.saveSettings();

    tick();

    expect(mockUserSettingService.savedUserSettings).toBeTruthy();
    expect(mockUserSettingService.savedUserSettings.username).toBe(USERS[1].username);

    mockUserSettingService.savedUserSettings.username = 'Brodtkamp';

    abstractComp.settingsIsDirty = true;
    abstractComp.saveSettings();

    tick();
    fixture.detectChanges();
    tick();

    expect(mockUserSettingService.savedUserSettings.username).toBe('Brodtkamp');

  }));

  it('should store new user settings correctly', async(() => {
    const abstractComp: any = component; // used to access privates
    component.user = USERS[1];
    component.gridId = 'Gridd';
    component.sortingState = {
      column: 'shorty',
      counter: 1,
      defaultState: true,
      isDesc: true
    };
    component.filteringSearchText = {
      branchId: 'filty',
      title: 'fix',
      statusId: 'foxy'
    };
    mockUserSettingService.savedUserSettings = {};

    abstractComp.saveSettings();

    fixture.whenStable().then(() => {

      expect(mockUserSettingService.savedUserSettings).toBeTruthy();
      expect(mockUserSettingService.savedUserSettings.username).toBe(USERS[1].username);

      mockUserSettingService.savedUserSettings.username = 'Brodtkamp';

      abstractComp.settingsIsDirty = true;
      abstractComp.saveSettings();

      fixture.detectChanges();
      fixture.whenStable().then(() => {

        expect(mockUserSettingService.savedUserSettings.username).toBe('Brodtkamp');

      });
    });

  }));

  class ParamsMockState {
    setFilterCalled = false;
    onFilterChangedCalled = false;
    setSortModelCalled = false;
    setColumnStateCalled = false;
    getAllColumnsCalled = false;
    autoSizeColumnsCalled = false;

  }

  function getParamsMock(mockState: ParamsMockState) {
    const param = {
      api: {

        setFilterModel: (filertext) => { mockState.setFilterCalled = true; },
        onFilterChanged: () => { mockState.onFilterChangedCalled = true; },
        setSortModel: () => { mockState.setSortModelCalled = true; }
      },
      columnApi: {
        setColumnState: (state) => { mockState.setColumnStateCalled = true; },
        getAllColumns: () => { mockState.getAllColumnsCalled = true; return []; },
        autoSizeColumns: (allColIds) => { mockState.autoSizeColumnsCalled = true; },

      }
    };
    return param;
  }

});

