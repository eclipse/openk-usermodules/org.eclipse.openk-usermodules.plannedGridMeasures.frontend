/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import {
  Component, OnDestroy, OnInit,
  Input, ChangeDetectorRef
} from '@angular/core';


import { GridMeasureService } from './../../../services/grid-measure.service';
import { ReminderService } from './../../../services/reminder.service';
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import { Router } from '@angular/router';
import { Globals } from '../../../common/globals';
import { User } from '../../../model/user';
import { SessionContext } from '../../../common/session-context';
import { ListHelperTool } from '../../../common/list-helper-tool';
import { SettingType, UserSettings, SettingValue } from '../../../model/user-settings';
import { UserSettingsService } from '../../../services/user-settings.service';
import { RoleAccess } from '../../../model/role-access';
import { RoleAccessHelperService } from '../../../services/jobs/role-access-helper.service';
import { ModeValidator } from '../../../custom_modules/helpers/mode-validator';
import { GridOptions } from 'ag-grid/dist/lib/entities/gridOptions';
import { StatusMainFilter } from '../../../model/status-main-filter';
import { RowDragEndEvent } from '../../../../../node_modules/ag-grid';
import { ToasterMessageService } from '../../../services/toaster-message.service';

@Component({
  selector: 'app-abstract-list',
  templateUrl: './abstract-list.component.html',
  styleUrls: ['./abstract-list.component.css']
})

export class AbstractListComponent implements OnInit, OnDestroy {


  userSettings: UserSettings;
  @Input() withCheckboxes = false;
  @Input() withDatePicker = true;
  @Input() withEditButtons = false;
  @Input() isCollapsible = true;
  @Input() gridId: string;

  showSpinner = true;
  globals = Globals;
  settingsIsDirty = false;
  selectAll = false;
  currentDate = new Date();
  endDate: Date;
  startDate: Date;
  minDate: Date;
  maxDate: Date;
  user: User = null;
  sortingState: any;
  columnState: any;
  subscription: any;
  filteringSearchText: any;
  statusMainFilter = new StatusMainFilter();
  gridmeasures: any;
  stayCollapsedGridMeasures: boolean;
  roleAccessDefinition: RoleAccess;
  gridApi: any;
  gridColumnApi: any;
  calculatedPaginationPageSize: number;
  protected globalGridOptions: any = <GridOptions>{
    context: {
      componentParent: this
    },
    pagination: true,
    enableSorting: true,
    enableColResize: true,
    floatingFilter: true,
    rowHeight: Globals.GRID_ROW_HEIGHT,
    localeText: {
      equals: 'ist gleich',
      contains: 'enthält',
      startsWith: 'beginnt mit',
      endsWith: 'endet mit',
      notContains: 'enthält nicht',
      notEqual: 'ist ungleich',
      noRowsToShow: 'Keine Daten vorhanden',
      greaterThan: 'ist großer als',
      lessThan: 'ist kleiner als',
      inRange: 'ist zwischen',
      clearFilter: 'Filter zurücksetzen',
      page: 'Seite',
      to: 'bis',
      of: 'von'
    },
    onSortChanged: (model) => {
      const currentState = JSON.stringify(model.api.getSortModel());
      this.sessionContext.setSortingState(currentState);
      const oldState = JSON.stringify(this.sortingState);
      this.settingsIsDirty = this.sessionContext.getFilterDirtyState() || currentState !== oldState;
      this.sessionContext.setFilterDirtyState(this.settingsIsDirty);
      this.sortingState = model.api.getSortModel();
    },
    onFilterChanged: (model) => {
      const currentState = JSON.stringify(model.api.getFilterModel());
      this.sessionContext.setFilteringSearchText(currentState);
      const oldState = JSON.stringify(this.filteringSearchText);
      this.settingsIsDirty = this.sessionContext.getFilterDirtyState() || currentState !== oldState;
      this.sessionContext.setFilterDirtyState(this.settingsIsDirty);
      this.filteringSearchText = model.api.getFilterModel();

    },
    onColumnMoved: (model) => {
      const currentState = JSON.stringify(model.columnApi.getColumnState());
      this.sessionContext.setColumnState(currentState);
      const oldState = JSON.stringify(this.columnState);
      this.settingsIsDirty = this.sessionContext.getFilterDirtyState() || currentState !== oldState;
      this.sessionContext.setFilterDirtyState(this.settingsIsDirty);
      this.columnState = model.columnApi.getColumnState();
    },
    onGridReady: (params) => {
      // this.onGridReady(params);

      this.gridApi = params.api;
      this.gridColumnApi = params.columnApi;
      this.initAgGridStructure();

    },
    onRowDragEnd: (event: RowDragEndEvent) => {
      this.onRowDragEnd(event);
    }
  };

  protected gridOptions: GridOptions;
  constructor(
    private ref: ChangeDetectorRef,
    protected userSettingsService: UserSettingsService,
    protected router: Router,
    protected gridMeasureService: GridMeasureService,
    protected reminderService: ReminderService,
    protected sessionContext: SessionContext,
    protected daterangepickerConfig: DaterangepickerConfig,
    protected roleAccessHelper: RoleAccessHelperService,
    protected modeValidator: ModeValidator,
    protected toasterMessageService: ToasterMessageService) {

    this.gridOptions = <GridOptions>{};
  }


  ngOnDestroy() {
  }

  ngOnInit() {
    this.settingsIsDirty = this.sessionContext.getFilterDirtyState();
    this.init();
  }

  protected initAgGrid() { }

  protected initAgGridStructure() {
    const allColumnIds = [];

    if (this.columnState) {
      this.gridColumnApi.setColumnState(this.sessionContext.getColumnState() || this.columnState);
    }

    if (this.filteringSearchText) {
      this.gridApi.setFilterModel(this.sessionContext.getFilteringSearchText() || this.filteringSearchText);
      this.gridApi.onFilterChanged();
    }

    if (this.sortingState) {
      this.gridApi.setSortModel(this.sessionContext.getSortingState() || this.sortingState);
    }

    this.gridColumnApi.getAllColumns().forEach(function (column) {
      allColumnIds.push(column.colId);
    });
    this.gridColumnApi.autoSizeColumns(allColumnIds);
  }
  protected init() {
    this.user = this.sessionContext.getCurrUser();

    // console.log('ROLE: ' + this.user.role);
    this.roleAccessDefinition = this.roleAccessHelper.getRoleAccessDefinitions();
    this.userSettingsService.getUserSettings(this.gridId)
      .subscribe(res => {
        if (res.value) {
          this.userSettings = res;
          this.sortingState = this.sessionContext.getSortingState() || res.value[SettingType.Sorting] || this.sortingState;
          this.filteringSearchText = this.sessionContext.getFilteringSearchText()
            || res.value[SettingType.Filter] || this.filteringSearchText;
          this.columnState = this.sessionContext.getColumnState() || res.value[SettingType.ColumnState] || this.columnState;
          this.statusMainFilter = this.sessionContext.getStatusMainFilter() || res.value[SettingType.StatusFilter] || this.statusMainFilter;
        }
        this.retrieveData();
        this.initAgGrid();
      },
        error => {
          console.log(error);
          this.retrieveData();
        });
    // Daterangepicker Config
    this.daterangepickerConfig.settings = {
      timePicker: true,
      timePicker24Hour: true,
      timePickerSeconds: false,
      timePickerIncrement: 1,
      useCurrent: true,
      locale: {
        format: 'DD.MM.YYYY HH:mm',
        applyLabel: '&Uuml;bernehmen',
        cancelLabel: 'Abbrechen',
        daysOfWeek: ['So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa'],
        monthNames: ['Januar', 'Februar', 'M&auml;rz', 'April', 'Mai', 'Juni', 'Juli',
          'August', 'September', 'Oktober', 'November', 'Dezember'],
        firstDay: 1
      }
    };
  }
  public saveSettings() {

    if (!this.userSettings) {
      this.userSettings = <UserSettings>{
        username: this.user.username,
        settingType: this.gridId,
        value: <SettingValue>{
          [SettingType.Sorting]: this.sortingState,
          [SettingType.Filter]: this.filteringSearchText,
          [SettingType.ColumnState]: this.columnState,
          [SettingType.StatusFilter]: this.statusMainFilter
        }
      };
    } else {
      this.userSettings = {
        ...this.userSettings, value: <SettingValue>{
          [SettingType.Sorting]: this.sortingState,
          [SettingType.Filter]: this.filteringSearchText,
          [SettingType.ColumnState]: this.columnState,
          [SettingType.StatusFilter]: this.statusMainFilter
        }
      };
    }

    this.userSettingsService.setUserSettings(this.userSettings).subscribe(gms => {
      this.settingsIsDirty = false;
      this.sessionContext.setFilterDirtyState(this.settingsIsDirty);
    }, error => {
      console.log(error);
    });

  }

  protected calculatePaginationPageSize(gridMeasure: number) {
  }

  protected onRowDragEnd(e) {
    console.log('onRowDragEnd', e);
  }

  protected selectionChanged(): void {

  }
  protected onItemChanged(item: any): void {
  }
  protected retrieveData() { }

  onItemAdded(item: any): void {
  }

  getListHelperTool() {
    return new ListHelperTool();
  }
  protected changeAllSelection() { }

}
