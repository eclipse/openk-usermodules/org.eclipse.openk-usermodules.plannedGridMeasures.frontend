/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { Component, Input, Output, EventEmitter } from '@angular/core';
import { AbstractListComponent } from '../common-components/abstract-list/abstract-list.component';
import { Globals } from './../../common/globals';
import { GridOptions } from 'ag-grid/dist/lib/entities/gridOptions';
import { GridMeasure } from '../../model/grid-measure';
import { EmailDistributionEntry } from '../../model/email-distribution-entry';
import { EmailDistributionListAgGridConfiguration } from './email-distribution-ag-grid-configuration';

@Component({
  selector: 'app-email-distribution-list',
  templateUrl: './email-distribution-list.component.html',
  styleUrls: ['./email-distribution-list.component.css', '../common-components/abstract-list/abstract-list.component.css']
})

export class EmailDistributionListComponent extends AbstractListComponent {

  @Input() gridMeasureDetail: GridMeasure = new GridMeasure();
  @Output() gridMeasureChanged = new EventEmitter<GridMeasure>();

  Globals = Globals;
  listEmailDistribution: any;
  currentDate = new Date();
  isStatusCollapsed = true;

  initAgGrid() {
    const localGridOptions = <GridOptions>{
      columnDefs: EmailDistributionListAgGridConfiguration.createColumnDefs(),
      pagination: false
    };
    this.gridOptions = Object.assign(this.globalGridOptions, localGridOptions);

  }

  retrieveData() {

    if (!this.gridMeasureDetail.listEmailDistribution || this.gridMeasureDetail.listEmailDistribution.length === 0) {
      this.gridMeasureDetail.listEmailDistribution = new Array<EmailDistributionEntry>();

      let idCount = 1;
      // preconfigured emailaddresses from mailtemplates
      let preconfiguredEmailAddressStringArray = [];
      preconfiguredEmailAddressStringArray = this.sessionContext.getEmailAddressesFromTemplates();
      if (preconfiguredEmailAddressStringArray && preconfiguredEmailAddressStringArray.length > 0) {
        preconfiguredEmailAddressStringArray.forEach(email => {
          const ede = new EmailDistributionEntry();
          ede.emailAddress = email.trim();
          ede.preconfigured = true;
          ede.delete = false;
          ede._isValide = true;
          ede.id = idCount;
          this.gridMeasureDetail.listEmailDistribution = [...this.gridMeasureDetail.listEmailDistribution, ede];
          idCount++;
        });
      }

      if (this.gridMeasureDetail.emailAddresses) {

        const emailAddressStringArray = this.gridMeasureDetail.emailAddresses.split(';');
        emailAddressStringArray.forEach(email => {
          const ede = new EmailDistributionEntry();
          ede.emailAddress = email.trim();
          ede.preconfigured = false;
          ede.delete = false;
          ede._isValide = true;
          ede.id = idCount;
          this.gridMeasureDetail.listEmailDistribution = [...this.gridMeasureDetail.listEmailDistribution, ede];
          idCount++;
        });
      }

    }

    this.listEmailDistribution = this.gridMeasureDetail.listEmailDistribution;
    this.showSpinner = false;

  }


  deleteEmailAddress(emailDistributionEntry: EmailDistributionEntry): void {

    emailDistributionEntry.delete = true;
    if (!this.gridMeasureDetail.listEmailDistributionDeleted) {
      this.gridMeasureDetail.listEmailDistributionDeleted = new Array<EmailDistributionEntry>();
    }

    if (emailDistributionEntry.id !== Globals.TEMP_ID_TO_SHOW_NEW_EMAILDISTRIBUTIONENTRYS) {
      this.gridMeasureDetail.listEmailDistributionDeleted.push(emailDistributionEntry);
    }

    this.gridMeasureDetail.listEmailDistribution = this.gridMeasureDetail.listEmailDistribution.filter(s => !s.delete);
    this.listEmailDistribution = this.gridMeasureDetail.listEmailDistribution;
    this.gridMeasureChanged.emit(this.gridMeasureDetail);
  }
}

