/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { SimpleChange } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AbstractMockObservableService } from '../../testing/abstract-mock-observable.service';
import { MockComponent } from '../../testing/mock.component';
import { ActivatedRouteStub } from '../../testing/router-stubs';
import { Globals } from './../../common/globals';

import { SessionContext } from './../../common/session-context';
import { GridMeasureService } from '../../services/grid-measure.service';
import { USERS } from '../../test-data/users';
import { EmailDistributionEntry } from '../../model/email-distribution-entry';
import { GRIDMEASURE } from '../../test-data/grid-measures';
import { BaseDataService } from '../../services/base-data.service';
import { ToasterMessageService } from '../../services/toaster-message.service';
import { MessageService } from 'primeng/api';
import { EmailDistributionEntryComponent } from '../../pages/email-distribution-entry/email-distribution-entry.component';

class FakeRouter {
    navigate(commands: any[]) {
        return commands[0];
    }
}

describe('EmailDistributionEntryComponent', () => {
    let component: EmailDistributionEntryComponent;
    let fixture: ComponentFixture<EmailDistributionEntryComponent>;
    let activatedStub: ActivatedRouteStub;
    let sessionContext: SessionContext;

    let mockGridmeasureService;
    let toasterMessageService: ToasterMessageService;
    let mockBaseDataService;
    let messageService: MessageService;

    class MockGridmeasureService extends AbstractMockObservableService {
        storeGridMeasure() {
            return this;
        }
        getGridMeasure(id: number) {
            return this;
        }
    }

    class MockBaseDataService extends AbstractMockObservableService {
        getEmailAddressesFromGridmeasures() {
            return this;
        }
    }

    beforeEach(async(() => {
        messageService = new MessageService;
        activatedStub = new ActivatedRouteStub();
        sessionContext = new SessionContext();
        toasterMessageService = new ToasterMessageService(sessionContext, messageService);
        mockGridmeasureService = new MockGridmeasureService();
        mockBaseDataService = new MockBaseDataService();

        TestBed.configureTestingModule({
            imports: [
                FormsModule
            ],
            declarations: [
                EmailDistributionEntryComponent,
                MockComponent({ selector: 'input', inputs: ['options'] })
            ],
            providers: [
                { provide: ActivatedRoute, useValue: activatedStub },
                { provide: SessionContext, useValue: sessionContext },
                { provide: ToasterMessageService, useValue: toasterMessageService },
                { provide: BaseDataService, useValue: mockBaseDataService },
                { provide: GridMeasureService, useValue: mockGridmeasureService }
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(EmailDistributionEntryComponent);
        component = fixture.componentInstance;
        sessionContext.setCurrUser(USERS[0]);
        sessionContext.setAllUsers(USERS);
        component.isReadOnlyForm = false;

        component.isCollapsible = true;
        component.gridMeasureDetail = GRIDMEASURE[0];
        activatedStub.testParams = { id: 555, mode: Globals.MODE.EDIT };
        component.ngOnInit();
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should set form in readonly mode', () => {
        component.readOnlyForm = false;
        fixture.detectChanges();
        component.ngOnChanges({
            isReadOnlyForm: new SimpleChange(component.readOnlyForm, true, true)
        });
        fixture.detectChanges();
        expect(component.readOnlyForm).toBeTruthy();
    });

    it('should convert xmlstring to json correctly', () => {
        const xmlstring = `<root>
        <child><textNode>First &amp; Child</textNode></child>
        <child><textNode>Second Child</textNode></child>
        <testAttrs attr1='attr1Value'/>
        </root>`;
        const jsonstring =
            `{"root":{"child":[{"textNode":"First & Child"},{"textNode":"Second Child"}],"testAttrs":{"_attr1":"attr1Value"}}}`;
        expect(JSON.stringify(component.convertXmlToJsonObj(xmlstring))).toBe(jsonstring);
    });

    it('should add email-distribution-entry correctly for empty emailDistributionList', () => {
        component.gridMeasureDetail.listEmailDistribution = [];
        component.emailDistributionEntry = new EmailDistributionEntry();
        component.emailDistributionEntry.id = 3;
        component.emailDistributionEntry.emailAddress = 'test-me-new@test.de';
        component.emailDistributionEntry.delete = false;
        component.emailDistributionEntry._isValide = true;
        component.processAddEmailDistributionEntry();
        fixture.detectChanges();
        expect(component.gridMeasureDetail.listEmailDistribution.length).toBe(1);
    });

    it('should add email-distribution-entry correctly', () => {
        component.emailDistributionEntry = new EmailDistributionEntry();
        component.emailDistributionEntry.id = 3;
        component.emailDistributionEntry.emailAddress = 'test-me-new1@test.de';
        component.emailDistributionEntry.delete = false;
        component.emailDistributionEntry._isValide = true;
        const numberOflistEmailDistribution = component.gridMeasureDetail.listEmailDistribution.length;
        component.processAddEmailDistributionEntry();
        fixture.detectChanges();
        expect(component.gridMeasureDetail.listEmailDistribution.length).toBe(numberOflistEmailDistribution + 1);
    });

    it('should emit warning message for empty email-distribution-entry', async(() => {
        spyOn(toasterMessageService, 'showWarn').and.callThrough();

        component.emailDistributionEntry.id = 3;
        component.emailDistributionEntry.emailAddress = '';
        component.emailDistributionEntry.delete = false;
        component.emailDistributionEntry._isValide = false;

        component.processAddEmailDistributionEntry();

        fixture.whenStable().then(() => {
            fixture.detectChanges();
            expect(toasterMessageService.showWarn).toHaveBeenCalled();
        });
    }));

});
