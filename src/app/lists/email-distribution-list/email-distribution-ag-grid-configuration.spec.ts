/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/



import { TestBed } from '@angular/core/testing';
import { EmailDistributionListAgGridConfiguration } from './email-distribution-ag-grid-configuration';
import { SessionContext } from '../../common/session-context';

describe('EmailDistributionListAgGridConfiguration', () => {


    beforeEach(() => {
        TestBed.configureTestingModule({
        });
    });

    it('should provide the correct functions', (() => {
        const configArray: any = EmailDistributionListAgGridConfiguration.createColumnDefs();

        const idSegment = configArray[0];
        expect(idSegment.field).toBe('id');

        const emailAddressSegment = configArray[1];
        expect(emailAddressSegment.field).toBe('emailAddress');

    }));

    it('should provide the correct modeCellRenderer', (() => {
        const configArray: any = (EmailDistributionListAgGridConfiguration as any).modeCellRenderer(
            {
                context:
                {
                    componentParent:
                    {
                        sessionContext: {
                            isLocked: function (data: any) { return true; }
                        },
                        modeValidator:
                        {
                            isEditModeAllowed: function (data: any) { return true; },
                            isEmailEditModeAllowed: function (data: any) { return true; }
                        },
                        deleteEmailAddress: function (data: any) { return true; }
                    }
                },
                params: {},
                data: { id: 1, emailAddress: 'test@test.de', delete: false }
            }
        );

    }));

});
