/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/


export class EmailDistributionListAgGridConfiguration {

    static createColumnDefs() {
        return [
            {
                headerName: '',
                field: 'id',
                cellRenderer: 'agGroupCellRenderer',
                cellRendererParams: { innerRenderer: EmailDistributionListAgGridConfiguration.modeCellRenderer },
                suppressFilter: true,
                cellClass: 'grid-measure-mode',
                colId: 'modeButton',
                minWidth: 58
            },
            {
                headerName: 'Verteilerliste',
                field: 'emailAddress',
                headerClass: 'grid-measures-header',
                cellClass: 'grid-measure-tab-emailaddress',
                suppressFilter: true,
                colId: 'emailAddress',
                width: 300
            }
        ];
    }

    private static modeCellRenderer(params) {
        const isEditMode =
            params.context.componentParent.modeValidator.isEmailEditModeAllowed(params.context.componentParent.gridMeasureDetail);
        const span = document.createElement('span');
        const modeHtmlTemplate = (isEditMode && !params.data.delete && !params.data.preconfigured) &&
            !params.context.componentParent.sessionContext.isLocked() &&
            !params.context.componentParent.sessionContext.isReadOnlyForStatus(params.context.componentParent.gridMeasureDetail) ?
            '<span style="cursor: default;">' +
            '<button type="button" id="modeButton" class="btn btn-danger btn-sm" >' +
            '<span class="glyphicon glyphicon-trash"></span>' +
            '</button>' +
            '</span>' :
            '<span style="cursor: not-allowed;">' +
            '<button type="button" id="modeButton" class="btn btn-default btn-sm" disabled>' +
            '<span class="glyphicon glyphicon-trash"></span>' +
            '</button></span>';
        span.innerHTML = '<span style="cursor: default;">' +
            modeHtmlTemplate +
            '</span>';
        const eButton = span.querySelector('#modeButton');
        eButton.addEventListener('click', function () {
            params.context.componentParent.deleteEmailAddress(params.data);
        });
        return span;

    }

}
