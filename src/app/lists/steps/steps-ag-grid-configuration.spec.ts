/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/



import { TestBed } from '@angular/core/testing';
import { StepsAgGridConfiguration } from './steps-ag-grid-configuration';

describe('StepsAgGridConfiguration', () => {

    beforeEach(() => {
        TestBed.configureTestingModule({
        });
    });

    it('should provide the correct functions', (() => {
        const configArray: any = StepsAgGridConfiguration.createColumnDefs();

        const idSegment = configArray[1];
        expect(idSegment.field).toBe('sortorder');

        const switchingObjectSegment = configArray[2];
        expect(switchingObjectSegment.field).toBe('switchingObject');

        const sortOrderSegment = configArray[4];
        expect(sortOrderSegment.field).toBe('targetState');
    }));

    it('should provide the correct modeCellRenderer', (() => {
        const configArray: any = (StepsAgGridConfiguration as any).modeCellRenderer(
            {
                context:
                {
                    componentParent:
                    {
                        gridMeasureDetail: { statusId: 1 },
                        sessionContext: {
                            isLocked: function (data: any) { return true; },
                            isReadOnlyForStatus: function (data: any) { return true; }
                        },
                        modeValidator:
                        {
                            isEditModeAllowed: function (data: any) { return true; }
                        },
                        steps: {
                            deleteStep: function (data: any) { return true; }
                        }
                    }
                },
                params: {},
                data: {
                    id: 1, sortorder: 1, switchingObject: 'Schalter 1', targetState: 'aus',
                    singleGridmeasureId: 3, delete: false
                }
            }
        );

    }));

});
