/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import { FormattedDatePipe } from '../../common-components/pipes/formatted-date.pipe';
import { FormattedTimestampPipe } from '../../common-components/pipes/formatted-timestamp.pipe';
import { StringToDatePipe } from '../../common-components/pipes/string-to-date.pipe';
import { Lock } from '../../model/lock';
import { GridMeasureService } from '../../services/grid-measure.service';
import { ReminderService } from '../../services/reminder.service';
import { LockService } from '../../services/lock.service';
import { UserSettingsService } from '../../services/user-settings.service';
import { GRIDMEASURE } from '../../test-data/grid-measures';
import { AbstractMockObservableService } from '../../testing/abstract-mock-observable.service';
import { MockComponent } from '../../testing/mock.component';
import { AbstractListComponent } from '../common-components/abstract-list/abstract-list.component';
import { UniquePipe } from './../../common-components/pipes/unique.pipe';
import { SessionContext } from './../../common/session-context';
import { UserSettings } from './../../model/user-settings';
import { USERS } from './../../test-data/users';
import { StepsComponent } from './steps.component';
import { RoleAccessHelperService } from '../../services/jobs/role-access-helper.service';
import { ModeValidator } from '../../custom_modules/helpers/mode-validator';
import { ToasterMessageService } from '../../services/toaster-message.service';


class FakeRouter {
    navigate(commands: any[]) {
        return commands[0];
    }
}

describe('StepsComponent', () => {
    let component: StepsComponent;
    let fixture: ComponentFixture<StepsComponent>;
    let routerStub: FakeRouter;
    let router: Router;
    let sessionContext: SessionContext;

    routerStub = {
        navigate: jasmine.createSpy('navigate').and.callThrough()
    };

    class MockService extends AbstractMockObservableService {
        getGridMeasures() {
            return this;
        }
    }

    class MockReminderService extends AbstractMockObservableService {
        getCurrentReminders() {
            return this;
        }
    }

    class MockUserSettingService extends AbstractMockObservableService {
        savedUserSettings: UserSettings;
        getUserSettings(gridId: string) {
            return this;
        }
        setUserSettings(userSettings: UserSettings) {
            this.savedUserSettings = userSettings;
            return this;
        }
    }
    class MockLockService extends AbstractMockObservableService {
        checkLock(key: number, info: string) {
            const lock = new Lock();
            lock.key = key;
            lock.username = 'otto';
            lock.info = info;
            return lock;
        }
        storeLock(lock: Lock) {
            return lock;
        }

        deleteLock(key: number, info: string) {
            return key;
        }
    }

    let mockGridMeasureService;
    let mockReminderService;
    let mockUserSettingService;
    let mockLockService: MockLockService;
    let roleAccessHelper: RoleAccessHelperService;

    beforeEach(async(() => {
        router = new FakeRouter() as any as Router;
        sessionContext = new SessionContext();

        mockGridMeasureService = new MockService();
        mockReminderService = new MockReminderService();
        mockUserSettingService = new MockUserSettingService();
        mockLockService = new MockLockService();
        roleAccessHelper = new RoleAccessHelperService();

        TestBed.configureTestingModule({
            imports: [
                FormsModule
            ],
            declarations: [
                StepsComponent,
                StringToDatePipe,
                AbstractListComponent,
                FormattedDatePipe,
                FormattedTimestampPipe,
                UniquePipe,
                MockComponent({ selector: 'input', inputs: ['options', 'gridId'] }),
                MockComponent({ selector: 'app-loading-spinner' }),
                MockComponent({ selector: 'ag-grid-angular ', inputs: ['gridOptions', 'rowData'] })
            ],
            providers: [
                ModeValidator,
                { provide: SessionContext, useValue: sessionContext },
                { provide: UserSettingsService, useValue: mockUserSettingService },
                { provide: Router, useValue: routerStub },
                { provide: GridMeasureService, useValue: mockGridMeasureService },
                { provide: ReminderService, useValue: mockReminderService },
                { provide: LockService, useValue: mockLockService },
                { provide: DaterangepickerConfig, useClass: DaterangepickerConfig },
                { provide: RoleAccessHelperService, useValue: roleAccessHelper },
                { provide: ToasterMessageService }
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(StepsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should retrieveData (steps) on init', async(() => {
        mockGridMeasureService.content = JSON.parse(JSON.stringify(GRIDMEASURE[0].listSingleGridmeasures[0].listSteps));
        mockReminderService.content = [];
        const abstractComp: any = component; // used to access privates
        component.user = USERS[1];
        component.gridId = 'steps';
        component.gridMeasureDetail = JSON.parse(JSON.stringify(GRIDMEASURE[0]));
        component.singleGridMeasure = GRIDMEASURE[0].listSingleGridmeasures[0];
        component.sortingState = {
            column: 'shorty',
            counter: 1,
            defaultState: true,
            isDesc: true
        };
        component.filteringSearchText = {
            branchId: 'filty',
            title: 'fix',
            statusId: 'foxy'
        };
        mockUserSettingService.savedUserSettings = {};

        abstractComp.saveSettings();
        component.retrieveData();
        fixture.detectChanges();

        fixture.whenRenderingDone().then(() => {
            fixture.detectChanges();
            expect(component.singleGridMeasure.listSteps.length).toBe(3);
        });
    }));

    it('should add step to deleted list', async(() => {
        mockGridMeasureService.content = JSON.parse(JSON.stringify(GRIDMEASURE[0].listSingleGridmeasures[0].listSteps));
        mockReminderService.content = [];
        const abstractComp: any = component; // used to access privates
        component.user = USERS[1];
        component.gridId = 'steps';
        component.gridMeasureDetail = JSON.parse(JSON.stringify(GRIDMEASURE[0]));
        component.singleGridMeasure = GRIDMEASURE[0].listSingleGridmeasures[0];
        component.sortingState = {
            column: 'shorty',
            counter: 1,
            defaultState: true,
            isDesc: true
        };
        component.filteringSearchText = {
            branchId: 'filty',
            title: 'fix',
            statusId: 'foxy'
        };
        mockUserSettingService.savedUserSettings = {};

        abstractComp.saveSettings();
        component.retrieveData();
        fixture.detectChanges();

        fixture.whenRenderingDone().then(() => {
            fixture.detectChanges();
            expect(component.singleGridMeasure.listSteps.length).toBe(3);

            component.deleteStep(component.singleGridMeasure.listSteps[1]);
            fixture.detectChanges();

            fixture.whenRenderingDone().then(() => {
                fixture.detectChanges();
                expect(component.singleGridMeasure.listSteps.length).toBe(2);
                expect(component.singleGridMeasure.listSteps.filter(s => s.delete === true).length).toBe(0);
                expect(component.singleGridMeasure.listStepsDeleted.length).toBe(1);
            });
        });
    }));

    it('should call changeAllSelection and set selected false', async(() => {
        component.selectAll = false;
        component.gridMeasureDetail = JSON.parse(JSON.stringify(GRIDMEASURE[0]));
        component.singleGridMeasure = GRIDMEASURE[0].listSingleGridmeasures[0];
        fixture.detectChanges();
        spyOn(component, 'changeAllSelection').and.callThrough();

        fixture.detectChanges();

        component.changeAllSelection();
        expect(component.changeAllSelection).toHaveBeenCalled();
        expect(component.singleGridMeasure.listSteps[0].selected).toBeFalsy();
    }));

    it('should call changeAllSelection and set selected true', async(() => {
        component.selectAll = true;
        component.gridMeasureDetail = JSON.parse(JSON.stringify(GRIDMEASURE[0]));
        component.singleGridMeasure = GRIDMEASURE[0].listSingleGridmeasures[0];
        fixture.detectChanges();
        spyOn(component, 'changeAllSelection').and.callThrough();

        fixture.detectChanges();

        component.changeAllSelection();
        expect(component.changeAllSelection).toHaveBeenCalled();
        expect(component.singleGridMeasure.listSteps[0].selected).toBeTruthy();
    }));

});
