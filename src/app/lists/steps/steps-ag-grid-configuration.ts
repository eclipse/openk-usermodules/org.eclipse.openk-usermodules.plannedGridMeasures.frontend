/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/


export class StepsAgGridConfiguration {


    static createColumnDefs() {
        return [
            {
                headerName: '',
                field: 'id',
                cellRenderer: 'agGroupCellRenderer',
                cellRendererParams: { innerRenderer: StepsAgGridConfiguration.modeCellRenderer },
                suppressFilter: true,
                cellClass: 'grid-measure-mode',
                colId: 'modeButton',
                minWidth: 58
            },
            {
                headerName: 'Nr',
                field: 'sortorder',
                headerClass: 'grid-measures-header',
                cellClass: 'grid-measure-tab-sortorder',
                suppressFilter: true,
                colId: 'sortorder',
                width: 80,
                rowDrag: true
            },
            {
                headerName: 'Objekt der Schaltung',
                field: 'switchingObject',
                headerClass: 'grid-measures-header',
                cellClass: 'grid-measure-tab-switchingObject',
                suppressFilter: true,
                colId: 'switchingObject',
                width: 220,
                editable: true
            },
            {
                headerName: 'Ist-Zustand',
                field: 'presentState',
                headerClass: 'grid-measures-header',
                cellClass: 'grid-measure-tab-presentState',
                suppressFilter: true,
                colId: 'presentState',
                width: 180,
                editable: true
            },
            {
                headerName: 'Soll-Zustand',
                field: 'targetState',
                headerClass: 'grid-measures-header',
                cellClass: 'grid-measure-tab-targetState',
                suppressFilter: true,
                colId: 'targetState',
                width: 180,
                editable: true
            },
            {
                headerName: 'Typ',
                field: 'type',
                headerClass: 'grid-measures-header',
                cellClass: 'grid-measure-tab-type',
                suppressFilter: true,
                colId: 'type',
                width: 100,
                editable: true
            },
            {
                headerName: 'Ist-Zeit',
                field: 'presentTime',
                headerClass: 'grid-measures-header',
                cellClass: 'grid-measure-tab-presentTime',
                suppressFilter: true,
                colId: 'presentTime',
                width: 180,
                editable: true
            },
            {
                headerName: 'Ausführender',
                field: 'operator',
                headerClass: 'grid-measures-header',
                cellClass: 'grid-measure-tab-operator',
                suppressFilter: true,
                colId: 'operator',
                width: 180,
                editable: true
            }
        ];
    }

    private static modeCellRenderer(params) {
        const isEditMode = params.context.componentParent.modeValidator.isEditModeAllowed(
            params.context.componentParent.gridMeasureDetail.statusId);
        const span = document.createElement('span');
        const modeHtmlTemplate = (isEditMode && !params.data.delete) &&
            !params.context.componentParent.sessionContext.isLocked() &&
            !params.context.componentParent.sessionContext.isReadOnlyForStatus(params.context.componentParent.gridMeasureDetail) ?
            '<span style="cursor: default;">' +
            '<button type="button" id="modeButton" class="btn btn-danger btn-sm" >' +
            '<span class="glyphicon glyphicon-trash"></span>' +
            '</button>' +
            '</span>' :
            '<span style="cursor: not-allowed;">' +
            '<button type="button" id="modeButton" class="btn btn-default btn-sm" disabled>' +
            '<span class="glyphicon glyphicon-trash"></span>' +
            '</button></span>';
        span.innerHTML = '<span style="cursor: default;">' +
            modeHtmlTemplate +
            '</span>';
        const eButton = span.querySelector('#modeButton');
        eButton.addEventListener('click', function () {
            params.context.componentParent.deleteStep(params.data);
        });
        return span;
    }

}
