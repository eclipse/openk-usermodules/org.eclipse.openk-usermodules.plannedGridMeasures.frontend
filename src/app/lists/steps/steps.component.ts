/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
import { Component, Input } from '@angular/core';
import { AbstractListComponent } from '../common-components/abstract-list/abstract-list.component';
import { Globals } from './../../common/globals';
import { GridOptions } from 'ag-grid/dist/lib/entities/gridOptions';
import { GridMeasure } from '../../model/grid-measure';
import { StepsAgGridConfiguration } from './steps-ag-grid-configuration';
import { SingleGridMeasure } from '../../model/single-grid-measure';
import { Step } from '../../model/step';
import { RowDragEvent } from '../../../../node_modules/ag-grid';

@Component({
  selector: 'app-steps',
  templateUrl: './steps.component.html',
  styleUrls: ['./steps.component.css', '../common-components/abstract-list/abstract-list.component.css'],
})

export class StepsComponent extends AbstractListComponent {

  @Input() gridMeasureDetail: GridMeasure = new GridMeasure();
  @Input() singleGridMeasure: SingleGridMeasure = new SingleGridMeasure();

  Globals = Globals;
  currentDate = new Date();
  isStatusCollapsed = true;


  initAgGrid() {

    const localGridOptions = <GridOptions>{
      rowDragManaged: !this.sessionContext.isReadOnlyForStatus(this.gridMeasureDetail),
      animateRows: true,
      stopEditingWhenGridLosesFocus: true,
      columnDefs: StepsAgGridConfiguration.createColumnDefs(),
      onRowDragEnd: (ev: RowDragEvent) => {
        this.setNewSortOrderAfterDrag();
      },
      pagination: false,
      enableSorting: false,
      enterMovesDown: true,
      enterMovesDownAfterEdit: true
    };
    this.gridOptions = Object.assign(this.globalGridOptions, localGridOptions);

  }

  retrieveData() {
    this.showSpinner = false;
    if (!this.singleGridMeasure.listSteps) {
      this.singleGridMeasure.listSteps = new Array<Step>();
    }
  }

  changeAllSelection(): void {
    if (this.selectAll) {
      for (const info of this.singleGridMeasure.listSteps) {
        info.selected = true;
      }
    } else {
      for (const info of this.singleGridMeasure.listSteps) {
        info.selected = false;
      }
    }
  }

  setNewSortOrderAfterDrag(): void {
    const listStepsTmp = new Array<Step>();
    const modelData: any[] = this.gridApi.getModel().rowsToDisplay;
    let newSortOrder = 1;
    for (const stepNode of modelData) {
      const step: Step = stepNode.data;
      step.sortorder = newSortOrder;
      listStepsTmp.push(step);
      newSortOrder++;
    }
    this.singleGridMeasure.listSteps = listStepsTmp;

  }

  deleteStep(step: Step): void {

    step.delete = true;
    if (!this.singleGridMeasure.listStepsDeleted) {
      this.singleGridMeasure.listStepsDeleted = new Array<Step>();
    }

    if (step.id !== Globals.TEMP_ID_TO_SHOW_NEW_STEPS) {
      this.singleGridMeasure.listStepsDeleted.push(step);
    }

    this.singleGridMeasure.listSteps = this.singleGridMeasure.listSteps.filter(s => !s.delete);
    for (let i = 0; i < this.singleGridMeasure.listSteps.length; i++) {
      this.singleGridMeasure.listSteps[i].sortorder = i + 1;
    }
  }
}

