/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { FormattedTimestampPipe } from '../../common-components/pipes/formatted-timestamp.pipe';
import { SessionContext } from '../../common/session-context';
import { Globals } from '../../common/globals';
import * as moment from 'moment';

export class GridMeasuresAgGridConfiguration {

    static createColumnDefs(sessionContext: SessionContext) {
        const datePipe = new FormattedTimestampPipe();
        return [
            {
                headerName: '',
                field: 'statusId',
                cellRenderer: 'agGroupCellRenderer',
                cellRendererParams: { innerRenderer: GridMeasuresAgGridConfiguration.modeCellRenderer },
                suppressFilter: true,
                cellClass: 'grid-measure-mode',
                colId: 'modeButton',
                minWidth: 100,
                maxWidth: 150
            },
            {
                headerName: 'Beginnt am',
                field: 'plannedStarttimeFirstSinglemeasure',
                valueFormatter: function (params) {
                    if (!params || !params.data) {
                        return;
                    }
                    return datePipe.transform(params.data.plannedStarttimeFirstSinglemeasure, 'DD.MM.YYYY HH:mm');

                },
                headerClass: 'grid-measures-header',
                cellClass: 'grid-measure-tab-date-col',
                colId: 'plannedStarttimeFirstSinglemeasure',
                cellStyle: function (params) {
                    if (!params || !params.data) {
                        return;
                    }
                    const shortterm = sessionContext.isShorttermNotification(params.data.id);
                    const overdue = sessionContext.isOverdueNotification(params.data.id);
                    if (overdue) {
                        return { color: 'tomato', 'font-weight': 'bold' };
                    } else if (shortterm) {
                        return { color: '#f79e60', 'font-weight': 'bold' };
                    } else {
                        return null;
                    }
                },
                minWidth: 150,
                maxWidth: 300,
                filter: 'agDateColumnFilter',
                filterParams: {
                    inRangeInclusive: true,
                    clearButton: true,
                    // provide comparator function
                    comparator: function (filterLocalDateAtMidnight, cellValue) {

                        // In the example application, dates are stored as dd/mm/yyyy
                        // We create a Date object for comparison against the filter date
                        if (cellValue) {
                            const cellDate = new Date(cellValue);
                            cellDate.setHours(0);
                            cellDate.setMinutes(0);

                            // Now that both parameters are Date objects, we can compare
                            if (cellDate < filterLocalDateAtMidnight) {
                                return -1;
                            } else if (cellDate > filterLocalDateAtMidnight) {
                                return 1;
                            } else {
                                return 0;
                            }
                        }

                    }
                }
            },
            {
                headerName: 'Nummer (ID)',
                field: 'descriptiveId',
                headerClass: 'grid-measures-header',
                cellClass: 'grid-measure-tab-id',
                colId: 'descriptiveId',
                minWidth: 150,
                maxWidth: 300
            },
            {
                headerName: 'Sparte',
                field: 'branchId',
                filter: 'agTextColumnFilter',
                valueGetter: function (params) {

                    if (!params || !params.data) {
                        return;
                    }
                    const branch = sessionContext.getBranchById(params.data.branchId);
                    if (branch) {
                        return branch.name;
                    } else {
                        return '';
                    }
                },
                cellStyle: function (params) {
                    if (!params || !params.data) {
                        return;
                    }
                    const branch = sessionContext.getBranchById(params.data.branchId);
                    if (branch) {
                        return {
                            backgroundColor: branch.colorCode
                        };
                    } else {
                        return '';
                    }
                },
                headerClass: 'grid-measures-header grid-measure-tab-branche',
                cellClass: 'grid-measure-tab-branche',
                colId: 'branchId',
                minWidth: 85,
                maxWidth: 120
            },
            {
                headerName: 'Titel der Maßnahme',
                filter: 'agTextColumnFilter',
                field: 'title',
                headerClass: 'grid-measures-header',
                cellClass: 'grid-measure-tab-title',
                colId: 'title',
                minWidth: 300,
                maxWidth: 625
            },
            {
                headerName: 'Name des Erstellers',
                field: 'createUser',
                headerClass: 'grid-measures-header',
                cellClass: 'grid-measure-tab-createUser',
                colId: 'createUser',
                minWidth: 175,
                maxWidth: 300
            },
            {
                headerName: 'Betroffenes Objekt / Betriebsmittel',
                field: 'affectedResource',
                headerClass: 'grid-measures-header',
                cellClass: 'grid-measure-tab-affectedResource',
                colId: 'affectedResource',
                minWidth: 275,
                maxWidth: 400
            },
            {
                headerName: 'Status',
                field: 'statusId',
                valueGetter: function (params) {
                    if (!params || !params.data) {
                        return;
                    }
                    const status = sessionContext.getStatusById(params.data.statusId);
                    if (status) {
                        return status.name;
                    } else {
                        return '';
                    }
                },
                headerClass: 'grid-measures-header grid-measure-tab-status',
                cellClass: 'grid-measure-tab-status',
                filter: 'agTextColumnFilter',
                colId: 'statusId',
                minWidth: 150,
                maxWidth: 200
            }
        ];
    }



    private static modeCellRenderer(params) {
        const viewBtnStyle = 'style="width: 32px;height: 28px;"';
        const editBtnStyle = 'style="width: 32px;height: 28px;"';
        if (!params || !params.data) {
            return;
        }
        const isEditMode = params.context.componentParent.modeValidator.isEditModeAllowed(params.data.statusId);
        const isCancelAllowed = params.context.componentParent.modeValidator.isCancelAllowed(params.data);
        const span = document.createElement('span');
        const modeHtmlTemplate = isEditMode ? '<button type="button" id="modeButton" class="btn btn-primary btn-sm" '
            + editBtnStyle + ' >' +
            '<span class="glyphicon glyphicon-pencil"></span>' +
            '</button>' :
            '<button id="modeButton" class="btn btn-default btn-sm"' + viewBtnStyle + ' >' +
            '<span class="glyphicon glyphicon-eye-open"></span>' +
            '</button>';
        const cancelHtmlTemplate = isCancelAllowed ?
            '<button type="button" id="cancelButton" class="btn btn-warning btn-sm rescindBtn">' +
            '<span class="glyphicon glyphicon-remove"></span>' +
            '</button>' :
            '<button type="button" id="cancelButton" class="btn btn-warning btn-sm rescindBtnDisabled" disabled="disabled">' +
            '<span class="glyphicon glyphicon-remove"></span>' +
            '</button>';
        span.innerHTML = '<span style="cursor: default;">' +
            modeHtmlTemplate + '&nbsp' + cancelHtmlTemplate +
            '</span>';
        const eButton = span.querySelector('#modeButton');
        eButton.addEventListener('click', function () {
            const mode = isEditMode ? Globals.MODE.EDIT : Globals.MODE.VIEW;
            params.context.componentParent.modeValidator.handleGridMeasureMode(params.data, mode);
        });

        const cancelButton = span.querySelector('#cancelButton');
        cancelButton.addEventListener('click', function () {
            const mode = Globals.MODE.CANCEL;
            let gridMeasure: any;
            params.context.componentParent.gridMeasureService.getGridMeasure(params.data.id).subscribe(async (gm) => {
                gridMeasure = gm;
                params.context.componentParent.modeValidator.handleGridMeasureMode(gridMeasure, mode);
            });
        });
        return span;
    }


}
