/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/



import { TestBed } from '@angular/core/testing';
import { GridMeasuresAgGridConfiguration } from './grid-measures-ag-grid-configuration';
import { SessionContext } from '../../common/session-context';
import { BRANCHES } from '../../test-data/branches';
import { STATUSES } from '../../test-data/statuses';

describe('GridMeasureAgGridConfiguration', () => {
    let sessionContext: SessionContext;

    beforeEach(() => {
        sessionContext = new SessionContext();

        TestBed.configureTestingModule({
        });
    });

    it('should provide the correct functions', (() => {
        const configArray: any = GridMeasuresAgGridConfiguration.createColumnDefs(sessionContext);
        const plannedStarttimeSegment = configArray[1];
        expect(plannedStarttimeSegment.field).toBe('plannedStarttimeFirstSinglemeasure');
        expect(plannedStarttimeSegment.valueFormatter({ data: { plannedStarttimeFirstSinglemeasure: null } })).toBe('');

        sessionContext.setCurrentReminders([111]);
        sessionContext.setExpiredReminders([222]);
        expect((plannedStarttimeSegment.cellStyle({ data: { id: 222 } }) as any).color).toBe('tomato');

        sessionContext.setCurrentReminders([111]);
        sessionContext.setExpiredReminders([]);
        expect((plannedStarttimeSegment.cellStyle({ data: { id: 111 } }) as any).color).toBe('#f79e60');

        const branchIdSegment = configArray[3];
        expect(branchIdSegment.field).toBe('branchId');
        sessionContext.setBranches(BRANCHES);
        expect(branchIdSegment.valueGetter({ data: { branchId: 999 } })).toBeFalsy();
        expect(branchIdSegment.valueGetter({ data: { branchId: 4 } })).toBe('W');

        expect(branchIdSegment.cellStyle({ data: { branchId: 999 } })).toBeFalsy();
        expect((branchIdSegment.cellStyle({ data: { branchId: 4 } }) as any).backgroundColor).toBe('');

        const statusIdSegment = configArray[7];
        expect(statusIdSegment.field).toBe('statusId');
        sessionContext.setStatuses(STATUSES);
        expect(statusIdSegment.valueGetter({ data: { statusId: 999 } })).toBeFalsy();
        expect(statusIdSegment.valueGetter({ data: { statusId: 3 } })).toBe('beendet');
    }));

    it('should provide the correct modeCellRenderer', (() => {
        const configArray: any = (GridMeasuresAgGridConfiguration as any).modeCellRenderer(
            {
                context:
                {
                    componentParent:
                    {
                        modeValidator:
                        {
                            isEditModeAllowed: function (data: any) { return true; },
                            isCancelAllowed: function (data: any) { return true; }
                        }
                    }
                },
                params: {}
            }
        );



    }));


});
