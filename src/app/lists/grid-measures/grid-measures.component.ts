/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { Component } from '@angular/core';
import { AbstractListComponent } from '../common-components/abstract-list/abstract-list.component';
import { ErrorType } from '../../common/enums';
import { Globals } from './../../common/globals';
import { OnInit } from '@angular/core';
import { Observable } from '../../../../node_modules/rxjs/Rx';
import { GridOptions } from 'ag-grid/dist/lib/entities/gridOptions';
import { GridMeasuresAgGridConfiguration } from './grid-measures-ag-grid-configuration';

@Component({
  selector: 'app-grid-measures',
  templateUrl: './grid-measures.component.html',
  styleUrls: ['./grid-measures.component.css', '../common-components/abstract-list/abstract-list.component.css'],
})

export class GridMeasuresComponent extends AbstractListComponent implements OnInit {
  Globals = Globals;
  currentDate = new Date();
  isExpanded = true;
  isCancelClosedFilterButtons: boolean;
  windowHeight: number = window.innerHeight;

  ngOnInit() {
    super.ngOnInit();
    if (this.sessionContext.getStatusMainFilter() !== null) {
      this.statusMainFilter = this.sessionContext.getStatusMainFilter();
    }

    Observable.fromEvent(window, 'resize').debounceTime(100).subscribe(() => this.windowHeight = window.innerHeight);
  }

  initAgGrid() {
    const localGridOptions = <GridOptions>{
      columnDefs: GridMeasuresAgGridConfiguration.createColumnDefs(this.sessionContext)
    };
    this.gridOptions = Object.assign(this.globalGridOptions, localGridOptions);
  }

  async retrieveData() {

    if (this.statusMainFilter.item.onlyUsersGMsDesired === true) {
      this.statusMainFilter.item.isCanceledStatusActive = false;
      this.statusMainFilter.item.isClosedStatusActive = false;
    }

    await this.reminderService.getCurrentReminders().subscribe(currentrems => {
      this.sessionContext.setCurrentReminders(currentrems);
    }, error => {
      console.log(error);
      this.toasterMessageService.showError(ErrorType.retrieve, error);
    });

    await this.reminderService.getExpiredReminders().subscribe(expiredrems => {
      this.sessionContext.setExpiredReminders(expiredrems);
    }, error => {
      console.log(error);
      this.toasterMessageService.showError(ErrorType.retrieve, error);
    });

    this.gridMeasureService.getGridMeasures(this.statusMainFilter).subscribe(gms => {
      this.gridmeasures = gms;
      this.showSpinner = false;
    }, error => {
      console.log(error);
      this.toasterMessageService.showError(ErrorType.retrieve, this.gridId, error);
    });

    this.sessionContext.setStatusMainFilter(this.statusMainFilter);

    if (this.gridApi) {
      this.gridApi.setFilterModel(this.filteringSearchText);
      this.gridApi.onFilterChanged();
    }
  }

  setDirty() {
    this.settingsIsDirty = true;
    this.sessionContext.setFilterDirtyState(true);
  }


  calcGridHeight(): number {
    const gridOffset = document.getElementsByTagName('ag-grid-angular')[0].getBoundingClientRect().top;
    const appOffset = 80;
    const gridHeight = this.windowHeight - appOffset - gridOffset;
    this.calculatePaginationPageSize(gridHeight);
    return gridHeight;
  }

  calculatePaginationPageSize(gridHeight: number) {
    const headerAndBottomRows = 4;
    this.calculatedPaginationPageSize = Math.round(gridHeight / Globals.GRID_ROW_HEIGHT - headerAndBottomRows);
    const localGridOptions = <GridOptions>{
      paginationPageSize: this.calculatedPaginationPageSize
    };
    this.gridOptions = Object.assign(this.globalGridOptions, localGridOptions);
  }

  changeAllSelection(): void {
    if (this.selectAll) {
      for (const info of this.gridmeasures) {
        info.selected = true;
      }
    } else {
      for (const info of this.gridmeasures) {
        info.selected = false;
      }
    }
  }
}
