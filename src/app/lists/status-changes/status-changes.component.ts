/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { Component } from '@angular/core';
import { AbstractListComponent } from '../common-components/abstract-list/abstract-list.component';
import { ErrorType } from '../../common/enums';
import { Globals } from './../../common/globals';
import { GridOptions } from 'ag-grid/dist/lib/entities/gridOptions';
import { StatusChangesAgGridConfiguration } from './status-changes-ag-grid-configuration';

@Component({
  selector: 'app-status-changes',
  templateUrl: './status-changes.component.html',
  styleUrls: ['./status-changes.component.css', '../common-components/abstract-list/abstract-list.component.css']
})

export class StatusChangesComponent extends AbstractListComponent {

  Globals = Globals;
  statuschanges: any;
  currentDate = new Date();
  isStatusCollapsed = true;
  isExpandedStatus = false;

  initAgGrid() {
    const localGridOptions = <GridOptions>{
      columnDefs: StatusChangesAgGridConfiguration.createColumnDefs(this.sessionContext),
      pagination: false
    };
    this.gridOptions = Object.assign(this.globalGridOptions, localGridOptions);

  }

  retrieveData() {

    if (this.gridId) {
      this.gridMeasureService.getHistoricalStatusChanges(parseInt(this.gridId, 10)).subscribe(statchanges => {
        this.statuschanges = statchanges;
        this.showSpinner = false;
      }, error => {
        console.log(error);
        this.toasterMessageService.showError(ErrorType.retrieve, this.gridId, error);
      });
    } else {
      this.statuschanges = [];
      this.showSpinner = false;
    }
  }

  changeAllSelection(): void {
    if (this.selectAll) {
      for (const info of this.statuschanges) {
        info.selected = true;
      }
    } else {
      for (const info of this.statuschanges) {
        info.selected = false;
      }
    }
  }


}

