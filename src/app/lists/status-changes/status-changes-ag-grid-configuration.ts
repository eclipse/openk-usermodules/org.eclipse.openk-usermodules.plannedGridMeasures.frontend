/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { FormattedTimestampPipe } from '../../common-components/pipes/formatted-timestamp.pipe';
import { SessionContext } from '../../common/session-context';

export class StatusChangesAgGridConfiguration {

    static createColumnDefs(sessionContext: SessionContext) {
        const datePipe = new FormattedTimestampPipe();
        return [
            {
                headerName: 'Status (neu)',
                field: 'statusId',
                valueGetter: function (params) {
                    const status = sessionContext.getStatusById(params.data.statusId);
                    if (status) {
                        return status.name;
                    } else {
                        return '';
                    }
                },
                headerClass: 'grid-measures-header grid-measure-tab-status',
                cellClass: 'grid-measure-tab-status',
                filter: 'agTextColumnFilter',
                colId: 'statusId',
                minWidth: 171
            },
            {
                headerName: 'Geändert am',
                field: 'modDate',
                valueFormatter: function (params) {
                    return datePipe.transform(params.data.modDate, 'DD.MM.YYYY HH:mm');

                },
                headerClass: 'grid-measures-header',
                cellClass: 'grid-measure-tab-date-col',
                suppressFilter: true,
                colId: 'modDate'
            },
            {
                headerName: 'Bearbeitet von',
                field: 'modUser',
                valueFormatter: function (params) {
                    return sessionContext.getUserMap().findAndRenderUser(params.data.modUser);
                },
                headerClass: 'grid-measures-header',
                cellClass: 'grid-measure-tab-moduser',
                suppressFilter: true,
                colId: 'modUser',
                width: 300
            }
        ];
    }


}
