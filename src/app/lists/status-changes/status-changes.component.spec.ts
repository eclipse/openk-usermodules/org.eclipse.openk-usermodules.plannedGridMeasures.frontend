/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { ComponentFixture, TestBed, async, fakeAsync, tick } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import { FormattedDatePipe } from '../../common-components/pipes/formatted-date.pipe';
import { FormattedTimestampPipe } from '../../common-components/pipes/formatted-timestamp.pipe';
import { StringToDatePipe } from '../../common-components/pipes/string-to-date.pipe';
import { Lock } from '../../model/lock';
import { GridMeasureService } from '../../services/grid-measure.service';
import { ReminderService } from '../../services/reminder.service';
import { LockService } from '../../services/lock.service';
import { UserSettingsService } from '../../services/user-settings.service';
import { AbstractMockObservableService } from '../../testing/abstract-mock-observable.service';
import { MockComponent } from '../../testing/mock.component';
import { AbstractListComponent } from '../common-components/abstract-list/abstract-list.component';
import { UniquePipe } from './../../common-components/pipes/unique.pipe';
import { SessionContext } from './../../common/session-context';
import { UserSettings } from './../../model/user-settings';
import { USERS } from './../../test-data/users';
import { StatusChangesComponent } from './status-changes.component';
import { RoleAccessHelperService } from '../../services/jobs/role-access-helper.service';
import { ModeValidator } from '../../custom_modules/helpers/mode-validator';
import { STATUSCHANGES } from '../../test-data/status-changes';
import { ToasterMessageService } from '../../services/toaster-message.service';
import { MessageService } from 'primeng/api';

class FakeRouter {
    navigate(commands: any[]) {
        return commands[0];
    }
}

describe('StatusChangesComponent', () => {
    let component: StatusChangesComponent;
    let fixture: ComponentFixture<StatusChangesComponent>;
    let routerStub: FakeRouter;
    let router: Router;
    let sessionContext: SessionContext;

    routerStub = {
        navigate: jasmine.createSpy('navigate').and.callThrough()
    };

    class MockService extends AbstractMockObservableService {
        getGridMeasures() {
            return this;
        }

        getHistoricalStatusChanges(id: number) {
            return this;
        }
    }

    class MockReminderService extends AbstractMockObservableService {
        getCurrentReminders() {
            return this;
        }
    }

    class MockUserSettingService extends AbstractMockObservableService {
        savedUserSettings: UserSettings;
        getUserSettings(gridId: string) {
            return this;
        }
        setUserSettings(userSettings: UserSettings) {
            this.savedUserSettings = userSettings;
            return this;
        }
    }
    class MockLockService extends AbstractMockObservableService {
        checkLock(key: number, info: string) {
            const lock = new Lock();
            lock.key = key;
            lock.username = 'otto';
            lock.info = info;
            return lock;
        }
        storeLock(lock: Lock) {
            return lock;
        }

        deleteLock(key: number, info: string) {
            return key;
        }
    }

    let mockGridMeasureService;
    let mockReminderService;
    let mockUserSettingService;
    let mockLockService: MockLockService;
    let roleAccessHelper: RoleAccessHelperService;
    let toasterMessageService: ToasterMessageService;
    let messageService: MessageService;
    beforeEach(async(() => {
        router = new FakeRouter() as any as Router;
        sessionContext = new SessionContext();
        messageService = new MessageService;
        mockGridMeasureService = new MockService();
        mockReminderService = new MockReminderService();
        mockUserSettingService = new MockUserSettingService();
        mockLockService = new MockLockService();
        roleAccessHelper = new RoleAccessHelperService();
        toasterMessageService = new ToasterMessageService(sessionContext, messageService);

        TestBed.configureTestingModule({
            imports: [
                FormsModule
            ],
            declarations: [
                StatusChangesComponent,
                StringToDatePipe,
                AbstractListComponent,
                FormattedDatePipe,
                FormattedTimestampPipe,
                UniquePipe,
                MockComponent({ selector: 'input', inputs: ['options'] }),
                MockComponent({ selector: 'app-loading-spinner' }),
                MockComponent({ selector: 'ag-grid-angular ', inputs: ['gridOptions', 'rowData'] })
            ],
            providers: [
                ModeValidator,
                { provide: SessionContext, useValue: sessionContext },
                { provide: UserSettingsService, useValue: mockUserSettingService },
                { provide: Router, useValue: routerStub },
                { provide: GridMeasureService, useValue: mockGridMeasureService },
                { provide: ReminderService, useValue: mockReminderService },
                { provide: LockService, useValue: mockLockService },
                { provide: DaterangepickerConfig, useClass: DaterangepickerConfig },
                { provide: RoleAccessHelperService, useValue: roleAccessHelper },
                { provide: ToasterMessageService, useValue: toasterMessageService }
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(StatusChangesComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should call init', async(() => {
        spyOn((component as any), 'init').and.callThrough();
        spyOn(component, 'initAgGrid').and.callThrough();
        spyOn(component, 'retrieveData').and.callThrough();
        sessionContext.setCurrUser(USERS[1]);
        sessionContext.setUserAuthenticated(true);
        mockGridMeasureService.content = JSON.parse(JSON.stringify(STATUSCHANGES));
        mockReminderService.content = [];
        mockUserSettingService.content = {};
        const abstractComp: any = component; // used to access privates
        component.user = USERS[1];
        component.gridId = 'statusChanges';
        component.sortingState = {
            column: 'shorty',
            counter: 1,
            defaultState: true,
            isDesc: true
        };
        component.filteringSearchText = {
            branchId: 'filty',
            title: 'fix',
            statusId: 'foxy'
        };
        mockUserSettingService.savedUserSettings = {};

        abstractComp.saveSettings();
        fixture.detectChanges();
        component.ngOnInit();
        fixture.detectChanges();
        component.initAgGrid();
        fixture.detectChanges();
        fixture.whenStable().then(() => {
            fixture.detectChanges();
            expect((component as any).init).toHaveBeenCalled();
            expect(component.initAgGrid).toHaveBeenCalled();
            expect(component.retrieveData).toHaveBeenCalled();
        });
    }));

    it('should retrieveData (statuschanges) on init', async(() => {
        mockGridMeasureService.content = JSON.parse(JSON.stringify(STATUSCHANGES));
        mockReminderService.content = [];
        const abstractComp: any = component; // used to access privates
        component.user = USERS[1];
        component.gridId = 'statusChanges';
        component.sortingState = {
            column: 'shorty',
            counter: 1,
            defaultState: true,
            isDesc: true
        };
        component.filteringSearchText = {
            branchId: 'filty',
            title: 'fix',
            statusId: 'foxy'
        };
        mockUserSettingService.savedUserSettings = {};

        abstractComp.saveSettings();
        component.retrieveData();
        fixture.detectChanges();

        fixture.whenRenderingDone().then(() => {
            fixture.detectChanges();
            expect(component.statuschanges.length).toBe(2);
        });



    }));

    it('should raise an message on error in retrieveData (statuschanges)', fakeAsync(() => {
        spyOn(toasterMessageService, 'showError').and.callThrough();
        sessionContext.setUserAuthenticated(true);
        mockGridMeasureService.error = 'Error in GridmeasureService';
        const abstractComp: any = component; // used to access privates
        component.user = USERS[1];
        component.gridId = 'statusChanges';
        component.sortingState = {
            column: 'shorty',
            counter: 1,
            defaultState: true,
            isDesc: true
        };
        component.filteringSearchText = {
            branchId: 'filty',
            title: 'fix',
            statusId: 'foxy'
        };
        mockUserSettingService.savedUserSettings = {};

        abstractComp.saveSettings();
        component.retrieveData();
        fixture.detectChanges();
        tick();
        expect(toasterMessageService.showError).toHaveBeenCalled();

    }));

    it('should set statuschanges as empty array in retrieveData for unknown gridId', fakeAsync(() => {

        sessionContext.setUserAuthenticated(true);
        const abstractComp: any = component; // used to access privates
        component.user = USERS[1];
        component.gridId = null;

        mockUserSettingService.savedUserSettings = {};
        component.showSpinner = true;
        abstractComp.saveSettings();
        component.retrieveData();
        fixture.detectChanges();
        tick();
        expect(component.statuschanges.length).toBe(0);
        expect(component.showSpinner).toBeFalsy();
    }));

    it('should call changeAllSelection and set selected false', async(() => {
        component.selectAll = false;
        component.statuschanges = JSON.parse(JSON.stringify(STATUSCHANGES));
        fixture.detectChanges();
        spyOn(component, 'changeAllSelection').and.callThrough();

        fixture.detectChanges();

        component.changeAllSelection();
        expect(component.changeAllSelection).toHaveBeenCalled();
        expect(component.statuschanges[0].selected).toBeFalsy();
    }));

    it('should call changeAllSelection and set selected true', async(() => {
        component.selectAll = true;
        component.statuschanges = JSON.parse(JSON.stringify(STATUSCHANGES));
        fixture.detectChanges();
        spyOn(component, 'changeAllSelection').and.callThrough();

        fixture.detectChanges();

        component.changeAllSelection();
        expect(component.changeAllSelection).toHaveBeenCalled();
        expect(component.statuschanges[0].selected).toBeTruthy();
    }));


});
