/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/



import { TestBed, inject } from '@angular/core/testing';
import { StatusChangesAgGridConfiguration } from './status-changes-ag-grid-configuration';
import { SessionContext } from '../../common/session-context';
import { STATUSES } from '../../test-data/statuses';
import * as moment from 'moment';

describe('StatusChangesAgGridConfiguration', () => {
    let sessionContext: SessionContext;

    beforeEach(() => {
        sessionContext = new SessionContext();

      TestBed.configureTestingModule({
      });
    });

    it('should provide the correct functions', (() => {
        const configArray: any = StatusChangesAgGridConfiguration.createColumnDefs( sessionContext );

        const statusIdSegment = configArray[0];
        expect(statusIdSegment.field).toBe('statusId');
        sessionContext.setStatuses(STATUSES);
        expect(statusIdSegment.valueGetter({data: { statusId: 999 }})).toBeFalsy();
        expect(statusIdSegment.valueGetter({data: { statusId: 3 }})).toBe('beendet');

        const modDateSegment = configArray[1];
        expect(modDateSegment.field).toBe('modDate');
        expect(modDateSegment.valueFormatter({data: {modDate: null}})).toBe('');

    }));

});
