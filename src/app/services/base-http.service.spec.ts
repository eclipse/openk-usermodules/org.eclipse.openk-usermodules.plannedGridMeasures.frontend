/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { TestBed, fakeAsync, inject, tick } from '@angular/core/testing';
import { ErrorType } from '../common/enums';
import { Globals } from '../common/globals';
import { SessionContext } from '../common/session-context';
import { BaseHttpService, HttpCallInfo, HttpMethodEn } from './base-http.service';
import { HttpClient, HttpClientModule, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { ToasterMessageService } from './toaster-message.service';
import { MessageService } from 'primeng/api';

class MockResponse {
  status: number;
  data: any;

  json() {
    return this.data;
  }
}

describe('BaseHttpService', () => {
  class BaseHttpServiceMock extends BaseHttpService {
    public constructor(public toasterMessageService1: ToasterMessageService, public http: HttpClient) {
      super(toasterMessageService1, http);
    }
    public getBaseUrl(): string {
      return super.getBaseUrl();
    }
  }
  let sessionContext: SessionContext;
  let toasterMessageService;
  let messageService;

  beforeEach(() => {
    messageService = new MessageService();
    sessionContext = new SessionContext();
    sessionContext.clearStorage();
    toasterMessageService = new ToasterMessageService(sessionContext, messageService);
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [
        BaseHttpService,
        { provide: ToasterMessageService, useValue: toasterMessageService },
        { provide: SessionContext, useValue: sessionContext }
      ]
    });

  });

  it('should deliver the BaseUrl', () => {
    const bhs = new BaseHttpServiceMock(this.messageService, null);
    expect(bhs.getBaseUrl()).toBe(Globals.BASE_URL);
  });

  it('should provide a correct envelope with POST', inject([BaseHttpService], (baseservice: BaseHttpService) => {
    const callInfo = new HttpCallInfo('ServiceName', HttpMethodEn.post, 'testMeTester', { data: 'blabla' });
    const openService: any = baseservice;

    const env = openService.buildEnvelope(callInfo, []);

    expect(env.method).toBe(HttpMethodEn.post);
    expect(env.serviceName).toBe('ServiceName');
    expect(env.uriFragment).toBe('testMeTester');
    expect(env.payload).toBeTruthy();
    expect(env.headers.length).toBe(0);
  }));
  it('should provide a correct envelope with GET', inject([BaseHttpService], (baseservice: BaseHttpService) => {
    const callInfo = new HttpCallInfo('ServiceName', HttpMethodEn.get, 'testMeTester', { data: 'blabla' });
    const openService: any = baseservice;

    const env = openService.buildEnvelope(callInfo, []);

    expect(env.method).toBe(HttpMethodEn.get);
    expect(env.serviceName).toBe('ServiceName');
    expect(env.uriFragment).toBe('testMeTester');
    expect(env.payload).toBeUndefined();
    expect(env.headers.length).toBe(0);
  }));

  it('should map the headers correctly', inject([BaseHttpService], (baseservice: BaseHttpService) => {
    const openService: any = baseservice;
    sessionContext.setCurrSessionId('TOKKO');
    const headers: HttpHeaders = openService.createCommonHeaders(sessionContext);

    const micsEnvelopHeaders: any[] = openService.mapHeaders(headers);

    expect(micsEnvelopHeaders.find(x => x.attribute === 'Accept').value).toBe('application/json');
    expect(micsEnvelopHeaders.find(x => x.attribute === Globals.SESSION_TOKEN_TAG).value).toBe('TOKKO');
  }));


  it('should extract Data from a response correctly (302)', inject([BaseHttpService], fakeAsync((baseservice: BaseHttpService) => {
    let msgWasFired = false;
    const openService: any = baseservice;
    const response = new MockResponse();
    response.status = 302;
    response.data = 'payload';

    sessionContext.centralHttpResultCode$.subscribe(x => {
      expect(x).toBe(302);
      msgWasFired = true;
    });

    expect(openService.extractData(response, sessionContext).data).toBe('payload');
    tick();

    expect(msgWasFired).toBeTruthy();
  })));

  it('should extract Data from a response correctly (299)', inject([BaseHttpService], fakeAsync((baseservice: BaseHttpService) => {
    let msgWasFired = false;
    const openService: any = baseservice;
    const response = new MockResponse();
    response.status = 299;
    response.data = 'payload';

    sessionContext.centralHttpResultCode$.subscribe(x => {
      expect(x).toBe(299);
      msgWasFired = true;
    });

    expect(openService.extractData(response, sessionContext).data).toBe('payload');
    tick();

    expect(msgWasFired).toBeTruthy();
  })));


  it('should throw Exception on a response correctly (111)', inject([BaseHttpService], fakeAsync((baseservice: BaseHttpService) => {
    let msgWasFired = false;
    const openService: any = baseservice;
    const response = new MockResponse();
    response.status = 111;
    response.data = 'payload';

    sessionContext.centralHttpResultCode$.subscribe(x => {
      expect(x).toBe(111);
      msgWasFired = true;
    });

    expect(function () { openService.extractData(response, sessionContext); }).toThrowError();
    tick();

    expect(msgWasFired).toBeTruthy();
  })));

  it('should throw Exception on a response correctly (310)', inject([BaseHttpService], fakeAsync((baseservice: BaseHttpService) => {
    let msgWasFired = false;
    const openService: any = baseservice;
    const response = new MockResponse();
    response.status = 310;
    response.data = 'payload';

    sessionContext.centralHttpResultCode$.subscribe(x => {
      expect(x).toBe(310);
      msgWasFired = true;
    });

    expect(function () { openService.extractData(response, sessionContext); }).toThrowError();
    tick();

    expect(msgWasFired).toBeTruthy();
  })));

  it('should throw Exception on a null- response ', inject([BaseHttpService], fakeAsync((baseservice: BaseHttpService) => {
    let msgWasFired = false;
    const openService: any = baseservice;

    sessionContext.centralHttpResultCode$.subscribe(x => {
      expect(x).toBe(310);
      msgWasFired = true;
    });

    expect(function () { openService.extractData(null, sessionContext); }).toThrowError();
    tick();

    expect(msgWasFired).toBeFalsy();
  })));


  it('should extract the session id correctly', inject([BaseHttpService], (baseservice: BaseHttpService) => {
    const openService: any = baseservice;
    sessionContext.setCurrSessionId('untouched');

    openService.extractSessionId(null, sessionContext);
    expect(sessionContext.getCurrSessionId()).toBe('untouched');

    let headers = new HttpHeaders();

    headers = headers.append('Accept', 'application/json');
    headers = headers.append('content-Type', 'application/json');

    openService.extractSessionId(headers, sessionContext);
    expect(sessionContext.getCurrSessionId()).toBe('untouched');

    headers = headers.append(Globals.SESSION_TOKEN_TAG, 'hit me');

    openService.extractSessionId(headers, sessionContext);
    expect(sessionContext.getCurrSessionId()).toBe('hit me');



  }));

  it('should handle an text error promise correctly', inject([BaseHttpService], fakeAsync((baseservice: BaseHttpService) => {
    let msgWasFired = false;
    const openService: any = baseservice;
    const response = new MockResponse();
    response.status = 401;
    response.data = 'payload';

    openService.handleErrorPromise('texterror').subscribe(x => { },
      e => {
        expect(e).toBe('texterror');
        msgWasFired = true;
      });

    tick();

    expect(msgWasFired).toBeTruthy();


  })));

  it('should handle an 401 error promise correctly', inject([BaseHttpService], fakeAsync((baseservice: BaseHttpService) => {
    let msgWasFired = false;
    const openService: any = baseservice;
    const response = new HttpErrorResponse({
      status: 401, statusText: 'Bad Request'
    });

    openService.handleErrorPromise(response).subscribe(x => { },
      e => {
        expect(e).toBe(ErrorType.authentication);
        msgWasFired = true;
      });

    tick();

    expect(msgWasFired).toBeTruthy();
  })));

  it('should handle an 500 error promise correctly', inject([BaseHttpService], fakeAsync((baseservice: BaseHttpService) => {
    let msgWasFired = false;
    const openService: any = baseservice;
    const response = new HttpErrorResponse({ status: 500 });


    openService.handleErrorPromise(response).subscribe(x => { },
      e => {
        expect(e).toContain('500');
        msgWasFired = true;
      });

    tick();

    expect(msgWasFired).toBeTruthy();


  })));

  it('should handle an error with body promise correctly', inject([BaseHttpService], fakeAsync((baseservice: BaseHttpService) => {
    let msgWasFired = false;
    const openService: any = baseservice;
    const response = new HttpErrorResponse({
      status: 405, statusText: 'Method Not Allowed'
    });

    openService.handleErrorPromise(response).subscribe(x => { },
      e => {
        expect(e).toContain('405 - Method Not Allowed');
        msgWasFired = true;
      });

    tick();

    expect(msgWasFired).toBeTruthy();


  })));

  it('should handle an other error with message promise correctly', inject([BaseHttpService], fakeAsync((baseservice: BaseHttpService) => {
    let msgWasFired = false;
    const openService: any = baseservice;

    const response = { message: 'other error' };


    openService.handleErrorPromise(response).subscribe(x => { },
      e => {
        expect(e).toContain('other error');
        msgWasFired = true;
      });

    tick();

    expect(msgWasFired).toBeTruthy();


  })));



});

