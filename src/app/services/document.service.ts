/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Globals } from '../common/globals';
import { SessionContext } from '../common/session-context';
import { Document } from './../model/document';
import { BaseHttpService, HttpCallInfo, HttpMethodEn } from './base-http.service';

@Injectable()
export class DocumentService {


    constructor(
        private baseHttpService: BaseHttpService,
        private sessionContext: SessionContext) { }

    public uploadGridMeasureAttachments(gridmeasuereId: number, documentToUpload: Document): Observable<Document> {
        return this.baseHttpService.callService(
            new HttpCallInfo(Globals.GRID_MEASURES_SERVICE_NAME, HttpMethodEn.post, '/uploadGridMeasureAttachments/'
                + gridmeasuereId, documentToUpload), this.sessionContext);
    }

    public getGridMeasureAttachments(gridmeasuereId: number): Observable<Document[]> {
        return this.baseHttpService.callService(
            new HttpCallInfo(Globals.GRID_MEASURES_SERVICE_NAME, HttpMethodEn.get, '/getGridMeasureAttachments/'
                + gridmeasuereId, null), this.sessionContext);
    }

    public downloadGridMeasureAttachment(documentId: number): Observable<Document> {
        return this.baseHttpService.callService(
            new HttpCallInfo(Globals.GRID_MEASURES_SERVICE_NAME, HttpMethodEn.get, '/downloadGridMeasureAttachment/'
                + documentId, null), this.sessionContext);
    }

    public deleteGridMeasureAttachment(documentId: number) {
        return this.baseHttpService.callService(
            new HttpCallInfo(Globals.GRID_MEASURES_SERVICE_NAME, HttpMethodEn.delete, '/deleteGridMeasureAttachment/'
                + documentId, null), this.sessionContext);
    }

}
