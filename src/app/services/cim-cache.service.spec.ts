/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { TestBed, inject } from '@angular/core/testing';
import { BaseHttpService, HttpMethodEn } from './base-http.service';
import { CimCacheService } from './cim-cache.service';
import { MockBaseHttpService } from '../testing/mock-base-http.service';
import { SessionContext } from '../common/session-context';
import { Globals } from '../common/globals';

describe('CimCacheService', () => {
  let sessionContext: SessionContext;
  let mockedBaseHttpService: MockBaseHttpService;

  beforeEach(() => {
    sessionContext = new SessionContext();
    mockedBaseHttpService = new MockBaseHttpService();

    TestBed.configureTestingModule({
      providers: [
        CimCacheService,
        { provide: SessionContext, useValue: sessionContext },
        { provide: BaseHttpService, useValue: mockedBaseHttpService },
        SessionContext
      ]
    });
  });

  it('should be created', inject([CimCacheService], (service: CimCacheService) => {
    expect(service).toBeTruthy();
  }));

  it('should call get getRessourceTypes', inject([CimCacheService], (service: CimCacheService) => {
    mockedBaseHttpService.content = [];
    service.getRessourceTypes();

    expect(mockedBaseHttpService.lastHttpCallInfo.method).toBe(HttpMethodEn.get);
    expect(mockedBaseHttpService.lastHttpCallInfo.uriFragment).toBe('/electricity/dynamic-topology/power-system-resource-types?revision=1');
    expect(mockedBaseHttpService.lastHttpCallInfo.serviceName).toBe(Globals.CIM_CACHE_SERVICE);
  }));

  it('should call get getRessourcesWithType', inject([CimCacheService], (service: CimCacheService) => {
    mockedBaseHttpService.content = [];
    const ressourceType = '';
    service.getRessourcesWithType(ressourceType);

    expect(mockedBaseHttpService.lastHttpCallInfo.method).toBe(HttpMethodEn.get);
    expect(mockedBaseHttpService.lastHttpCallInfo.uriFragment)
      .toBe('/electricity/dynamic-topology/power-system-resources?revision=1&power-system-resource-types=' + ressourceType);
    expect(mockedBaseHttpService.lastHttpCallInfo.serviceName).toBe(Globals.CIM_CACHE_SERVICE);
  }));

  it('should call get getTopology', inject([CimCacheService], (service: CimCacheService) => {
    mockedBaseHttpService.content = [];
    service.getTopology();

    expect(mockedBaseHttpService.lastHttpCallInfo.method).toBe(HttpMethodEn.get);
    expect(mockedBaseHttpService.lastHttpCallInfo.uriFragment)
      .toBe('/electricity/dynamic-topology/topology?revision=1');
    expect(mockedBaseHttpService.lastHttpCallInfo.serviceName).toBe(Globals.CIM_CACHE_SERVICE);
  }));
});
