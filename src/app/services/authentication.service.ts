/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { SessionContext } from '../common/session-context';
import { BaseHttpService, HttpCallInfo, HttpMethodEn } from './base-http.service';
import { Globals } from '../common/globals';

@Injectable()
export class AuthenticationService {

    constructor(
        private baseHttpService: BaseHttpService,
        private sessionContext: SessionContext) { }

    public logout(): Observable<any> {
        return this.baseHttpService.callService(
            new HttpCallInfo(Globals.GRID_MEASURES_SERVICE_NAME, HttpMethodEn.get, '/logout', null), this.sessionContext);
    }
}
