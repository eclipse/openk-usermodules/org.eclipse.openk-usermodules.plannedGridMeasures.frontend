/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { Injectable, EventEmitter } from '@angular/core';
import { MessageService } from 'primeng/api';
import { ErrorType, ToasterButtonEventEn } from '../common/enums';
import { SessionContext } from '../common/session-context';
import { ToasterMessage } from '../common/banner-message';

export class MessageDefines {
  static MSG_LOG_IN_SUCCEEDED = 'LOG_IN_SUCCEEDED';
  static MSG_LOG_OFF = 'LOG_OFF';
}

@Injectable()
export class ToasterMessageService {

  public toasterEmitter$: EventEmitter<ToasterButtonEventEn> = new EventEmitter<ToasterButtonEventEn>();
  public loginLogoff$: EventEmitter<string> = new EventEmitter<string>();
  public errorOccured$: EventEmitter<ToasterMessage> = new EventEmitter<ToasterMessage>();

  constructor(private sessionContext: SessionContext,
    private messageService: MessageService) {

  }

  showSuccess(summaryText: string, detailText?: string) {
    this.messageService.add({ life: 6000, severity: 'success', summary: summaryText, detail: detailText });
  }

  showInfo(summaryText: string, detailText?: string) {
    this.messageService.add({ life: 6000, severity: 'info', summary: summaryText, detail: detailText });
  }

  showWarn(summaryText: string, detailText?: string) {
    this.messageService.add({ life: 6000, severity: 'warn', summary: summaryText, detail: detailText });
  }



  showError(errorType: ErrorType, location: string, detailText?: string) {
    let message = '';
    switch (errorType) {
      case ErrorType.create:
        message = 'Fehler beim Erstellen des Objekts ' + location + '.';
        break;
      case ErrorType.update:
        message = 'Fehler beim Aktualiseren des Objekts ' + location + '.';
        break;
      case ErrorType.delete:
        message = 'Fehler beim Löschen des Objekts ' + location + '.';
        break;
      case ErrorType.retrieve:
        message = 'Fehler beim Zugriff auf ' + location + '.';
        break;
      case ErrorType.upload:
        message = 'Fehler beim Hochladen der Datei in ' + location + '.';
        break;
      case ErrorType.locked:
        message = 'Eintrag ist gesperrt von ' + this.sessionContext.getUserMap().findAndRenderUser(location) + '.';
        break;
      case ErrorType.datedependency:
        message = 'Beginn der ersten geplanten Schrittsequenz muss vor dem Enddatum der geplanten Netzmaßnahme liegen.';
        break;
      case ErrorType.stornoLocked:
        message = 'Eintrag ist gesperrt. Bitte versuchen Sie es später noch einmal.';
        break;
      default:
        message = 'Es ist ein unbekannter Fehler aufgetreten.';
        break;
    }

    this.messageService.add({ severity: 'error', sticky: true, summary: message, detail: detailText });
  }

  showTopLeft(summaryText: string, detailText?: string) {
    this.messageService.add({ life: 6000, key: 'tl', severity: 'info', summary: summaryText, detail: detailText });
  }

  showTopCenter(summaryText: string, detailText?: string) {
    this.messageService.add({ life: 6000, key: 'tc', severity: 'warn', summary: summaryText, detail: detailText });
  }

  showSingleGMDeleteConfirm(summaryText: string, detailText?: string) {
    this.messageService.clear();
    this.messageService.add({ key: 'deletec', sticky: true, severity: 'warn', summary: summaryText, detail: detailText });
  }

  showUnlockConfirm(summaryText: string, detailText?: string) {
    this.messageService.clear();
    this.messageService.add({ key: 'unlockc', sticky: true, severity: 'warn', summary: summaryText, detail: detailText });
  }

  clear(tmp?: string) {
    if (tmp) {
      this.messageService.clear(tmp);
    } else {
      this.messageService.clear();
    }
  }

}
