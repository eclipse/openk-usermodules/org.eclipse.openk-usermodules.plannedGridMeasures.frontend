/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { Injectable } from '@angular/core';
import { SessionContext } from '../common/session-context';
import { BaseHttpService, HttpMethodEn, HttpCallInfo } from './base-http.service';
import { Observable } from 'rxjs/Observable';
import { Globals } from '../common/globals';
import { BackendSettings } from '../model/backend-settings';

@Injectable()
export class BackendSettingsService {

  constructor(
    private baseHttpService: BaseHttpService,
    private sessionContext: SessionContext) { }


  getBackendSettings(): Observable<BackendSettings> {
    return this.baseHttpService.callService(
      new HttpCallInfo(Globals.GRID_MEASURES_SERVICE_NAME, HttpMethodEn.get, '/getBackendSettings', null),
      this.sessionContext);
  }
}
