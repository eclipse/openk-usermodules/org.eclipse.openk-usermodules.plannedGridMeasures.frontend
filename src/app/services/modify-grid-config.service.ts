/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { Injectable } from '@angular/core';
import { GridConfig } from '../model/grid-config';
import { Observable } from 'rxjs';
import { BaseHttpService, HttpCallInfo, HttpMethodEn } from './base-http.service';
import { SessionContext } from '../common/session-context';
import { Globals } from '../common/globals';

@Injectable()
export class ModifyGridConfigService {

  constructor(
    private baseHttpService: BaseHttpService,
    private sessionContext: SessionContext) {
  }

  setGridConfig(modifyGridConfig: GridConfig): Observable<GridConfig> {
    return this.baseHttpService.callService(
      new HttpCallInfo(Globals.GRID_MEASURES_SERVICE_NAME, HttpMethodEn.get, '/setGridConfig/?' +
        'skipForApproval=' + modifyGridConfig.skipForApproval + '&' +
        'endAfterApproved=' + modifyGridConfig.endAfterApproved + '&' +
        'skipRequesting=' + modifyGridConfig.skipRequesting + '&' +
        'endAfterReleased=' + modifyGridConfig.endAfterReleased + '&' +
        'skipInWork=' + modifyGridConfig.skipInWork
        , null),
      this.sessionContext);
  }
}
