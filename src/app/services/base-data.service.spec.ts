/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { TestBed, inject } from '@angular/core/testing';
import { BaseHttpService, HttpMethodEn } from './base-http.service';
import { BaseDataService } from './base-data.service';
import { MockBaseHttpService } from '../testing/mock-base-http.service';
import { SessionContext } from '../common/session-context';

describe('Service: BaseData', () => {
  let sessionContext: SessionContext;
  let mockedBaseHttpService: MockBaseHttpService;

  beforeEach(() => {
    sessionContext = new SessionContext();
    mockedBaseHttpService = new MockBaseHttpService();

    TestBed.configureTestingModule({
      providers: [
        BaseDataService,
        { provide: SessionContext, useValue: sessionContext },
        { provide: BaseHttpService, useValue: mockedBaseHttpService },
        SessionContext
      ]
    });
  });

  it('should call getBranches', inject([BaseDataService], (service: BaseDataService) => {
    expect(service).toBeTruthy();

    service.getBranches();
    expect(mockedBaseHttpService.lastHttpCallInfo.method).toBe(HttpMethodEn.get);
    expect(mockedBaseHttpService.lastHttpCallInfo.uriFragment).toContain('getBranches');

  }));

  it('should call getBrancheLevels', inject([BaseDataService], (service: BaseDataService) => {
    expect(service).toBeTruthy();

    service.getBranchLevels(1);
    expect(mockedBaseHttpService.lastHttpCallInfo.method).toBe(HttpMethodEn.get);
    expect(mockedBaseHttpService.lastHttpCallInfo.uriFragment).toContain('getBranchLevels');

  }));

  it('should call getStatuses', inject([BaseDataService], (service: BaseDataService) => {
    expect(service).toBeTruthy();

    service.getStatuses();
    expect(mockedBaseHttpService.lastHttpCallInfo.method).toBe(HttpMethodEn.get);
    expect(mockedBaseHttpService.lastHttpCallInfo.uriFragment).toContain('getGmStatus');

  }));

  it('should call getCostCenters', inject([BaseDataService], (service: BaseDataService) => {
    expect(service).toBeTruthy();

    service.getCostCenters();
    expect(mockedBaseHttpService.lastHttpCallInfo.method).toBe(HttpMethodEn.get);
    expect(mockedBaseHttpService.lastHttpCallInfo.uriFragment).toContain('getCostCenters');

  }));

  it('should call getEmailAddressesFromTemplates', inject([BaseDataService], (service: BaseDataService) => {
    expect(service).toBeTruthy();

    service.getEmailAddressesFromTemplates();
    expect(mockedBaseHttpService.lastHttpCallInfo.method).toBe(HttpMethodEn.get);
    expect(mockedBaseHttpService.lastHttpCallInfo.uriFragment).toContain('getEmailAddressesFromTemplates');

  }));

  it('should call getEmailAddressesFromGridmeasures', inject([BaseDataService], (service: BaseDataService) => {
    expect(service).toBeTruthy();

    service.getEmailAddressesFromGridmeasures();
    expect(mockedBaseHttpService.lastHttpCallInfo.method).toBe(HttpMethodEn.get);
    expect(mockedBaseHttpService.lastHttpCallInfo.uriFragment).toContain('getMailAddressesFromGridmeasures');

  }));

  it('should call getTerritories', inject([BaseDataService], (service: BaseDataService) => {
    expect(service).toBeTruthy();

    service.getTerritories();
    expect(mockedBaseHttpService.lastHttpCallInfo.method).toBe(HttpMethodEn.get);
    expect(mockedBaseHttpService.lastHttpCallInfo.uriFragment).toContain('getTerritories');

  }));
});

