/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { HttpClient, HttpHeaders, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UUID } from 'angular2-uuid';
import { Observable } from 'rxjs/Observable';
import { ToasterMessage } from '../common/banner-message';
import { ErrorType } from '../common/enums';
import { Globals } from '../common/globals';
import { SessionContext } from '../common/session-context';
import { ToasterMessageService } from './toaster-message.service';

export enum HttpMethodEn {
    get = 'GET',
    post = 'POST',
    put = 'PUT',
    delete = 'DELETE'
}

export class HttpCallInfo {
    constructor(
        public serviceName: string,
        public method: HttpMethodEn,
        public uriFragment: string,
        public payload: any
    ) { }
}

class MicsEnvelopeHeader {
    constructor(
        public attribute: string,
        public value: string) { }
}

class MicsEnvelope {
    public serviceName: string;
    public method: string;
    public uriFragment: string;
    public payload: string;
    public headers: MicsEnvelopeHeader[];
}

export interface BaseHttpServiceInterface {
    callService(callInfo: HttpCallInfo, sessionContext: SessionContext);
}

@Injectable()
export class BaseHttpService implements BaseHttpServiceInterface {

    constructor(protected toasterMessageService: ToasterMessageService,
        protected http: HttpClient) {
    }

    public callService(callInfo: HttpCallInfo, sessionContext: SessionContext, providedHeaders: HttpHeaders = null): Observable<any> {
        let headers: HttpHeaders;
        if (providedHeaders == null) {
            headers = this.createCommonHeaders(sessionContext);
        } else {
            headers = providedHeaders;
        }

        const acceptValue = headers.get('Accept');

        let responseType;
        if (acceptValue.includes('xml')) {
            responseType = 'text';
        } else {
            responseType = 'json';
        }

        const body = JSON.stringify(this.buildEnvelope(callInfo, headers));
        return this.http.post(this.getBaseUrl() + '/dispatch', body, { headers: headers, responseType: responseType })
            .catch(error => {
                return this.handleErrorPromise(error);
            });

    }


    private buildEnvelope(callInfo: HttpCallInfo, headers: HttpHeaders): MicsEnvelope {
        const envelope = new MicsEnvelope();
        envelope.method = callInfo.method;
        envelope.serviceName = callInfo.serviceName;
        envelope.uriFragment = callInfo.uriFragment;
        if (callInfo.method === HttpMethodEn.post || callInfo.method === HttpMethodEn.put) {
            envelope.payload = btoa(encodeURIComponent(JSON.stringify(callInfo.payload)));
        }
        envelope.headers = this.mapHeaders(headers);
        return envelope;
    }

    private mapHeaders(headers: HttpHeaders): MicsEnvelopeHeader[] {
        const mappedHeaders = new Array<MicsEnvelopeHeader>();

        if (headers.keys().length > 0) {
            const keys = headers.keys();
            keys.forEach(key => {
                mappedHeaders.push(new MicsEnvelopeHeader(key, headers.get(key)));
            });
        }
        return mappedHeaders;
    }

    protected getBaseUrl(): string {
        return Globals.BASE_URL;
    }

    public createCommonHeaders(sessionContext: SessionContext): HttpHeaders {
        let headers = new HttpHeaders();
        headers = headers.append('Accept', 'application/json');
        headers = headers.append('content-Type', 'application/json');
        headers = headers.append('Access-Control-Allow-Origin', '*');
        headers = headers.set('Authorization', 'Bearer ' + sessionContext.getAccessToken());
        headers = headers.append('unique-TAN', UUID.UUID());
        if (sessionContext.getCurrSessionId() !== null) {
            headers = headers.append(Globals.SESSION_TOKEN_TAG, sessionContext.getCurrSessionId());
        }

        return headers;
    }

    protected extractData(res: HttpResponse<any>, _sessContext: SessionContext) {
        // let the interested 'people' know about our result
        _sessContext.centralHttpResultCode$.emit(res.status);

        if (res.status !== 302 && (res.status < 200 || res.status >= 300)) {
            throw new Error('Bad response status: ' + res.status);
        }

        console.log(res.type);
        const data = res;
        return data || {};
    }

    protected extractSessionId(headers: HttpHeaders, sessionContext: SessionContext) {
        if (headers != null) {
            if (headers.has(Globals.SESSION_TOKEN_TAG)) {
                sessionContext.setCurrSessionId(headers.get(Globals.SESSION_TOKEN_TAG));
            }
        }
    }

    protected handleErrorPromise(error: any) {
        let errMsg: string;
        let body = null;

        if (error instanceof HttpErrorResponse) {
            if (error.status === 401 && this.toasterMessageService) {
                const bannerMessage = new ToasterMessage();
                bannerMessage.errorType = ErrorType.authentication;
                this.toasterMessageService.errorOccured$.emit(bannerMessage);
                return Observable.throw(ErrorType.authentication);
            }
            body = error || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }

        console.error(errMsg);
        return Observable.throw(errMsg);
    }
}
