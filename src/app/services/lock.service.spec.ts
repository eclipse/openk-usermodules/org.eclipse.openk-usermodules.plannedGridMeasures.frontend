/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { BaseHttpService, HttpMethodEn } from './base-http.service';
import { TestBed, inject } from '@angular/core/testing';
import { MockBaseHttpService } from '../testing/mock-base-http.service';
import { SessionContext } from '../common/session-context';
import { LOCKS } from './../test-data/locks';
import { LockService } from './lock.service';
import { Lock } from './../model/lock';
import { Globals } from '../common/globals';

describe('Service: Lock', () => {
  let sessionContext: SessionContext;
  let mockedBaseHttpService: MockBaseHttpService;

  beforeEach(() => {
    sessionContext = new SessionContext();
    mockedBaseHttpService = new MockBaseHttpService();
    const lock: Lock = LOCKS[0];
    const gmId = 2;
    const lockId = 1;

    TestBed.configureTestingModule({
      providers: [
        LockService,
        { provide: SessionContext, useValue: sessionContext },
        { provide: BaseHttpService, useValue: mockedBaseHttpService },
        SessionContext
      ]
    });
  });

  it('should call storeLock', inject([LockService], (service: LockService) => {
    expect(service).toBeTruthy();

    service.storeLock(this.lock);
    expect(mockedBaseHttpService.lastHttpCallInfo.method).toBe(HttpMethodEn.put);
    expect(mockedBaseHttpService.lastHttpCallInfo.uriFragment).toContain('createLock');

  }));

  it('should call checkLock', inject([LockService], (service: LockService) => {
    expect(service).toBeTruthy();

    const returnValue = service.checkLock(this.gmId, 'gridmeasure');

    expect(mockedBaseHttpService.lastHttpCallInfo.method).toBe(HttpMethodEn.get);
    expect(mockedBaseHttpService.lastHttpCallInfo.uriFragment).toContain('checkLock');

  }));

  it('should call deleteLock', inject([LockService], (service: LockService) => {
    expect(service).toBeTruthy();

    service.deleteLock(this.gmId, 'gridmeasure', Globals.NO_FORCE_UNLOCK);
    expect(mockedBaseHttpService.lastHttpCallInfo.method).toBe(HttpMethodEn.delete);
    expect(mockedBaseHttpService.lastHttpCallInfo.uriFragment).toContain('deleteLock');

  }));

});
