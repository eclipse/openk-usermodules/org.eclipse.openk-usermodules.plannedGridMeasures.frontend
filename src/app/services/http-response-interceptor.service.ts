/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { Injectable } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToasterMessage } from '../common/banner-message';
import { ErrorType } from '../common/enums';
import { ToasterMessageService, MessageDefines } from './toaster-message.service';

@Injectable()
export class HttpResponseInterceptorService {

  constructor(
    private messageService: ToasterMessageService,
    private router: Router,
    private activatedRoute: ActivatedRoute) { this.init(); }

  private init(): void {
    this.subscribeToMessageService();
  }

  private subscribeToMessageService() {
    this.messageService.errorOccured$.subscribe((errorMessage: ToasterMessage) => {
      if ((errorMessage.errorType === ErrorType.authentication)) {
        if (this.router.url.includes('loggedout')) {
          return;
        }

        const fwdUrl = this.activatedRoute.snapshot.queryParams['fwdUrl'];
        if (fwdUrl) {
          window.location.href = fwdUrl;
        } else {
          this.router.navigate(['/loggedout']);
        }
        this.messageService.loginLogoff$.emit(MessageDefines.MSG_LOG_OFF);
      }
    });
  }
}
