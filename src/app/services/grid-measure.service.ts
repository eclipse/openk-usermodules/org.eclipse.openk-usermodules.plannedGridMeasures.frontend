/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { Globals } from '../common/globals';
import { SessionContext } from '../common/session-context';
import { GridMeasure } from '../model/grid-measure';
import { StatusChange } from '../model/status-change';
import { BaseHttpService, HttpCallInfo, HttpMethodEn } from './base-http.service';
import { StatusMainFilter } from '../model/status-main-filter';
import { CalendarEntry } from '../model/calendar-entry';

@Injectable()
export class GridMeasureService {


    constructor(
        private baseHttpService: BaseHttpService,
        private sessionContext: SessionContext) {
    }

    storeGridMeasure(gridMeasure: GridMeasure): Observable<GridMeasure> {
        return this.baseHttpService.callService(
            new HttpCallInfo(Globals.GRID_MEASURES_SERVICE_NAME, HttpMethodEn.put, '/storeGridMeasure', gridMeasure),
            this.sessionContext);
    }

    getGridMeasures(statusMainFilter?: StatusMainFilter): Observable<GridMeasure[]> {
        const isMainFilterSetAndActive = statusMainFilter;
        return this.baseHttpService.callService(
            new HttpCallInfo(Globals.GRID_MEASURES_SERVICE_NAME, HttpMethodEn.post, '/getGridMeasures',
                isMainFilterSetAndActive ? statusMainFilter.item : null),
            this.sessionContext);
    }

    getGridMeasure(id: number): Observable<GridMeasure> {
        return this.baseHttpService.callService(
            new HttpCallInfo(Globals.GRID_MEASURES_SERVICE_NAME, HttpMethodEn.get, '/getGridMeasure/' + id, null),
            this.sessionContext);
    }

    getHistoricalStatusChanges(id: number): Observable<StatusChange[]> {

        return this.baseHttpService.callService(
            new HttpCallInfo(Globals.GRID_MEASURES_SERVICE_NAME, HttpMethodEn.get, '/getHistoricalStatusChanges/' + id, null),
            this.sessionContext);
    }

    getAffectedResourcesDistinct(): Observable<string[]> {
        return this.baseHttpService.callService(
            new HttpCallInfo(Globals.GRID_MEASURES_SERVICE_NAME, HttpMethodEn.get, '/getAffectedResourcesDistinct/', null),
            this.sessionContext);
    }

    getResponsiblesOnSiteFromSingleGridmeasures(): Observable<string[]> {
        return this.baseHttpService.callService(
            new HttpCallInfo(Globals.GRID_MEASURES_SERVICE_NAME, HttpMethodEn.get, '/getResponsiblesOnSiteFromSingleGridmeasures', null),
            this.sessionContext);
    }

    getNetworkControlsFromSingleGridmeasures(): Observable<string[]> {
        return this.baseHttpService.callService(
            new HttpCallInfo(Globals.GRID_MEASURES_SERVICE_NAME, HttpMethodEn.get, '/getNetworkControlsFromSingleGridmeasures', null),
            this.sessionContext);
    }

    getUserDepartmentsCreated(): Observable<string[]> {
        return this.baseHttpService.callService(
            new HttpCallInfo(Globals.GRID_MEASURES_SERVICE_NAME, HttpMethodEn.get,
                '/getUserDepartmentsCreated', null), this.sessionContext);
    }

    getUserDepartmentsModified(): Observable<string[]> {
        return this.baseHttpService.callService(
            new HttpCallInfo(Globals.GRID_MEASURES_SERVICE_NAME, HttpMethodEn.get,
                '/getUserDepartmentsModified', null), this.sessionContext);
    }

    getUserDepartmentsResponsibleOnSite(): Observable<string[]> {
        return this.baseHttpService.callService(
            new HttpCallInfo(Globals.GRID_MEASURES_SERVICE_NAME, HttpMethodEn.get,
                '/getUserDepartmentsResponsibleOnSite', null), this.sessionContext);
    }

    getCalender(): Observable<CalendarEntry[]> {
        return this.baseHttpService.callService(
            new HttpCallInfo(Globals.GRID_MEASURES_SERVICE_NAME, HttpMethodEn.get, '/getCalender', null), this.sessionContext);
    }
}
