/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { Injectable, EventEmitter } from '@angular/core';
import { Lock } from '../model/lock';
import { LockService } from './lock.service';
import { SessionContext } from '../common/session-context';
import { ErrorType } from '../common/enums';
import { isNullOrUndefined } from 'util';
import { Globals } from '../common/globals';
import { ToasterMessageService } from './toaster-message.service';

@Injectable()
export class LockHelperService {

  constructor(private lockService: LockService,
    private sessionContext: SessionContext,
    private toasterMessageService: ToasterMessageService) { }

  checkLockedByUser(measureId: number, lockType: string, lockedByOtherEmitter$: EventEmitter<boolean>) {
    this.lockService.checkLock(measureId, lockType).subscribe((lock: Lock) => {
      const isLockedByOther = this.checkLock(lock);
      lockedByOtherEmitter$.emit(isLockedByOther);
    },
      error => {
        this.toasterMessageService.showError(ErrorType.locked, 'lockService checkLock', error);
        lockedByOtherEmitter$.emit(true);
      });
  }

  private checkLock(lock: Lock): boolean {
    let isLockedByOther = true;
    if (!lock || !lock.username || lock.username === '' || lock.username === 'No lock') {
      isLockedByOther = false;
    } else if (lock.username !== '' && lock.username !== this.sessionContext.getCurrUser().username) {
      this.toasterMessageService.showUnlockConfirm('Eintrag ist gesperrt von ' +
        this.sessionContext.getUserMap().findAndRenderUser(lock.username) + '.');
      isLockedByOther = true;
    } else if (lock.username === this.sessionContext.getCurrUser().username) {
      isLockedByOther = false;
    }

    return isLockedByOther;
  }

  createLock(gridmeasureId: number, lockType: string, createLockEmitter$: EventEmitter<boolean>) {
    const lock = new Lock();
    lock.key = gridmeasureId;
    lock.username = this.sessionContext.getCurrUser().username;
    lock.info = lockType;
    this.lockService.storeLock(lock).subscribe(resp => {
      this.emitIfSet(true, createLockEmitter$);
    },
      error => {
        this.emitIfSet(false, createLockEmitter$);
        this.toasterMessageService.showError(ErrorType.create, 'Sperre (Maßnahme {' + gridmeasureId + '})');
        console.log(error);
      });
  }

  deleteLock(measureId: number, lockType: string, force: boolean, deleteLockEmitter$: EventEmitter<boolean>) {
    if (!isNullOrUndefined(measureId)) {
      const forceText = force ? Globals.FORCE_UNLOCK : Globals.NO_FORCE_UNLOCK;
      this.lockService.deleteLock(measureId, lockType, forceText).subscribe(resp => {
        this.emitIfSet(true, deleteLockEmitter$);
      },
        error => {
          this.emitIfSet(false, deleteLockEmitter$);
          console.log('Fehler beim Aufheben der Sperre (Maßnahme {' + measureId + '})');
        });
    }
  }

  emitIfSet(msg: boolean, emitter: EventEmitter<boolean>) {
    if (emitter) {
      emitter.emit(msg);
    }
  }

}
