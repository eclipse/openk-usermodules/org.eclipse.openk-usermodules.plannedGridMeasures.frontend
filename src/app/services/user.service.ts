/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { BaseHttpService, HttpCallInfo, HttpMethodEn } from './base-http.service';
import { SessionContext } from '../common/session-context';
import { Globals } from '../common/globals';
import { User } from '../model/user';


@Injectable()
export class UserService {


    constructor(
        private baseHttpService: BaseHttpService,
        private sessionContext: SessionContext) { }

    public getUsers(): Observable<User[]> {
        return this.baseHttpService.callService(
            new HttpCallInfo(Globals.AUTH_AND_AUTH_SERVICE_NAME, HttpMethodEn.get, '/users', null), this.sessionContext);
    }

}
