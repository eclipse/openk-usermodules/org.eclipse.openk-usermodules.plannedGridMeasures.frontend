/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { TestBed, inject, async, fakeAsync, tick } from '@angular/core/testing';
import { AbstractMockObservableService } from '..//testing/abstract-mock-observable.service';
import { Lock } from '../model/lock';
import { LockHelperService } from './lock-helper.service';
import { LockService } from './lock.service';
import { SessionContext } from '../common/session-context';
import { EventEmitter } from '@angular/core';
import { USERS } from '../test-data/users';
import { UserMap } from '../common/user-map';
import { ToasterMessageService } from './toaster-message.service';
import { MessageService } from 'primeng/api';

describe('LockHelperService', () => {

  class MockLockService extends AbstractMockObservableService {
    checkLock(key: number, info: string) {
      return this;
    }
    storeLock(lock: Lock) {
      return this;
    }

    deleteLock(key: number, info: string) {
      return this;
    }
  }

  let lockHelperService: LockHelperService;
  let lockService: MockLockService;
  let sessionContext: SessionContext;
  let toasterMessageService: ToasterMessageService;
  let messageService: MessageService;

  beforeEach(async(() => {

    messageService = new MessageService();
    lockService = new MockLockService();
    sessionContext = new SessionContext();
    sessionContext.userMap = new UserMap(USERS);
    toasterMessageService = new ToasterMessageService(sessionContext, messageService);

    TestBed.configureTestingModule({
      providers: [
        LockHelperService,
        { provide: LockService, useValue: lockService },
        { provide: SessionContext, useValue: sessionContext },
        { provide: ToasterMessageService, useValue: toasterMessageService },
        { provide: MessageService, useValue: messageService }
      ]
    });
  }));


  beforeEach(inject([LockHelperService], (service: LockHelperService) => {
    lockHelperService = service;
  }));

  it('should be created', fakeAsync(() => {
    expect(lockHelperService).toBeTruthy();
  }));

  it('should fail if service does respond empty', fakeAsync(() => {
    lockService.content = {};
    let serviceResponded = false;
    const lockedByOtherEmitter$ = new EventEmitter<boolean>();

    lockedByOtherEmitter$.subscribe(isLocked => {
      expect(isLocked).toBe(false);
      serviceResponded = true;
    });

    tick();

    lockHelperService.checkLockedByUser(5, 'testType', lockedByOtherEmitter$);

    expect(serviceResponded).toBeTruthy();
  }));


  it('should fail if service responds with error', fakeAsync(() => {
    spyOn(toasterMessageService, 'showError').and.callThrough();
    lockService.error = 'error';
    let serviceResponded = false;
    const lockedByOtherEmitter$ = new EventEmitter<boolean>();

    lockedByOtherEmitter$.subscribe(isLocked => {
      expect(isLocked).toBe(true);
      serviceResponded = true;
    });

    tick();

    lockHelperService.checkLockedByUser(5, 'testType', lockedByOtherEmitter$);

    expect(toasterMessageService.showError).toHaveBeenCalled();
    expect(serviceResponded).toBeTruthy();
  }));

  it('should return locked if service responds with different user', fakeAsync(() => {
    sessionContext.setCurrUser({ username: 'tom' });
    lockService.content = { id: 8888, key: 'testType', username: 'paule', info: 'testType' };
    let serviceResponded = false;
    const lockedByOtherEmitter$ = new EventEmitter<boolean>();

    lockedByOtherEmitter$.subscribe(isLocked => {
      expect(isLocked).toBe(true);
      serviceResponded = true;
    });

    tick();

    lockHelperService.checkLockedByUser(5, 'testType', lockedByOtherEmitter$);

    expect(serviceResponded).toBeTruthy();
  }));

  it('should return correct value when calling checkLock', fakeAsync(() => {
    spyOn(toasterMessageService, 'showUnlockConfirm').and.callThrough();

    sessionContext.setCurrUser({ username: 'tom' });
    const lockedByOtherEmitter$ = new EventEmitter<boolean>();

    expect((lockHelperService as any).checkLock({ id: 8888, key: 'testType', username: 'paule', info: 'testType' }))
      .toBe(true);

    expect((lockHelperService as any).checkLock({ id: 8888, key: 'testType', username: 'jakub', info: 'testType' }))
      .toBe(true);
    expect((lockHelperService as any).checkLock(null)).toBe(false);
    expect((lockHelperService as any).checkLock({})).toBe(false);
    expect((lockHelperService as any).checkLock({ id: 8888, key: 'testType', username: 'No lock', info: 'testType' }))
      .toBe(false);

    expect((lockHelperService as any).checkLock({ id: 8888, key: 'testType', username: 'tom', info: 'testType' }))
      .toBe(false);

    expect(toasterMessageService.showUnlockConfirm).toHaveBeenCalledTimes(2);

  }));


  it('should create a lock correctly', fakeAsync(() => {
    sessionContext.setCurrUser({ username: 'tom' });
    lockService.content = {};

    spyOn(lockService, 'storeLock').and.callThrough();

    tick();

    lockHelperService.createLock(5, 'testType', null);

    expect(lockService.storeLock).toHaveBeenCalled();
  }));


  it('should behave correctly when create a lock fails', fakeAsync(() => {
    lockService.error = 'error';

    spyOn(lockService, 'storeLock').and.callThrough();
    spyOn(toasterMessageService, 'showError').and.callThrough();

    tick();

    lockHelperService.createLock(5, 'testType', null);

    expect(lockService.storeLock).toHaveBeenCalled();
    expect(toasterMessageService.showError).toHaveBeenCalled();

  }));

  it('should delete a lock correctly', fakeAsync(() => {
    lockService.content = {};

    spyOn(lockService, 'deleteLock').and.callThrough();

    tick();

    lockHelperService.deleteLock(5, 'testType', false, null);

    expect(lockService.deleteLock).toHaveBeenCalled();
  }));


  it('should behave correctly when delete lock fails', fakeAsync(() => {
    lockService.error = 'error';

    spyOn(lockService, 'deleteLock').and.callThrough();

    tick();

    lockHelperService.deleteLock(5, 'testType', false, null);

    expect(lockService.deleteLock).toHaveBeenCalled();
  }));

  it('should not call lockService delete lock when given id is undefined', fakeAsync(() => {
    lockService.error = 'error';

    spyOn(lockService, 'deleteLock').and.callThrough();

    tick();

    lockHelperService.deleteLock(undefined, 'testType', false, null);

    expect(lockService.deleteLock).toHaveBeenCalledTimes(0);
  }));

});
