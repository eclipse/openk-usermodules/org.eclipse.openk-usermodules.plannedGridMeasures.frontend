/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { TestBed, inject } from '@angular/core/testing';
import { ModifyGridConfigService } from './modify-grid-config.service';
import { SessionContext } from '../common/session-context';
import { BaseHttpService } from './base-http.service';
import { MockBaseHttpService } from '../testing/mock-base-http.service';

describe('ModifyGridConfigService', () => {
  let sessionContext: SessionContext;
  let mockedBaseHttpService: MockBaseHttpService;
  beforeEach(() => {
    sessionContext = new SessionContext();
    mockedBaseHttpService = new MockBaseHttpService();

    TestBed.configureTestingModule({
      providers: [ModifyGridConfigService,
        { provide: SessionContext, useValue: sessionContext },
        { provide: BaseHttpService, useValue: mockedBaseHttpService },
        SessionContext
      ]
    });
  });

  it('should be created', inject([ModifyGridConfigService], (service: ModifyGridConfigService) => {
    expect(service).toBeTruthy();
  }));
});
