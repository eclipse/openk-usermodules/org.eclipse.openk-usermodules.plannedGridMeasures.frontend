/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { TestBed, inject } from '@angular/core/testing';

import { ToasterMessageService } from './toaster-message.service';
import { SessionContext } from '../common/session-context';
import { MessageService } from 'primeng/api';
import { ErrorType } from '../common/enums';
import { UserMap } from '../common/user-map';
import { USERS } from '../test-data/users';

describe('ToasterMessageService', () => {
  let sessionContext: SessionContext;
  beforeEach(() => {
    sessionContext = new SessionContext();
    sessionContext.userMap = new UserMap(USERS);
    TestBed.configureTestingModule({
      providers: [ToasterMessageService,
        { provide: SessionContext, useValue: sessionContext },
        MessageService]
    });
  });

  it('should be created', inject([ToasterMessageService], (service: ToasterMessageService) => {
    expect(service).toBeTruthy();
  }));

  it('should can call showSuccess', inject([ToasterMessageService], (service: ToasterMessageService) => {
    spyOn(service, 'showSuccess').and.callThrough();
    service.showSuccess('testLoc', 'details');
    expect(service.showSuccess).toHaveBeenCalled();
  }));

  it('should can call showInfo', inject([ToasterMessageService], (service: ToasterMessageService) => {
    spyOn(service, 'showInfo').and.callThrough();
    service.showInfo('testLoc', 'details');
    expect(service.showInfo).toHaveBeenCalled();
  }));

  it('should can call showWarn', inject([ToasterMessageService], (service: ToasterMessageService) => {
    spyOn(service, 'showWarn').and.callThrough();
    service.showWarn('testLoc', 'details');
    expect(service.showWarn).toHaveBeenCalled();
  }));

  it('should can call showTopLeft', inject([ToasterMessageService], (service: ToasterMessageService) => {
    spyOn(service, 'showTopLeft').and.callThrough();
    service.showTopLeft('testLoc', 'details');
    expect(service.showTopLeft).toHaveBeenCalled();
  }));

  it('should can call showTopCenter', inject([ToasterMessageService], (service: ToasterMessageService) => {
    spyOn(service, 'showTopCenter').and.callThrough();
    service.showTopCenter('testLoc', 'details');
    expect(service.showTopCenter).toHaveBeenCalled();
  }));

  it('should can call showSingleGMDeleteConfirm', inject([ToasterMessageService], (service: ToasterMessageService) => {
    spyOn(service, 'showSingleGMDeleteConfirm').and.callThrough();
    service.showSingleGMDeleteConfirm('testLoc', 'details');
    expect(service.showSingleGMDeleteConfirm).toHaveBeenCalled();
  }));

  it('should can call showUnlockConfirm', inject([ToasterMessageService], (service: ToasterMessageService) => {
    spyOn(service, 'showUnlockConfirm').and.callThrough();
    service.showUnlockConfirm('testLoc', 'details');
    expect(service.showUnlockConfirm).toHaveBeenCalled();
  }));

  it('should can call clear without key input', inject([ToasterMessageService], (service: ToasterMessageService) => {
    spyOn(service, 'clear').and.callThrough();
    service.clear();
    expect(service.clear).toHaveBeenCalled();
  }));

  it('should can call clear with key input', inject([ToasterMessageService], (service: ToasterMessageService) => {
    spyOn(service, 'clear').and.callThrough();
    service.clear('c');
    expect(service.clear).toHaveBeenCalledWith('c');
  }));

  it('should can call showError for delete', inject([ToasterMessageService], (service: ToasterMessageService) => {
    spyOn(service, 'showError').and.callThrough();
    service.showError(ErrorType.delete, 'testLoc');
    expect(service.showError).toHaveBeenCalled();
  }));

  it('should can call showError for authentication', inject([ToasterMessageService], (service: ToasterMessageService) => {
    spyOn(service, 'showError').and.callThrough();
    service.showError(ErrorType.authentication, 'testLoc');
    expect(service.showError).toHaveBeenCalled();
  }));


  it('should can call showError for create', inject([ToasterMessageService], (service: ToasterMessageService) => {
    spyOn(service, 'showError').and.callThrough();
    service.showError(ErrorType.create, 'testLoc');
    expect(service.showError).toHaveBeenCalled();
  }));

  it('should can call showError for datedependency', inject([ToasterMessageService], (service: ToasterMessageService) => {
    spyOn(service, 'showError').and.callThrough();
    service.showError(ErrorType.datedependency, 'testLoc');
    expect(service.showError).toHaveBeenCalled();
  }));


  it('should can call showError for locked', inject([ToasterMessageService], (service: ToasterMessageService) => {
    sessionContext.setUserAuthenticated(true);
    spyOn(service, 'showError').and.callThrough();
    service.showError(ErrorType.locked, 'testLoc');
    expect(service.showError).toHaveBeenCalled();
  }));

  it('should can call showError for retrieve', inject([ToasterMessageService], (service: ToasterMessageService) => {
    spyOn(service, 'showError').and.callThrough();
    service.showError(ErrorType.retrieve, 'testLoc');
    expect(service.showError).toHaveBeenCalled();
  }));


  it('should can call showError for stornoLocked', inject([ToasterMessageService], (service: ToasterMessageService) => {
    spyOn(service, 'showError').and.callThrough();
    service.showError(ErrorType.stornoLocked, 'testLoc');
    expect(service.showError).toHaveBeenCalled();
  }));

  it('should can call showError for update', inject([ToasterMessageService], (service: ToasterMessageService) => {
    spyOn(service, 'showError').and.callThrough();
    service.showError(ErrorType.update, 'testLoc');
    expect(service.showError).toHaveBeenCalled();
  }));

  it('should can call showError for upload', inject([ToasterMessageService], (service: ToasterMessageService) => {
    spyOn(service, 'showError').and.callThrough();
    service.showError(ErrorType.upload, 'testLoc');
    expect(service.showError).toHaveBeenCalled();
  }));

});
