/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { Globals } from '../common/globals';
import { SessionContext } from '../common/session-context';
import { Status } from '../model/status';
import { Branch } from './../model/branch';
import { CostCenter } from './../model/cost-center';
import { BaseHttpService, HttpCallInfo, HttpMethodEn } from './base-http.service';
import { BranchLevel } from '../model/branch-level';
import { Territory } from '../model/territory';

@Injectable()
export class BaseDataService {

  constructor(
    private baseHttpService: BaseHttpService,
    private sessionContext: SessionContext) {
  }

  public getBranches(): Observable<Branch[]> {
    return this.baseHttpService.callService(
      new HttpCallInfo(Globals.GRID_MEASURES_SERVICE_NAME, HttpMethodEn.get, '/getBranches', null), this.sessionContext);
  }

  public getBranchLevels(branchId: number): Observable<BranchLevel[]> {
    return this.baseHttpService.callService(
      new HttpCallInfo(Globals.GRID_MEASURES_SERVICE_NAME, HttpMethodEn.get, '/getBranchLevelsByBranch/' + branchId, null),
      this.sessionContext);
  }

  public getStatuses(): Observable<Status[]> {
    return this.baseHttpService.callService(
      new HttpCallInfo(Globals.GRID_MEASURES_SERVICE_NAME, HttpMethodEn.get, '/getGmStatus', null), this.sessionContext);
  }

  public getCostCenters(): Observable<CostCenter[]> {
    return this.baseHttpService.callService(
      new HttpCallInfo(Globals.GRID_MEASURES_SERVICE_NAME, HttpMethodEn.get, '/getCostCenters', null), this.sessionContext);
  }

  public getEmailAddressesFromTemplates(): Observable<string[]> {
    return this.baseHttpService.callService(
      new HttpCallInfo(Globals.GRID_MEASURES_SERVICE_NAME, HttpMethodEn.get, '/getEmailAddressesFromTemplates', null), this.sessionContext);
  }

  public getEmailAddressesFromGridmeasures(): Observable<string[]> {
    return this.baseHttpService.callService(
      new HttpCallInfo(Globals.GRID_MEASURES_SERVICE_NAME, HttpMethodEn.get, '/getMailAddressesFromGridmeasures', null),
      this.sessionContext);
  }

  public getTerritories(): Observable<Territory[]> {
    return this.baseHttpService.callService(
      new HttpCallInfo(Globals.GRID_MEASURES_SERVICE_NAME, HttpMethodEn.get, '/getTerritories', null), this.sessionContext);
  }
}

