/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { BaseHttpService, HttpMethodEn } from './base-http.service';
import { TestBed, async, inject } from '@angular/core/testing';
import { UserSettingsService } from './user-settings.service';
import { AbstractMockObservableService } from '../testing/abstract-mock-observable.service';
import { MockBaseHttpService } from '../testing/mock-base-http.service';
import { SessionContext } from '../common/session-context';
import { UserSettings } from '../model/user-settings';

describe('Service: UserSettings', () => {
  class MockService extends AbstractMockObservableService {

    getUserSettings() {
      return this;
    }
  }
  let sessionContext: SessionContext;
  let mockService;
  let mockedBaseHttpService;

  beforeEach(async(() => {
    sessionContext = new SessionContext();
    mockedBaseHttpService = new MockBaseHttpService();
    mockService = new MockService();
    TestBed.configureTestingModule({
      providers: [
        UserSettingsService,
        SessionContext,
        { provide: SessionContext, useValue: sessionContext },
        { provide: BaseHttpService, useValue: mockedBaseHttpService }
      ]
    });
  }));

  it('should ...', inject([UserSettingsService], (service: UserSettingsService) => {
    expect(service).toBeTruthy();
  }));

  it('should call setUserSettings', inject([UserSettingsService], (service: UserSettingsService) => {
    expect(service).toBeTruthy();

    service.setUserSettings(new UserSettings);
    expect(mockedBaseHttpService.lastHttpCallInfo.method).toBe(HttpMethodEn.put);
    expect(mockedBaseHttpService.lastHttpCallInfo.uriFragment).toContain('storeUserSettings');

  }));
});
