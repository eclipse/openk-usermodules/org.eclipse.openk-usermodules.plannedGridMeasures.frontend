/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { async, fakeAsync, tick, TestBed, ComponentFixture, inject, discardPeriodicTasks } from '@angular/core/testing';

import { ToasterMessageService, MessageDefines } from '../../services/toaster-message.service';
import { AbstractMockObservableService } from '../../testing/abstract-mock-observable.service';
import { SessionContext } from '../../common/session-context';
import { ReminderCallerJobService } from './reminder-caller-job.service';
import { Globals } from '../../common/globals';
import { ReminderService } from '../reminder.service';
import { MessageService } from 'primeng/api';



describe('ReminderCallerJobService', () => {

  class MockReminderService extends AbstractMockObservableService {

    getCurrentReminders() {
      return this;
    }
    getExpiredReminders() {
      return this;
    }
  }


  let sessionContext: SessionContext;
  let toasterMessageService: ToasterMessageService;
  let mockReminderService;
  let injectedService;
  let msgService: MessageService;

  beforeEach(async(() => {
    msgService = new MessageService();
    mockReminderService = new MockReminderService();
    sessionContext = new SessionContext();
    sessionContext.clearStorage();
    toasterMessageService = new ToasterMessageService(sessionContext, msgService);
    injectedService = new ReminderCallerJobService(sessionContext, mockReminderService, toasterMessageService);

    TestBed.configureTestingModule({
      providers: [
        { provide: ReminderService, useValue: mockReminderService },
        { provide: SessionContext, useValue: sessionContext },
        ToasterMessageService
      ]
    }).compileComponents();

  }));

  it('should init the timer after successfully login', fakeAsync(() => {
    spyOn(injectedService, 'init').and.callThrough();
    expect(injectedService.init).toHaveBeenCalledTimes(0);

    toasterMessageService.loginLogoff$.emit(MessageDefines.MSG_LOG_IN_SUCCEEDED);
    tick();

    expect(injectedService.init).toHaveBeenCalledTimes(1);
    discardPeriodicTasks();
  }));

  it('should destroy the timer after logout', fakeAsync(() => {
    spyOn(injectedService, 'destroy').and.callThrough();
    toasterMessageService.loginLogoff$.emit(MessageDefines.MSG_LOG_IN_SUCCEEDED);
    tick();

    expect(injectedService.destroy).toHaveBeenCalledTimes(0);

    toasterMessageService.loginLogoff$.emit(MessageDefines.MSG_LOG_OFF);
    tick();

    expect(injectedService.destroy).toHaveBeenCalledTimes(1);
    discardPeriodicTasks();
  }));

  it('should set setUpcomingReminder and setOverdueReminder to true when remindernotifications exist', fakeAsync(() => {
    spyOn(injectedService, 'init').and.callThrough();
    sessionContext.setUpcomingReminder(false);
    sessionContext.setOverdueReminder(false);

    mockReminderService.content = [111];
    Globals.REMINDER_JOB_POLLING_START_DELAY = 10;
    expect(sessionContext.getUpcomingReminder()).toBeFalsy();
    expect(sessionContext.getOverdueReminder()).toBeFalsy();

    toasterMessageService.loginLogoff$.emit(MessageDefines.MSG_LOG_IN_SUCCEEDED);
    tick(15);

    expect(injectedService.init).toHaveBeenCalledTimes(1);
    expect(sessionContext.getUpcomingReminder()).toBeTruthy();
    expect(sessionContext.getOverdueReminder()).toBeTruthy();
    discardPeriodicTasks();
  }));

  it('should set setUpcomingReminder and setOverdueReminder to false when the are no remindernotifications', fakeAsync(() => {
    spyOn(injectedService, 'init').and.callThrough();
    sessionContext.setUpcomingReminder(false);
    sessionContext.setOverdueReminder(false);

    mockReminderService.content = [];
    Globals.REMINDER_JOB_POLLING_START_DELAY = 10;

    expect(sessionContext.getUpcomingReminder()).toBeFalsy();
    expect(sessionContext.getOverdueReminder()).toBeFalsy();

    toasterMessageService.loginLogoff$.emit(MessageDefines.MSG_LOG_IN_SUCCEEDED);
    tick(15);

    expect(injectedService.init).toHaveBeenCalledTimes(1);
    expect(sessionContext.getUpcomingReminder()).toBeFalsy();
    expect(sessionContext.getOverdueReminder()).toBeFalsy();
    discardPeriodicTasks();
  }));

  it('should call "setError" when an error occurs while running "doJob"', fakeAsync(() => {
    spyOn(injectedService, 'setError').and.callThrough();
    sessionContext.setUpcomingReminder(false);
    sessionContext.setOverdueReminder(false);

    Globals.REMINDER_JOB_POLLING_START_DELAY = 10;
    mockReminderService.error = 'REMINDER_MOCK_ERROR';
    expect(sessionContext.getUpcomingReminder()).toBe(false);
    expect(sessionContext.getOverdueReminder()).toBeFalsy();

    toasterMessageService.loginLogoff$.emit(MessageDefines.MSG_LOG_IN_SUCCEEDED);
    tick(15);

    expect(injectedService.setError).toHaveBeenCalledTimes(2);
    discardPeriodicTasks();
  }));


});
