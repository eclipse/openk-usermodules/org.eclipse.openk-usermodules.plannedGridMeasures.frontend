/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { Injectable } from '@angular/core';
import { ToasterMessageService, MessageDefines } from '../toaster-message.service';
import { UserService } from '../user.service';
import { SessionContext } from '../../common/session-context';
import { BaseDataService } from '../base-data.service';
import { RoleAccessService } from '../role-access.service';
import { RoleAccessHelperService } from './role-access-helper.service';
import { BackendSettingsService } from '../backend-settings.service';


@Injectable()
export class BaseDataLoaderService {

  constructor(
    private baseDataService: BaseDataService,
    private userService: UserService,
    private msgService: ToasterMessageService,
    private sessionContext: SessionContext,
    private roleAccessService: RoleAccessService,
    private roleAccessHelper: RoleAccessHelperService,
    private backendSettingsService: BackendSettingsService
  ) {
    this.msgService.loginLogoff$.subscribe(msg => this.onLoginLogoff(msg));
  }

  onLoginLogoff(msg: string): void {
    this.sessionContext.initBannerMessage();
    if (msg === MessageDefines.MSG_LOG_IN_SUCCEEDED) {
      this.sessionContext.setUserAuthenticated(true);
      this.loadBaseData();
    }

    if (msg === MessageDefines.MSG_LOG_OFF) {
      this.sessionContext.setUserAuthenticated(false);
      this.sessionContext.clearStorage();
    }

  }

  loadBaseData() {
    // load base data (stammdaten) here!
    this.baseDataService.getCostCenters()
      .subscribe(center => this.sessionContext.setCostCenters(center),
        error => {
          console.log(error);
        });

    this.baseDataService.getBranches()
      .subscribe(branch => this.sessionContext.setBranches(branch),
        error => {
          console.log(error);
        });

    this.baseDataService.getStatuses()
      .subscribe(status => this.sessionContext.setStatuses(status),
        error => {
          console.log(error);
        });

    this.userService.getUsers()
      .subscribe(users => {
        this.sessionContext.setAllUsers(users);
      },
        error => {
          console.log(error);
        });

    this.loadRoleAccessdefinitions();

    this.baseDataService.getEmailAddressesFromTemplates()
      .subscribe(emails => this.sessionContext.setEmailAddressesFromTemplates(emails),
        error => {
          console.log(error);
        });

    this.backendSettingsService.getBackendSettings()
      .subscribe(settings => this.sessionContext.setBackendsettings(settings),
        error => {
          console.log(error);
        });

    this.baseDataService.getTerritories()
      .subscribe(ter => this.sessionContext.setTerritories(ter),
        error => {
          console.log(error);
        });
  }

  loadRoleAccessdefinitions(): void {
    this.roleAccessService.getRoleAccessDefinition().subscribe(
      res => this.roleAccessHelper.init(res),
      error => {
        console.log(error);
      });
  }

}
