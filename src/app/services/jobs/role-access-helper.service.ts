/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

/* tslint:disable:no-unused-variable */
import { Injectable } from '@angular/core';
import { RoleAccess, EditRoleItems } from '../../model/role-access';

@Injectable()
export class RoleAccessHelperService {
  roleAccess: RoleAccess;
  status2RoleMap: string[][];

  constructor() { }

  public init(newRoleAccess: RoleAccess) {
    if (!this.roleAccess) {
      this.roleAccess = newRoleAccess;

      this.status2RoleMap = [];
      this.roleAccess.editRoles.forEach(item => this.addEditRoleItem(item));
    }
  }

  public editPossibleForRoles(userRoles: string[], statusToBeChecked: number): boolean {
    const rolesForStatus = this.status2RoleMap[statusToBeChecked];
    if (rolesForStatus && userRoles) {
      for (let i = 0; i < userRoles.length; i++) {
        if (rolesForStatus.find(s => s === userRoles[i])) {
          return true;
        }
      }
    }
    return false;
  }

  public getRoleAccessDefinitions(): RoleAccess {
    return this.roleAccess;
  }

  private addEditRoleItem(item: EditRoleItems) {
    item.gridMeasureStatusIds.forEach(status => {
      if (!this.status2RoleMap[status]) {
        this.status2RoleMap[status] = [];
      }
      this.status2RoleMap[status].push(item.name);
    });
  }


}
