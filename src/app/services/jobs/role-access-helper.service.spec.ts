/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { TestBed, inject } from '@angular/core/testing';
import { RoleAccessHelperService } from './role-access-helper.service';
import { RoleAccess } from '../../model/role-access';

describe('RoleAccessHelperService', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [RoleAccessHelperService]
        });
    });

    it('should be created', inject([RoleAccessHelperService], (service: RoleAccessHelperService) => {
        expect(service).toBeTruthy();
    }));

    it('should behave correctly', inject([RoleAccessHelperService], (service: RoleAccessHelperService) => {
        const roleAccessDef: RoleAccess = {
            editRoles: [
                {
                    name: 'planned-policies-measureapplicant',
                    gridMeasureStatusIds: [
                        0
                    ]
                },
                {
                    name: 'planned-policies-measureplanner',
                    gridMeasureStatusIds: [
                        1
                    ]
                },
                {
                    name: 'planned-policies-measureapprover',
                    gridMeasureStatusIds: [
                        2
                    ]
                }
            ],
            controls: [
                {
                    gridMeasureStatusId: 0,
                    activeButtons: [
                        'save',
                        'apply'
                    ],
                    inactiveFields: [
                        'titleControl'
                    ]
                }
            ],
            stornoSection:
            {
                'stornoRoles': [
                    'planned-policies-measureapplicant',
                    'planned-policies-measureplanner',
                    'planned-policies-measureapprover',
                    'planned-policies-requester',
                    'planned-policies-clearance'
                ]
            },
            duplicateSection:
            {
                'duplicateRoles': [
                    'planned-policies-measureapplicant'
                ]
            }
        };

        service.init(roleAccessDef);
        expect(service.getRoleAccessDefinitions().controls[0].inactiveFields[0]).toBe('titleControl');
        expect(service.editPossibleForRoles(['bruno', 'bertil', 'planned-policies-measureplanner'], 1)).toBeTruthy();
        expect(service.editPossibleForRoles([], 1)).toBeFalsy();
        expect(service.editPossibleForRoles(['bruno', 'bertil', 'nope'], 1)).toBeFalsy();
        expect(service.editPossibleForRoles(['bruno', 'bertil', 'nope'], 666)).toBeFalsy();

    }));
});
