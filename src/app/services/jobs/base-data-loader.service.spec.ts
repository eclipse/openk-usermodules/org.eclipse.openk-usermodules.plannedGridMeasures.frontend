/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { async, fakeAsync, tick } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { SessionContext } from '../../common/session-context';
import { Branch } from '../../model/branch';
import { CostCenter } from '../../model/cost-center';
import { Status } from '../../model/status';
import { User } from '../../model/user';
import { MessageDefines, ToasterMessageService } from '../../services/toaster-message.service';
import { USERS } from '../../test-data/users';
import { AbstractMockObservableService } from '../../testing/abstract-mock-observable.service';
import { RoleAccess } from './../../model/role-access';
import { BaseDataLoaderService } from './base-data-loader.service';
import { RoleAccessHelperService } from './role-access-helper.service';
import { BackendSettings } from './../../model/backend-settings';
import { HttpClient } from '@angular/common/http';
import { TERRITORY } from '../../test-data/territories';
import { MessageService } from 'primeng/api';

describe('BaseDataLoaderService', () => {
  class MockDataService extends AbstractMockObservableService {
    public branchList: Branch[];
    public statusList: Status[];
    public departmentList: string[];
    public costCenters: CostCenter[];
    public emailAddresses: string[];
    public responsibleOnSiteNameList: string[];
    public NetworkControlsNameList: string[];
    public areaOfSwitchingList: string[];
    public statusError;
    public branchError;
    public costCenterError;
    public departmentError;
    public responsibleOnSiteNameError;
    public NetworkControlsNameError;
    public emailAddressesError;
    public areaOfSwitchingError;

    getStatuses() {
      const newService = new MockDataService();
      newService.content = this.statusList;
      newService.error = this.statusError;
      return newService;
    }

    getResponsiblesOnSiteFromSingleGridmeasures() {
      const newService = new MockDataService();
      newService.content = this.responsibleOnSiteNameList;
      newService.error = this.responsibleOnSiteNameError;
      return newService;
    }

    getTerritories() {
      const newService = new MockDataService();
      newService.content = this.areaOfSwitchingList;
      newService.error = this.areaOfSwitchingError;
      return newService;
    }

    getBranches() {
      const newService = new MockDataService();
      newService.content = this.branchList;
      newService.error = this.branchError;
      return newService;
    }
    getCostCenters() {
      const newService = new MockDataService();
      newService.content = this.costCenters;
      newService.error = this.costCenterError;
      return newService;
    }
    getEmailAddressesFromTemplates() {
      const newService = new MockDataService();
      newService.content = this.emailAddresses;
      newService.error = this.emailAddressesError;
      return newService;
    }
  }

  class MockUserService extends AbstractMockObservableService {
    public userList: User[];
    public userError;
    public http: HttpClient;
    getUsers() {
      const newService = new MockDataService();
      newService.content = this.userList;
      newService.error = this.userError;
      return newService;
    }
  }
  class MockRoleAccessService extends AbstractMockObservableService {
    public roleAccess: RoleAccess;
    public roleAccessError;
    public http: HttpClient;
    getRoleAccessDefinition(): Observable<RoleAccess> {
      const newService = new MockDataService();
      newService.content = this.roleAccess;
      newService.error = this.roleAccessError;
      return newService as any as Observable<RoleAccess>;
    }
  }

  class MockBackendSettingsService extends AbstractMockObservableService {
    public backendSettings: BackendSettings;
    public backendSettingsError;
    public http: HttpClient;
    getBackendSettings() {
      const newService = new MockDataService();
      newService.content = this.backendSettings;
      newService.error = this.backendSettingsError;
      return newService;
    }
  }


  let sessionContext: SessionContext;
  let toasterMessageSercie: ToasterMessageService;
  let mockRoleAccessService;
  let mockDataService;
  let loaderServer: BaseDataLoaderService;
  let mockUserService;
  let roleAccessHelper: RoleAccessHelperService;
  let mockBackendSettings;
  let msgService: MessageService;

  beforeEach(async(() => {
    mockDataService = new MockDataService();
    mockUserService = new MockUserService();
    mockRoleAccessService = new MockRoleAccessService();
    mockBackendSettings = new MockBackendSettingsService();
    msgService = new MessageService();
    sessionContext = new SessionContext();
    sessionContext.clearStorage();
    toasterMessageSercie = new ToasterMessageService(sessionContext, msgService);
    roleAccessHelper = new RoleAccessHelperService();

    loaderServer = new BaseDataLoaderService(mockDataService, mockUserService, toasterMessageSercie,
      sessionContext, mockRoleAccessService, roleAccessHelper, mockBackendSettings);

  }));


  it('should be load the base-data after MSG_LOG_IN_SUCCEEDED-Message "', fakeAsync(() => {
    mockDataService.statusList = [{ id: 1, name: 'neu' }];
    mockDataService.branchList = [{ id: 1, name: 'W', description: 'Wasser' }];
    mockDataService.costCenters = [{ id: 1, name: 'cc1' }, { id: 2, name: 'cc2' }];
    mockDataService.emailAddresses = ['testpreconfmail@test.de', 'testpreconfmail2@test.de'];
    mockDataService.areaOfSwitchingList = JSON.parse(JSON.stringify(TERRITORY));
    mockUserService.userList = USERS;
    mockBackendSettings.backendSettings = [{ reminderPeriod: 48 }];
    const roleAccessDef: RoleAccess = {
      editRoles: [
        {
          name: 'planned-policies-measureapplicant',
          gridMeasureStatusIds: [
            0
          ]
        },
        {
          name: 'planned-policies-measureplanner',
          gridMeasureStatusIds: [
            1
          ]
        },
        {
          name: 'planned-policies-measureapprover',
          gridMeasureStatusIds: [
            2
          ]
        }
      ],
      controls: [
        {
          gridMeasureStatusId: 0,
          activeButtons: [
            'save',
            'apply'
          ],
          inactiveFields: [
            'titleControl'
          ]
        }
      ],
      stornoSection:
      {
        'stornoRoles': [
          'planned-policies-measureapplicant',
          'planned-policies-measureplanner',
          'planned-policies-measureapprover',
          'planned-policies-requester',
          'planned-policies-clearance'
        ]
      },
      duplicateSection:
      {
        'duplicateRoles': [
          'planned-policies-measureapplicant'
        ]
      }
    };

    (mockRoleAccessService as any).roleAccess = roleAccessDef;
    roleAccessHelper.init(roleAccessDef);

    expect(sessionContext.getStatuses()).toBeNull();
    expect(sessionContext.getBranches()).toBeNull();
    expect(sessionContext.getAllUsers()).toBeNull();
    expect(sessionContext.getBackendsettings()).toBeNull();
    expect(sessionContext.getEmailAddressesFromTemplates()).toBeNull();

    expect(sessionContext.getTerritories()).toBeNull();
    toasterMessageSercie.loginLogoff$.emit(MessageDefines.MSG_LOG_IN_SUCCEEDED);

    tick();

    expect(sessionContext.getStatuses().length).toBe(1);
    expect(sessionContext.getStatuses()[0].name).toBe('neu');

    expect(sessionContext.getBranches().length).toBe(1);
    expect(sessionContext.getBranches()[0].description).toBe('Wasser');

    expect(sessionContext.getEmailAddressesFromTemplates().length).toBe(2);
    expect(sessionContext.getEmailAddressesFromTemplates()[0]).toBe('testpreconfmail@test.de');

    expect(sessionContext.getTerritories().length).toBe(3);
    expect(sessionContext.getTerritories()[0].name).toBe('h1');

    expect(sessionContext.getEmailAddressesFromTemplates().length).toBe(2);

    expect(sessionContext.getAllUsers().length).toBe(3);

    expect(sessionContext.getBackendsettings()[0].reminderPeriod).toBe(48);
    expect(roleAccessHelper.getRoleAccessDefinitions().editRoles.length > 0).toBeTruthy();

  }));

  it('should log to the console if errors occur while loading', fakeAsync(() => {
    spyOn(console, 'log');
    mockDataService.costCenterError = 'Error in costCenter service';
    mockDataService.responsibleOnSiteNameError = 'Error in responsibleOnSiteName service';
    mockDataService.branchError = 'Error in branch service';
    mockDataService.statusError = 'Error in status service';
    mockUserService.userError = 'Error in user service';
    mockDataService.departmentError = 'Error in department service';
    mockDataService.emailAddressError = 'Error in emailAddress service';
    mockDataService.areaOfSwitchingError = 'Error in areaOfSwitching service';
    mockBackendSettings.backendSettingsError = 'Error in backendsettings service';
    (mockRoleAccessService as MockRoleAccessService).roleAccessError = 'Error in roleAcess service';
    toasterMessageSercie.loginLogoff$.emit(MessageDefines.MSG_LOG_IN_SUCCEEDED);
    toasterMessageSercie.loginLogoff$.emit(MessageDefines.MSG_LOG_OFF);

    tick();

    expect(console.log).toHaveBeenCalledTimes(7);

  }));
});
