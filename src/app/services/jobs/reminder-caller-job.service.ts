/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { Injectable } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { TimerObservable } from 'rxjs/observable/TimerObservable';
import { ReminderService } from '../../services/reminder.service';

import { SessionContext } from '../../common/session-context';

import { ToasterMessageService, MessageDefines } from '../../services/toaster-message.service';
import { Globals } from '../../common/globals';



@Injectable()
export class ReminderCallerJobService {

  private timer;
  private subscription: Subscription;


  constructor(
    protected _sessionContext: SessionContext,
    protected reminderService: ReminderService,
    protected msgService: ToasterMessageService
  ) {
    this.msgService.loginLogoff$.subscribe(msg => this.onLoginLogoff(msg));
  }

  onLoginLogoff(msg: string) {
    if (msg === MessageDefines.MSG_LOG_IN_SUCCEEDED) {
      this.init();
    }
    if (msg === MessageDefines.MSG_LOG_OFF) {
      this.destroy();
    }
  }

  init() {

    this.timer = TimerObservable.create(Globals.REMINDER_JOB_POLLING_START_DELAY, Globals.REMINDER_JOB_POLLING_INTERVALL);
    this.subscription = this.timer.subscribe(t => this.doJob(t));

  }

  destroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  doJob(tick: number) {

    this.reminderService.getCurrentReminders()
      .subscribe(currentReminders => {
        this._sessionContext.setCurrentReminders(currentReminders);

        if (currentReminders && currentReminders.length) {
          this._sessionContext.setUpcomingReminder(true);
        } else {
          this._sessionContext.setUpcomingReminder(false);
        }
      }, error => {
        console.log(error);
        this.setError(error);
      });

    this.reminderService.getExpiredReminders()
      .subscribe(expiredReminders => {
        this._sessionContext.setExpiredReminders(expiredReminders);

        if (expiredReminders && expiredReminders.length) {
          this._sessionContext.setOverdueReminder(true);
        } else {
          this._sessionContext.setOverdueReminder(false);
        }
      }, error => {
        console.log(error);
        this.setError(error);
      });

  }

  protected setError(showErr: boolean) {
  }


}
