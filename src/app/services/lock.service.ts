/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Globals } from '../common/globals';
import { SessionContext } from '../common/session-context';
import { Lock } from './../model/lock';
import { BaseHttpService, HttpCallInfo, HttpMethodEn } from './base-http.service';

@Injectable()
export class LockService {

    constructor(
        private baseHttpService: BaseHttpService,
        private sessionContext: SessionContext) {
    }

    storeLock(lock: Lock): Observable<Lock> {
        return this.baseHttpService.callService(
            new HttpCallInfo(Globals.GRID_MEASURES_SERVICE_NAME, HttpMethodEn.put, '/createLock', lock),
            this.sessionContext);
    }
    deleteLock(key: number, info: string, force: string) {
        return this.baseHttpService.callService(
            new HttpCallInfo(Globals.GRID_MEASURES_SERVICE_NAME, HttpMethodEn.delete, '/deleteLock/'
                + key + '/' + info + '/' + force, null),
            this.sessionContext);
    }
    checkLock(key: number, info: string): Observable<Lock> {
        return this.baseHttpService.callService(
            new HttpCallInfo(Globals.GRID_MEASURES_SERVICE_NAME, HttpMethodEn.get, '/checkLock/'
                + key + '/' + info, null),
            this.sessionContext);
    }
}
