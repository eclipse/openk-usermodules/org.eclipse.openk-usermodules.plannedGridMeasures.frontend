/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { TestBed, async, inject } from '@angular/core/testing';

import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/toPromise';

import { ReminderService } from './reminder.service';
import { SessionContext } from '../common/session-context';
import { BaseHttpService, HttpMethodEn } from './base-http.service';
import { MockBaseHttpService } from '../testing/mock-base-http.service';

describe('ReminderService', () => {

  let sessionContext: SessionContext;
  let mockedBaseHttpService: MockBaseHttpService;
  mockedBaseHttpService = new MockBaseHttpService();

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [
        ReminderService,
        SessionContext,
        { provide: BaseHttpService, useValue: mockedBaseHttpService },
      ]
    })
      .compileComponents();

    sessionContext = new SessionContext();
  }));

  it('can instantiate service when inject service',
    inject([ReminderService], (service: ReminderService) => {
      expect(service instanceof ReminderService).toBe(true);
    }));

  it('should call getCurrentReminders', inject([ReminderService], (service: ReminderService) => {
    expect(service).toBeTruthy();

    service.getCurrentReminders();
    expect(mockedBaseHttpService.lastHttpCallInfo.method).toBe(HttpMethodEn.get);
    expect(mockedBaseHttpService.lastHttpCallInfo.uriFragment).toContain('getCurrentReminders');
  }));

  it('should call getExpiredReminders', inject([ReminderService], (service: ReminderService) => {
    expect(service).toBeTruthy();

    service.getExpiredReminders();
    expect(mockedBaseHttpService.lastHttpCallInfo.method).toBe(HttpMethodEn.get);
    expect(mockedBaseHttpService.lastHttpCallInfo.uriFragment).toContain('getExpiredReminders');
  }));
});
