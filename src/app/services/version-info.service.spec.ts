/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { TestBed, inject } from '@angular/core/testing';
import { BaseHttpService, HttpMethodEn } from './base-http.service';
import { VersionInfoService } from './version-info.service';
import { MockBaseHttpService } from '../testing/mock-base-http.service';
import { SessionContext } from '../common/session-context';


describe('Service: VersionInfo', () => {
  let sessionContext: SessionContext;
  let mockedBaseHttpService: MockBaseHttpService;

  beforeEach(() => {
    sessionContext = new SessionContext();
    mockedBaseHttpService = new MockBaseHttpService();

    TestBed.configureTestingModule({
      providers: [
        VersionInfoService,
        { provide: SessionContext, useValue: sessionContext },
        { provide: BaseHttpService, useValue: mockedBaseHttpService },
        SessionContext
      ]
    });
  });

  it('should call getVersionInfo', inject([VersionInfoService], (service: VersionInfoService) => {
    expect(service).toBeTruthy();

    service.loadBackendServerInfo();
    expect(mockedBaseHttpService.lastHttpCallInfo.method).toBe(HttpMethodEn.get);
    expect(mockedBaseHttpService.lastHttpCallInfo.uriFragment).toBe('/versionInfo');

  }));
});

