/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { HttpClientModule, HttpXhrBackend } from '@angular/common/http';
import { TestBed, async, inject } from '@angular/core/testing';
import { MockBackend } from '@angular/http/testing';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/toPromise';
import { SessionContext } from '../common/session-context';
import { BaseHttpService, HttpMethodEn } from '../services/base-http.service';
import { AuthenticationService } from './authentication.service';
import { MockBaseHttpService } from '../testing/mock-base-http.service';

describe('Http-AuthenticationService (mockBackend)', () => {
  let sessionContext: SessionContext;
  let mockedBaseHttpService: MockBaseHttpService;

  beforeEach(() => {
    sessionContext = new SessionContext();
    mockedBaseHttpService = new MockBaseHttpService();

    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [
        AuthenticationService,
        { provide: SessionContext, useValue: sessionContext },
        { provide: BaseHttpService, useValue: mockedBaseHttpService }
      ]
    })
      .compileComponents();
  });

  it('can instantiate service when inject service',
    inject([AuthenticationService], (service: AuthenticationService) => {
      expect(service instanceof AuthenticationService).toBe(true);
    }));


  it('should call logout', inject([AuthenticationService], (service: AuthenticationService) => {
    expect(service).toBeTruthy();

    service.logout();
    expect(mockedBaseHttpService.lastHttpCallInfo.method).toBe(HttpMethodEn.get);
    expect(mockedBaseHttpService.lastHttpCallInfo.uriFragment).toContain('logout');
  }));
});
