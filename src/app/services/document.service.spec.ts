/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { BaseHttpService, HttpMethodEn } from './base-http.service';
import { TestBed, inject } from '@angular/core/testing';
import { MockBaseHttpService } from '../testing/mock-base-http.service';
import { SessionContext } from '../common/session-context';
import { DocumentService } from './document.service';

describe('Service: Document', () => {
  let sessionContext: SessionContext;
  let mockedBaseHttpService: MockBaseHttpService;

  beforeEach(() => {
    sessionContext = new SessionContext();
    mockedBaseHttpService = new MockBaseHttpService();

    TestBed.configureTestingModule({
      providers: [
        DocumentService,
        { provide: SessionContext, useValue: sessionContext },
        { provide: BaseHttpService, useValue: mockedBaseHttpService },
        SessionContext
      ]
    });
  });

  it('should call uploadGridMeasureAttachments', inject([DocumentService], (service: DocumentService) => {
    expect(service).toBeTruthy();

    service.uploadGridMeasureAttachments(this.gmId, this.document);
    expect(mockedBaseHttpService.lastHttpCallInfo.method).toBe(HttpMethodEn.post);
    expect(mockedBaseHttpService.lastHttpCallInfo.uriFragment).toContain('uploadGridMeasureAttachments');

  }));

  it('should call getGridMeasureAttachments', inject([DocumentService], (service: DocumentService) => {
    expect(service).toBeTruthy();

    service.getGridMeasureAttachments(this.gmId);
    expect(mockedBaseHttpService.lastHttpCallInfo.method).toBe(HttpMethodEn.get);
    expect(mockedBaseHttpService.lastHttpCallInfo.uriFragment).toContain('getGridMeasureAttachments');

  }));

  it('should call downloadGridMeasureAttachment', inject([DocumentService], (service: DocumentService) => {
    expect(service).toBeTruthy();

    service.downloadGridMeasureAttachment(this.docId);
    expect(mockedBaseHttpService.lastHttpCallInfo.method).toBe(HttpMethodEn.get);
    expect(mockedBaseHttpService.lastHttpCallInfo.uriFragment).toContain('downloadGridMeasureAttachment');

  }));

  it('should call  deleteGridMeasureAttachment', inject([DocumentService], (service: DocumentService) => {
    expect(service).toBeTruthy();

    service.deleteGridMeasureAttachment(this.docId);
    expect(mockedBaseHttpService.lastHttpCallInfo.method).toBe(HttpMethodEn.delete);
    expect(mockedBaseHttpService.lastHttpCallInfo.uriFragment).toContain('deleteGridMeasureAttachment');

  }));

});
