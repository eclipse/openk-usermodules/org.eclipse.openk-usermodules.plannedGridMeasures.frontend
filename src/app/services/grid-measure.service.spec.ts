/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { TestBed, inject } from '@angular/core/testing';
import { BaseHttpService, HttpMethodEn } from './base-http.service';
import { GridMeasureService } from './grid-measure.service';
import { MockBaseHttpService } from '../testing/mock-base-http.service';
import { SessionContext } from '../common/session-context';
import { GridMeasure } from '../model/grid-measure';
import { Globals } from '../common/globals';

describe('Service: GridMeasure', () => {
  let sessionContext: SessionContext;
  let mockedBaseHttpService: MockBaseHttpService;

  beforeEach(() => {
    sessionContext = new SessionContext();
    mockedBaseHttpService = new MockBaseHttpService();

    TestBed.configureTestingModule({
      providers: [
        GridMeasureService,
        { provide: SessionContext, useValue: sessionContext },
        { provide: BaseHttpService, useValue: mockedBaseHttpService },
        SessionContext
      ]
    });
  });

  it('should call createGridMeasure', inject([GridMeasureService], (service: GridMeasureService) => {
    expect(service).toBeTruthy();
    const gridmeasure = new GridMeasure();
    gridmeasure.id = 555;

    service.storeGridMeasure(gridmeasure);
    expect(mockedBaseHttpService.lastHttpCallInfo.method).toBe(HttpMethodEn.put);
    expect(mockedBaseHttpService.lastHttpCallInfo.uriFragment).toBe('/storeGridMeasure');

  }));
  it('should call getGridMeasures', inject([GridMeasureService], (service: GridMeasureService) => {
    mockedBaseHttpService.content = [{ id: 444 }, { id: 555 }];
    service.getGridMeasures();

    expect(mockedBaseHttpService.lastHttpCallInfo.method).toBe(HttpMethodEn.post);
    expect(mockedBaseHttpService.lastHttpCallInfo.uriFragment).toBe('/getGridMeasures');
    expect(mockedBaseHttpService.lastHttpCallInfo.serviceName).toBe(Globals.GRID_MEASURES_SERVICE_NAME);

  }));
  it('should call getGridMeasure', inject([GridMeasureService], (service: GridMeasureService) => {
    expect(service).toBeTruthy();

    service.getGridMeasure(2);
    expect(mockedBaseHttpService.lastHttpCallInfo.method).toBe(HttpMethodEn.get);
    expect(mockedBaseHttpService.lastHttpCallInfo.uriFragment).toContain('getGridMeasure');

  }));

  it('should call getHistoricalStatusChanges', inject([GridMeasureService], (service: GridMeasureService) => {
    expect(service).toBeTruthy();

    service.getHistoricalStatusChanges(2);
    expect(mockedBaseHttpService.lastHttpCallInfo.method).toBe(HttpMethodEn.get);
    expect(mockedBaseHttpService.lastHttpCallInfo.uriFragment).toContain('getHistoricalStatusChanges');

  }));

  it('should call getUserDepartmentsCreated', inject([GridMeasureService], (service: GridMeasureService) => {
    expect(service).toBeTruthy();

    service.getUserDepartmentsCreated();
    expect(mockedBaseHttpService.lastHttpCallInfo.method).toBe(HttpMethodEn.get);
    expect(mockedBaseHttpService.lastHttpCallInfo.uriFragment).toContain('getUserDepartmentsCreated');

  }));

  it('should call getUserDepartmentsModified', inject([GridMeasureService], (service: GridMeasureService) => {
    expect(service).toBeTruthy();

    service.getUserDepartmentsModified();
    expect(mockedBaseHttpService.lastHttpCallInfo.method).toBe(HttpMethodEn.get);
    expect(mockedBaseHttpService.lastHttpCallInfo.uriFragment).toContain('getUserDepartmentsModified');

  }));

  it('should call getUserDepartmentsResponsibleOnSite', inject([GridMeasureService], (service: GridMeasureService) => {
    expect(service).toBeTruthy();

    service.getUserDepartmentsResponsibleOnSite();
    expect(mockedBaseHttpService.lastHttpCallInfo.method).toBe(HttpMethodEn.get);
    expect(mockedBaseHttpService.lastHttpCallInfo.uriFragment).toContain('getUserDepartmentsResponsibleOnSite');

  }));

  it('should call getResponsiblesOnSiteFromSingleGridmeasures', inject([GridMeasureService], (service: GridMeasureService) => {
    expect(service).toBeTruthy();

    service.getResponsiblesOnSiteFromSingleGridmeasures();
    expect(mockedBaseHttpService.lastHttpCallInfo.method).toBe(HttpMethodEn.get);
    expect(mockedBaseHttpService.lastHttpCallInfo.uriFragment).toContain('getResponsiblesOnSiteFromSingleGridmeasures');

  }));

  it('should call getNetworkControlsFromSingleGridmeasures', inject([GridMeasureService], (service: GridMeasureService) => {
    expect(service).toBeTruthy();

    service.getNetworkControlsFromSingleGridmeasures();
    expect(mockedBaseHttpService.lastHttpCallInfo.method).toBe(HttpMethodEn.get);
    expect(mockedBaseHttpService.lastHttpCallInfo.uriFragment).toContain('getNetworkControlsFromSingleGridmeasures');

  }));

  it('should call getAffectedResourcesDistinct', inject([GridMeasureService], (service: GridMeasureService) => {
    expect(service).toBeTruthy();

    service.getAffectedResourcesDistinct();
    expect(mockedBaseHttpService.lastHttpCallInfo.method).toBe(HttpMethodEn.get);
    expect(mockedBaseHttpService.lastHttpCallInfo.uriFragment).toContain('getAffectedResourcesDistinct');

  }));

  it('should call getCalender', inject([GridMeasureService], (service: GridMeasureService) => {
    expect(service).toBeTruthy();

    service.getCalender();
    expect(mockedBaseHttpService.lastHttpCallInfo.method).toBe(HttpMethodEn.get);
    expect(mockedBaseHttpService.lastHttpCallInfo.uriFragment).toContain('getCalender');

  }));

});
