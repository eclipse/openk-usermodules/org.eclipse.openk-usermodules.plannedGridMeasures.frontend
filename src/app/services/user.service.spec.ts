/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { TestBed, async } from '@angular/core/testing';
import { Globals } from '../common/globals';
import { SessionContext } from '../common/session-context';
import { USERS } from '../test-data/users';
import { MockBaseHttpService } from '../testing/mock-base-http.service';
import { HttpMethodEn } from './base-http.service';
import { UserService } from './user.service';



describe('Http-UserService (mockBackend)', () => {
  let mockedBaseService: any;
  let sessionContext: SessionContext;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [
        SessionContext
      ]
    })
      .compileComponents();
    sessionContext = new SessionContext();
    mockedBaseService = new MockBaseHttpService();
  }));

  it('create a correct envelope when called', () => {
    const userService: UserService = new UserService(mockedBaseService, sessionContext);
    const userResult = USERS;
    mockedBaseService.content = userResult;

    userService.getUsers().subscribe(users => {
      expect(users.length).toBe(userResult.length);
      expect(mockedBaseService.lastHttpCallInfo).toBeTruthy();
      expect(mockedBaseService.lastHttpCallInfo.method).toBe(HttpMethodEn.get);
      expect(mockedBaseService.lastHttpCallInfo.serviceName).toBe(Globals.AUTH_AND_AUTH_SERVICE_NAME);
      expect(mockedBaseService.lastHttpCallInfo.uriFragment).toBe('/users');
    },
      error => expect('No Error').toBe('Where an Error occured')
    );
  });
});
