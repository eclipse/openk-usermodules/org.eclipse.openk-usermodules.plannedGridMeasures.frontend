/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { Injectable } from '@angular/core';
import { UserSettings } from '../model/user-settings';
import { SessionContext } from '../common/session-context';
import { BaseHttpService, HttpMethodEn, HttpCallInfo } from './base-http.service';
import { Observable } from 'rxjs/Observable';
import { Globals } from '../common/globals';

@Injectable()
export class UserSettingsService {

    constructor(private baseHttpService: BaseHttpService,
        private sessionContext: SessionContext) { }
    setUserSettings(userSettings: UserSettings): any {
        const settingsPayload = { ...userSettings, value: JSON.stringify(userSettings.value) };
        return this.baseHttpService.callService(
            new HttpCallInfo(Globals.GRID_MEASURES_SERVICE_NAME, HttpMethodEn.put, '/storeUserSettings', settingsPayload)
            , this.sessionContext);
    }
    getUserSettings(settingType: string): Observable<UserSettings> {
        return this.baseHttpService.callService(

            new HttpCallInfo(Globals.GRID_MEASURES_SERVICE_NAME, HttpMethodEn.get, '/getUserSettings/' + settingType, null)
            , this.sessionContext)
            .map((res) => {
                const userSettings = <UserSettings>res;

                return { ...userSettings, value: userSettings.value ? JSON.parse(userSettings.value.toString()) : userSettings.value };
            });
    }
}
