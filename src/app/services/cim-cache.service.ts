/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { SessionContext } from './../common/session-context';
import { BaseHttpService, HttpCallInfo, HttpMethodEn } from './base-http.service';
import { Injectable } from '@angular/core';
import { GridMeasure } from '../model/grid-measure';
import { Observable } from 'rxjs/Observable';
import { Globals } from '../common/globals';
import { HttpHeaders } from '@angular/common/http';

@Injectable()
export class CimCacheService {

  constructor(
    private baseHttpService: BaseHttpService,
    private sessionContext: SessionContext) {
  }

  getRessourceTypes(): Observable<any> {

    let headers = new HttpHeaders();
    headers = headers.append('Accept', 'application/xml');
    headers = headers.append('content-Type', 'application/json');

    return this.baseHttpService.callService(
      new HttpCallInfo(Globals.CIM_CACHE_SERVICE, HttpMethodEn.get,
        '/electricity/dynamic-topology/power-system-resource-types?revision=1', null),
      this.sessionContext, headers);
  }

  getRessourcesWithType(ressourceType: string): Observable<any> {

    let headers = new HttpHeaders();
    headers = headers.append('Accept', 'application/xml');
    headers = headers.append('content-Type', 'application/json');

    return this.baseHttpService.callService(
      new HttpCallInfo(Globals.CIM_CACHE_SERVICE, HttpMethodEn.get,
        '/electricity/dynamic-topology/power-system-resources?revision=1&power-system-resource-types=' + ressourceType, null),
      this.sessionContext, headers);
  }

  getTopology(): Observable<any> {

    let headers = new HttpHeaders();
    headers = headers.append('Accept', 'application/xml');
    headers = headers.append('content-Type', 'application/json');

    return this.baseHttpService.callService(
      new HttpCallInfo(Globals.CIM_CACHE_SERVICE, HttpMethodEn.get,
        '/electricity/dynamic-topology/topology?revision=1', null),
      this.sessionContext, headers);
  }

}
