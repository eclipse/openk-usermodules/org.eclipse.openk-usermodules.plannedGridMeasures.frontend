/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { HttpClientModule } from '@angular/common/http';
import { TestBed, async, inject } from '@angular/core/testing';
import { RoleAccessService } from './role-access.service';
import { SessionContext } from '../common/session-context';
import { MockBaseHttpService } from '../testing/mock-base-http.service';
import { BaseHttpService, HttpMethodEn } from './base-http.service';

describe('Service: RoleAccess', () => {

  let sessionContext: SessionContext;
  let mockedBaseHttpService: MockBaseHttpService;

  beforeEach(async(() => {
    sessionContext = new SessionContext();
    mockedBaseHttpService = new MockBaseHttpService();
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [
        RoleAccessService,
        { provide: SessionContext, useValue: sessionContext },
        { provide: BaseHttpService, useValue: mockedBaseHttpService },
        SessionContext
      ]
    });
  }));

  it('should call getRoleAccessDefinition', inject([RoleAccessService], (service: RoleAccessService) => {
    expect(service).toBeTruthy();

    service.getRoleAccessDefinition();
    expect(mockedBaseHttpService.lastHttpCallInfo.method).toBe(HttpMethodEn.get);
    expect(mockedBaseHttpService.lastHttpCallInfo.uriFragment).toContain('getRoleAccessDefinition');

  }));
});
