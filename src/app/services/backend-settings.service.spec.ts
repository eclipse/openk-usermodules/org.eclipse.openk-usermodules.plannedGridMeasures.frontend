/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { TestBed, inject } from '@angular/core/testing';

import { BackendSettingsService } from './backend-settings.service';
import { SessionContext } from '../common/session-context';
import { MockBaseHttpService } from '../testing/mock-base-http.service';
import { BaseHttpService, HttpMethodEn } from './base-http.service';

describe('BackendSettingsService', () => {
  let sessionContext: SessionContext;
  let mockedBaseHttpService: MockBaseHttpService;

  beforeEach(() => {
    sessionContext = new SessionContext();
    mockedBaseHttpService = new MockBaseHttpService();

    TestBed.configureTestingModule({
      providers: [BackendSettingsService,
        { provide: SessionContext, useValue: sessionContext },
        { provide: BaseHttpService, useValue: mockedBaseHttpService },
        SessionContext]
    });
  });

  it('should be created', inject([BackendSettingsService], (service: BackendSettingsService) => {
    expect(service).toBeTruthy();
  }));

  it('should call getBackendSettings', inject([BackendSettingsService], (service: BackendSettingsService) => {
    service.getBackendSettings();

    expect(mockedBaseHttpService.lastHttpCallInfo.method).toBe(HttpMethodEn.get);
    expect(mockedBaseHttpService.lastHttpCallInfo.uriFragment).toBe('/getBackendSettings');

  }));

});
