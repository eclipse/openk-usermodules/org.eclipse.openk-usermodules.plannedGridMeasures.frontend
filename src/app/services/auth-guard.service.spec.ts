/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { TestBed, async, inject } from '@angular/core/testing';
import { Router } from '@angular/router';
import { AuthGuard } from './auth-guard.service';
import { SessionContext } from '../common/session-context';

class FakeRouter {
  navigate(commands: any[]) {
    return commands[0];
  }
}

describe('AuthGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        AuthGuard,
        SessionContext,
        { provide: Router, useClass: FakeRouter }
      ]
    })
      .compileComponents();
  });

  it('should instantiate the service', inject([AuthGuard], (authGuard: AuthGuard) => {
    expect(authGuard instanceof AuthGuard).toBe(true);
  }));

  it('should allow activating route if user in session exists',
    inject([AuthGuard, SessionContext], (authGuard: AuthGuard, sessionContext: SessionContext) => {
      spyOn(sessionContext, 'getCurrUser').and.returnValue('dummy');

      expect(authGuard.canActivate()).toBe(true);
    })
  );

  it('should redirect to loggedout page if no user in session exists',
    inject([AuthGuard, SessionContext, Router], (authGuard: AuthGuard, sessionContext: SessionContext, router: Router) => {
      spyOn(sessionContext, 'getCurrUser').and.returnValue(null);
      spyOn(router, 'navigate');

      expect(authGuard.canActivate()).toBe(false);
      expect(router.navigate).toHaveBeenCalledWith(['/loggedout']);
    })
  );

});
