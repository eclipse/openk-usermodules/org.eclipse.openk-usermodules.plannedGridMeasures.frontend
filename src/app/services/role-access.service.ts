/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { RoleAccess } from '../model/role-access';
import { BaseHttpService, HttpCallInfo, HttpMethodEn } from './base-http.service';
import { Globals } from '../common/globals';
import { SessionContext } from '../common/session-context';

@Injectable()
export class RoleAccessService {

    constructor(public http: HttpClient,
        private baseHttpService: BaseHttpService,
        private sessionContext: SessionContext) {
    }

    getRoleAccessDefinition(): Observable<RoleAccess> {
        return this.baseHttpService.callService(
            new HttpCallInfo(Globals.GRID_MEASURES_SERVICE_NAME, HttpMethodEn.get, '/getRoleAccessDefinition', null),
            this.sessionContext);
    }
}
