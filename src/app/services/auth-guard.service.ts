/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { SessionContext } from '../common/session-context';

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(
        private router: Router,
        private sessionContext: SessionContext) {}

    public canActivate(): boolean {
        if (this.sessionContext.getCurrUser()) {
            return true;
        } else {
            this.router.navigate(['/loggedout']);
            return false;
        }
    }
}
