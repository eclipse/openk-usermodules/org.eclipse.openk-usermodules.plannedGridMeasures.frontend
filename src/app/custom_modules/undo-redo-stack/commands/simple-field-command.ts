/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { Command } from './command';
import { BaseCommand } from './base-command';

export class SimpleFieldCommand extends BaseCommand {
    constructor(
        private model: any,
        private field: string,
        private oldval: any,
        private newval: any) {
            super();
        }
    public undo() {
        this.model[this.field] = this.oldval;
    }

    public redo() {
        this.model[this.field] = this.newval;
    }

    public mergePossible( newerCommand: Command ): boolean {
        if ( newerCommand instanceof SimpleFieldCommand ) {
            const newCmd: SimpleFieldCommand = newerCommand;
            return this.model === newCmd.model && this.field === newCmd.field;
        }
        return false;
    }
    public merge( newerCommand: Command): Command {
        if ( newerCommand instanceof SimpleFieldCommand ) {
            const newCmd: SimpleFieldCommand = newerCommand;
            this.newval = newCmd.newval;

            return this;
        }
        throw new TypeError( 'Invalid merge call!');
    }

}
