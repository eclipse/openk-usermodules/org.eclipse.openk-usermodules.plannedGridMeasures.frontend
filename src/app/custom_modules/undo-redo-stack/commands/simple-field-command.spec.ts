/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { TestBed, inject } from '@angular/core/testing';
import { SimpleFieldCommand } from './simple-field-command';
import { BaseCommand } from './base-command';
import { Command } from './command';


class TestModel {
    public stringField = 'STRINga';
    public numField = 776655;
    public boolField = true;

}
class TestCommand extends BaseCommand {
    undo() {
    }
    redo() {
    }
    merge(newerCommand: Command): Command {
        return null;
    }
}

describe('SimpleFieldCommand', () => {
    beforeEach(() => {
      TestBed.configureTestingModule({
      });
    });

    it('should work with strings', (() => {
        const model = new TestModel();

        const cmdString1 = new SimpleFieldCommand(model, 'stringField', 'stringy', 'STRINg');
        const cmdString2 = new SimpleFieldCommand(model, 'stringField', 'STRINg', 'STRINga');

        expect(cmdString1.mergePossible(cmdString2)).toBe(true);
        cmdString1.merge(cmdString2);

        cmdString1.undo();

        expect(model.stringField).toBe( 'stringy' );

        cmdString1.redo();

        expect(model.stringField).toBe( 'STRINga' );



    }));
    it('should work with nums', (() => {
        const model = new TestModel();


        const cmdNum1 = new SimpleFieldCommand(model, 'numField', 112233, 776644 );
        const cmdNum2 = new SimpleFieldCommand(model, 'numField', 776644, 776655 );

        expect(cmdNum1.mergePossible(cmdNum2)).toBe(true);
        cmdNum1.merge(cmdNum2);

        cmdNum1.undo();

        expect(model.numField).toBe( 112233 );

        cmdNum1.redo();

        expect(model.numField).toBe( 776655 );

    }));

    it('should work with bool', (() => {
        const model = new TestModel();


        const cmdNum1 = new SimpleFieldCommand(model, 'boolField', true, false );
        const cmdNum2 = new SimpleFieldCommand(model, 'boolField', false, true );

        expect(cmdNum1.mergePossible(cmdNum2)).toBe(true);
        cmdNum1.merge(cmdNum2);

        cmdNum1.undo();

        expect(model.boolField).toBe( true );

        cmdNum1.redo();

        expect(model.boolField).toBe( true );

    }));

    it('should reject invalid commands for merge', (() => {
        const model = new TestModel();
        const model2 = new TestModel();

        const cmdString1 = new SimpleFieldCommand(model, 'stringField', 'bruno', 'haferkamp');
        const cmdString2 = new SimpleFieldCommand(model2, 'stringField', 'claudio', 'haysterkamp');
        const cmdNum3 = new SimpleFieldCommand(model2, 'numField', 123, 345);

        expect(cmdString1.mergePossible(cmdString2)).toBe(false);
        expect(cmdString1.mergePossible(cmdNum3)).toBe(false);
        expect(cmdString1.mergePossible(new TestCommand())).toBe(false);

        expect( function() { cmdString1.merge(new TestCommand); }).toThrowError();
    }));
});


