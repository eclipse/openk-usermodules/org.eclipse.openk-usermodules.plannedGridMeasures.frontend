/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { TestBed, inject } from '@angular/core/testing';
import { BaseCommand } from './commands/base-command';
import { Command } from './commands/command';
import { UndoRedoStackComponent } from './undo-redo-stack.component';

class TestCommand extends BaseCommand {
  undoCalled = 0;
  redoCalled = 0;
  mergePoss = false;
  mergeCalled = 0;

  undo() {
    this.undoCalled += 1;
  }
  redo() {
    this.redoCalled += 1;
  }
  mergePossible(newerCommand: Command): boolean {
    super.mergePossible(newerCommand); // for test coverage!
    return this.mergePoss;
  }
  merge(newerCommand: Command) {
    if (this.mergePossible(null)) {
      this.mergeCalled += 1;
    }
    return this;
  }
}

describe('UndoRedoStack', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
    });
  });

  it('should be createable', (() => {
    const stack = new UndoRedoStackComponent();
    expect(stack).not.toBeNull();
    expect(stack.isRedoPossible()).toBeFalsy();
    expect(stack.isUndoPossible()).toBeFalsy();
  }));

  it('behave correctly', (() => {
    const stack = new UndoRedoStackComponent();
    stack.undo(); // does nothing when stack is empty
    stack.redo(); // does nothing when stack is empty

    const testCommand1 = new TestCommand();
    const testCommand2 = new TestCommand();
    const testCommand3 = new TestCommand();
    const testCommand4 = new TestCommand();
    const testCommand5 = new TestCommand();


    stack.push(testCommand1);
    stack.push(testCommand2);
    stack.push(testCommand3);

    testCommand3.mergePoss = true;
    stack.push(testCommand4);

    testCommand3.mergePoss = false;
    stack.push(testCommand5);

    stack.undo();
    stack.undo();

    stack.redo();
    stack.redo();
    stack.redo();

    stack.undo();
    stack.undo();
    stack.undo();
    stack.undo();
    stack.undo();
    stack.undo();

    expect(testCommand1.undoCalled).toBe(1);
    expect(testCommand2.undoCalled).toBe(1);
    expect(testCommand3.undoCalled).toBe(2);
    expect(testCommand4.undoCalled).toBe(0);
    expect(testCommand5.undoCalled).toBe(2);

    expect(testCommand1.redoCalled).toBe(0);
    expect(testCommand2.redoCalled).toBe(0);
    expect(testCommand3.redoCalled).toBe(1);
    expect(testCommand4.redoCalled).toBe(0);
    expect(testCommand5.redoCalled).toBe(1);

    const testCommand6 = new TestCommand();
    stack.redo();
    stack.push(testCommand6);

    const stackUncovered: any = stack;
    expect(stackUncovered.pointer).toBe(1);
    expect(stackUncovered.limit).toBe(1);
  }));
});
