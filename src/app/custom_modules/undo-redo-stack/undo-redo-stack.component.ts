/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { Component } from '@angular/core';
import { Command } from './commands/command';

export class UndoRedoStackComponent {
  private stack: Command[] = [];
  private pointer = -1;
  private limit = -1;

  constructor(isMergeable: boolean = true) {
    this.clear();
  }

  public push(cmd: Command) {
    if (this.pointer >= 0 && this.stack[this.pointer].mergePossible(cmd)) {
      this.stack[this.pointer].merge(cmd);
    } else {
      this.pointer++;
      this.limit = this.pointer;
      this.stack[this.pointer] = cmd;
    }
  }

  public undo() {
    if (this.isUndoPossible()) {
      this.stack[this.pointer].undo();
      this.pointer--;
    }
  }

  public redo() {
    if (this.isRedoPossible()) {
      this.pointer++;
      this.stack[this.pointer].redo();
    }
  }

  public isUndoPossible() {
    return this.pointer >= 0;
  }

  public isRedoPossible() {
    return this.pointer < this.limit;
  }

  public clear() {
    this.stack = [];
    this.pointer = -1;
    this.limit = -1;
  }

}
