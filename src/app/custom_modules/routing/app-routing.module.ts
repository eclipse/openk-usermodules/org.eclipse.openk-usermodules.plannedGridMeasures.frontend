/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OverviewComponent } from '../../pages/overview/overview.component';
import { LogoutPageComponent } from '../../pages/logout/logout.component';
import { LoggedoutPageComponent } from '../../pages/loggedout/loggedout.component';
import { GridMeasureDetailComponent } from '../../pages/grid-measure-detail/grid-measure-detail.component';
import { AuthGuard } from '../../services/auth-guard.service';
import { GridConfigModifierComponent } from '../../pages/grid-config-modifier/grid-config-modifier.component';
import { CancelGridMeasureComponent } from '../../pages/cancel-grid-measure/cancel-grid-measure.component';

const ROUTES: Routes = [
  {
    path: '',
    redirectTo: '/overview',
    pathMatch: 'full'
  },
  {
    path: 'overview',
    component: OverviewComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'logout',
    component: LogoutPageComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'loggedout',
    component: LoggedoutPageComponent
  },
  {
    path: 'gridMeasureDetail',
    component: GridMeasureDetailComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'gridMeasureDetail/:id/:mode',
    component: GridMeasureDetailComponent
  },
  {
    path: 'modifyGridConfig',
    component: GridConfigModifierComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'gridMeasureDetail/:id/#/cancel',
    component: CancelGridMeasureComponent,
    canActivate: [AuthGuard]
  },
  {
    path: '**',
    redirectTo: '/overview'
  }

];

@NgModule({
  imports: [
    RouterModule.forRoot(ROUTES, { useHash: true })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
