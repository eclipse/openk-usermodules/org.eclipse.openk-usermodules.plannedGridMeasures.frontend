/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/



import { GridMeasure } from '../../model/grid-measure';
import { RoleAccessHelperService } from '../../services/jobs/role-access-helper.service';
import { Router } from '@angular/router';
import { SessionContext } from '../../common/session-context';
import { Globals } from '../../common/globals';
import { Injectable } from '@angular/core';

@Injectable()
export class ModeValidator {

    constructor(public sessionContext: SessionContext,
        public roleAccessHelper: RoleAccessHelperService,
        public router: Router) { }

    handleGridMeasureMode(gridmeasure: GridMeasure, mode: string) {
        switch (mode) {
            case Globals.MODE.VIEW:
                this.router.navigate(['/gridMeasureDetail/', gridmeasure.id, Globals.MODE.VIEW]);
                break;
            case Globals.MODE.EDIT:
                this.router.navigate(['/gridMeasureDetail/', gridmeasure.id, Globals.MODE.EDIT]);
                break;
            case Globals.MODE.CANCEL:
                // this.messageService.clearBannerLocalEvent$.emit(true);
                this.sessionContext.setGridMeasureDetail(gridmeasure);
                this.sessionContext.setCancelStage(true);
                this.router.navigate(['/gridMeasureDetail/', gridmeasure.id, '#', Globals.MODE.CANCEL]);
                break;
            default:
                this.router.navigate(['/overview']);
                break;
        }
    }

    isEditModeAllowed(statusId: number): boolean {
        const user = this.sessionContext.getCurrUser();
        return (user.roles.includes(Globals.OAUTH2CONF_SUPERUSER_ROLE) ||
            this.roleAccessHelper.editPossibleForRoles(user.roles, statusId))
            && statusId !== Globals.STATUS.CLOSED;
    }

    isEmailEditModeAllowed(gridmeasure: GridMeasure): boolean {
        const user = this.sessionContext.getCurrUser();
        return this.isEditModeAllowed(gridmeasure.statusId) &&
            (user.roles.includes(Globals.OAUTH2CONF_SUPERUSER_ROLE) ||
                user.roles.includes(Globals.OAUTH2CONF_MEASUREAPPLICANT_ROLE) ||
                user.roles.includes(Globals.OAUTH2CONF_MEASUREPLANNER_ROLE));
    }

    isCancelAllowed(gridmeasure: GridMeasure): boolean {
        const currRoles = this.sessionContext.getCurrUser().roles;
        const stornoSection = this.roleAccessHelper.getRoleAccessDefinitions().stornoSection;
        let cancelAllowedForRole = false;
        if (!stornoSection) {
            return;
        }
        stornoSection.stornoRoles.forEach(allowedRole => {
            if (currRoles.includes(allowedRole)) {
                cancelAllowedForRole = true;
                return;
            }
        });
        const status = gridmeasure.statusId;
        let cancelAllowedForStatus = false;
        if (status === Globals.STATUS.NEW || status === Globals.STATUS.APPLIED || status === Globals.STATUS.APPROVED
            || status === Globals.STATUS.FORAPPROVAL || status === Globals.STATUS.REQUESTED) {
            cancelAllowedForStatus = true;
        }
        return cancelAllowedForRole && cancelAllowedForStatus;
    }

}
