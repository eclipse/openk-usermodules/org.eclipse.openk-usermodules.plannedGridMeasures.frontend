/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { SingleGridMeasure } from '../../model/single-grid-measure';
import { GridMeasure } from '../../model/grid-measure';
import { Globals } from '../../common/globals';

export class CloneGridMeasureHelper {

    static cloneGridMeasure(parentGridMeasure: GridMeasure): GridMeasure {

        // use as deep copy
        const gm: GridMeasure = JSON.parse(JSON.stringify(parentGridMeasure));

        gm.id = null;
        gm.statusId = null;
        gm.descriptiveId = null;
        gm.title = 'Kopie von ' + parentGridMeasure.title;

        clearIdFromNewSingleGMsAndSteps(gm.listSingleGridmeasures);

        return gm;
    }

    static cloneSingleGridMeasure(parentGridMeasure: GridMeasure, singleGridMeasureToCopy: SingleGridMeasure): SingleGridMeasure {

        // use as deep copy
        const singleGM: SingleGridMeasure = JSON.parse(JSON.stringify(singleGridMeasureToCopy));

        // alter values according to inversion

        // new inverted singleGM is ordered after the hitherto last singleGM
        singleGM.sortorder = parentGridMeasure.listSingleGridmeasures[parentGridMeasure.listSingleGridmeasures.length - 1].sortorder + 1;

        singleGM.title = singleGridMeasureToCopy.title ? 'Rückschaltung von ' + singleGridMeasureToCopy.title : 'Rückschaltung';

        singleGM.id = null;
        singleGM.plannedEndtimeSinglemeasure = null;
        singleGM.plannedStarttimeSinglemeasure = null;

        // inverted singleGM has inverted step order
        if (singleGM.listSteps && singleGM.listSteps.length > 0) {
            singleGM.listSteps.reverse();

            reverseStepSortOrder(singleGM);
            clearIdFromNewSteps(singleGM, false);
        }

        return singleGM;
    }

}

function clearIdFromNewSingleGMsAndSteps(singleGMs: SingleGridMeasure[]) {
    singleGMs.forEach(sgm => {
        sgm.id = null;
        clearIdFromNewSteps(sgm, true);
    });
}

function clearIdFromNewSteps(singleGM: SingleGridMeasure, isDuplicate: boolean) {
    singleGM.listSteps.forEach(step => {
        if (isDuplicate) {
            step.id = null;
        } else {
            step.id = Globals.TEMP_ID_TO_SHOW_NEW_STEPS;
        }
    });
}

function reverseStepSortOrder(singleGM: SingleGridMeasure) {
    for (let i = 0; i < singleGM.listSteps.length / 2; i++) {
        const tmpSortorder: number = singleGM.listSteps[i].sortorder;

        singleGM.listSteps[i].sortorder = singleGM.listSteps[singleGM.listSteps.length - i - 1].sortorder;
        singleGM.listSteps[singleGM.listSteps.length - i - 1].sortorder = tmpSortorder;

        singleGM.listSteps[i].id = null;

    }
}
