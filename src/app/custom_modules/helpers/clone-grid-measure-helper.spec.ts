/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { TestBed, async } from '@angular/core/testing';
import { GridMeasure } from '../../model/grid-measure';
import { GRIDMEASURE } from '../../test-data/grid-measures';
import { CloneGridMeasureHelper } from '../helpers/clone-grid-measure-helper';
import { SingleGridMeasure } from '../../model/single-grid-measure';

describe('CloneGridMeasureHelper', () => {
    const gm: GridMeasure = JSON.parse(JSON.stringify(GRIDMEASURE[0]));
    let sgm1: SingleGridMeasure;
    let sgm2: SingleGridMeasure;
    let inv_sgm1: SingleGridMeasure;
    let inv_sgm2: SingleGridMeasure;
    let dupl_gm: GridMeasure;

    beforeEach(() => {
        TestBed.configureTestingModule({
        });
    });

    beforeAll(() => {
        dupl_gm = CloneGridMeasureHelper.cloneGridMeasure(gm);

        sgm1 = gm.listSingleGridmeasures[0];
        sgm2 = gm.listSingleGridmeasures[1];
        inv_sgm1 = CloneGridMeasureHelper.cloneSingleGridMeasure(gm, sgm1);
        inv_sgm2 = CloneGridMeasureHelper.cloneSingleGridMeasure(gm, sgm2);


    });

    it('should create duplicated GMs correctly', async(() => {
        expect(dupl_gm).toBeTruthy();
    }));

    it('should alter the titles correctly', async(() => {
        expect(dupl_gm.title).toBe('Kopie von ' + gm.title);
    }));

    it('should clear all ids', async(() => {
        expect(dupl_gm.id).toBeNull();
        expect(dupl_gm.listSingleGridmeasures[0].id).toBeNull();
        expect(dupl_gm.listSingleGridmeasures[0].listSteps[0].id).toBeNull();
    }));


    it('should create new SGMs correctly', async(() => {
        expect(inv_sgm1).toBeTruthy();
        expect(inv_sgm2).toBeTruthy();
    }));

    it('should alter the titles correctly', async(() => {
        expect(inv_sgm1.title).toBe('Rückschaltung von ' + sgm1.title);
        expect(inv_sgm2.title).toBe('Rückschaltung von ' + sgm2.title);
    }));

    it('should set sort order correctly', async(() => {
        expect(inv_sgm1.sortorder).toBe(gm.listSingleGridmeasures[gm.listSingleGridmeasures.length - 1].sortorder + 1);
        expect(inv_sgm2.sortorder).toBe(gm.listSingleGridmeasures[gm.listSingleGridmeasures.length - 1].sortorder + 1);
    }));

    it('should remove certain values', async(() => {
        expect(inv_sgm1.id).toBeFalsy();
        expect(inv_sgm1.plannedEndtimeSinglemeasure).toBeFalsy();
        expect(inv_sgm1.plannedStarttimeSinglemeasure).toBeFalsy();
        expect(inv_sgm2.id).toBeFalsy();
        expect(inv_sgm2.plannedEndtimeSinglemeasure).toBeFalsy();
        expect(inv_sgm2.plannedStarttimeSinglemeasure).toBeFalsy();
    }));

});
