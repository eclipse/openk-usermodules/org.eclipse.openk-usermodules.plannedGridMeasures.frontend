/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { TestBed, inject } from '@angular/core/testing';
import { RoleAccessHelperService } from '../../services/jobs/role-access-helper.service';
import { Router, ActivatedRoute } from '@angular/router';
import { SessionContext } from '../../common/session-context';
import { ModeValidator } from './mode-validator';
import { ActivatedRouteStub, RouterStub } from '../../testing';
import { GRIDMEASURE } from '../../test-data/grid-measures';
import { GridMeasure } from '../../model/grid-measure';
import { Globals } from '../../common/globals';
import { USERS } from '../../test-data/users';
import { RoleAccess } from '../../model/role-access';
import { ToasterMessageService } from '../../services/toaster-message.service';


describe('ModeValidator', () => {
    let sessionContext: SessionContext;
    let roleAccessHelper: RoleAccessHelperService;
    let activatedStub: ActivatedRouteStub;
    let routerStub: RouterStub;
    const gridmeasures: GridMeasure[] = JSON.parse(JSON.stringify(GRIDMEASURE));

    routerStub = {
        navigate: jasmine.createSpy('navigate').and.callThrough()
    };

    beforeEach(() => {
        sessionContext = new SessionContext();
        roleAccessHelper = new RoleAccessHelperService();
        activatedStub = new ActivatedRouteStub();
        const roleAcess: RoleAccess = {
            editRoles: [{
                name: 'planned-policies-measureplanner',
                gridMeasureStatusIds: [
                    0,
                    1
                ]
            }, {
                name: 'planned-policies-superuser',
                gridMeasureStatusIds: [
                    0,
                    1
                ]
            }, {
                name: 'planned-policies-measureapplicant',
                gridMeasureStatusIds: [
                    0,
                    1
                ]
            }],
            controls: [{
                gridMeasureStatusId: 0,
                activeButtons: [
                    'save',
                    'apply',
                    'cancel'
                ],
                inactiveFields: [
                    'titeldermassnahme'
                ]
            },
            {
                gridMeasureStatusId: 1,
                activeButtons: [
                    'save',
                    'cancel',
                    'forapproval'
                ],
                inactiveFields: [
                    'titeldermassnahme'
                ]
            }],
            stornoSection:
            {
                'stornoRoles': [
                    'planned-policies-measureapplicant',
                    'planned-policies-measureplanner',
                    'planned-policies-measureapprover',
                    'planned-policies-requester',
                    'planned-policies-clearance'
                ]
            },
            duplicateSection:
            {
                'duplicateRoles': [
                    'planned-policies-measureapplicant'
                ]
            }
        };
        roleAccessHelper.init(roleAcess);


        TestBed.configureTestingModule({
            providers: [ModeValidator,
                SessionContext,
                { provide: RoleAccessHelperService, useValue: roleAccessHelper },
                { provide: ActivatedRoute, useValue: activatedStub },
                { provide: Router, useValue: routerStub },
                { provide: ToasterMessageService }]
        });
    });

    it('can instatiate service when inject service', inject([ModeValidator], (service: ModeValidator) => {
        expect(service instanceof ModeValidator);
    }));

    it('should handle grid measure detail view mode', inject([ModeValidator], (modeValidator: ModeValidator) => {
        modeValidator.handleGridMeasureMode(gridmeasures[0], Globals.MODE.VIEW);
        expect(routerStub.navigate).toHaveBeenCalledWith(['/gridMeasureDetail/', gridmeasures[0].id, Globals.MODE.VIEW]);
    }));

    it('should handle grid measure detail edit mode', inject([ModeValidator], (modeValidator: ModeValidator) => {
        modeValidator.handleGridMeasureMode(gridmeasures[0], Globals.MODE.EDIT);
        expect(routerStub.navigate).toHaveBeenCalledWith(['/gridMeasureDetail/', gridmeasures[0].id, Globals.MODE.EDIT]);
    }));

    it('should handle grid measure detail cancel mode', inject([ModeValidator], (modeValidator: ModeValidator) => {
        modeValidator.handleGridMeasureMode(gridmeasures[0], Globals.MODE.CANCEL);
        expect(routerStub.navigate).toHaveBeenCalledWith(['/gridMeasureDetail/', gridmeasures[0].id, '#', Globals.MODE.CANCEL]);
    }));

    it('should handle grid measure detail wrong mode', inject([ModeValidator], (modeValidator: ModeValidator) => {
        modeValidator.handleGridMeasureMode(gridmeasures[0], 'wrongmode');
        expect(routerStub.navigate).toHaveBeenCalledWith(['/overview']);
    }));

    it('should check if edit mode is allowed for superuser', inject([ModeValidator], (modeValidator: ModeValidator) => {
        sessionContext.setCurrUser(USERS[1]);
        modeValidator.isEditModeAllowed(gridmeasures[0].statusId);
        expect(modeValidator.isEditModeAllowed).toBeTruthy();

    }));

    it('should check if edit mode is allowed for planner and status new', inject([ModeValidator], (modeValidator: ModeValidator) => {
        sessionContext.setCurrUser(USERS[0]);
        modeValidator.isEditModeAllowed(gridmeasures[0].statusId);
        expect(modeValidator.isEditModeAllowed).toBeTruthy();

    }));

    it('should check if email edit mode allowed for role and status new', inject([ModeValidator], (modeValidator: ModeValidator) => {
        sessionContext.setCurrUser(USERS[0]);
        modeValidator.isEmailEditModeAllowed(gridmeasures[0]);
        expect(modeValidator.isEmailEditModeAllowed).toBeTruthy();

    }));

    it('should check if cancel mode allowed for role and status', inject([ModeValidator], (modeValidator: ModeValidator) => {
        sessionContext.setCurrUser(USERS[0]);
        modeValidator.isCancelAllowed(gridmeasures[0]);
        expect(modeValidator.isCancelAllowed).toBeTruthy();

    }));

});
