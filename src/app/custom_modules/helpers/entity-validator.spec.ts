/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/


import { TestBed } from '@angular/core/testing';
import { EntityValidator, ValidationFunc } from './entity-validator';
import { SessionContext } from '../../common/session-context';
import { ToasterMessageService } from '../../services/toaster-message.service';
import { MessageService } from 'primeng/api';

class BoolValidator extends ValidationFunc<boolean> {
    constructor(private result: boolean) {
        super();
    }

    public validate(b: boolean, msgService: ToasterMessageService): boolean {
        if (!this.result) {
            msgService.showWarn('Test message: ' + b);
        }
        return this.result;
    }
}

describe('EntityValidator', () => {
    let toasterMessageService: ToasterMessageService;
    let sessionContext: SessionContext;
    let messageService: MessageService;

    beforeEach(() => {
        messageService = new MessageService;
        sessionContext = new SessionContext();
        toasterMessageService = new ToasterMessageService(sessionContext, messageService);

        TestBed.configureTestingModule({
        });
    });

    it('should work correctly with stop on first error', (() => {
        spyOn(toasterMessageService, 'showWarn').and.callThrough();
        const validationList: ValidationFunc<boolean>[] = [
            new BoolValidator(true),
            new BoolValidator(false),
            new BoolValidator(false)];

        const validator = new EntityValidator<boolean>(toasterMessageService, validationList);
        validator.validateEntity(true, true);

        expect(toasterMessageService.showWarn).toHaveBeenCalledTimes(1);

    }));


    it('should work correctly with dont stop on first error', (() => {
        spyOn(toasterMessageService, 'showWarn').and.callThrough();
        const validationList: ValidationFunc<boolean>[] = [
            new BoolValidator(true),
            new BoolValidator(false),
            new BoolValidator(false)];

        const validator = new EntityValidator<boolean>(toasterMessageService, validationList);
        validator.validateEntity(true, false);

        expect(toasterMessageService.showWarn).toHaveBeenCalledTimes(2);

    }));
});
