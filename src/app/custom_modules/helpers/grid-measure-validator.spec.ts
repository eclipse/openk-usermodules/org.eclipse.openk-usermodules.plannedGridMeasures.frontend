/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { TestBed, async } from '@angular/core/testing';
import { EntityValidator } from './entity-validator';
import { GridMeasureValidatorFactory } from './grid-measure-validator';
import { SessionContext } from '../../common/session-context';
import { GridMeasure } from '../../model/grid-measure';
import { GRIDMEASURE } from '../../test-data/grid-measures';
import { SingleGridMeasure } from '../../model/single-grid-measure';
import { MessageService } from 'primeng/api';
import { ToasterMessageService } from '../../services/toaster-message.service';

describe('GridMeasureValidator', () => {
    let toasterMessageService: ToasterMessageService;
    let sessionContext: SessionContext;
    let messageService: MessageService;

    beforeEach(() => {
        messageService = new MessageService;
        sessionContext = new SessionContext();
        toasterMessageService = new ToasterMessageService(sessionContext, messageService);

        TestBed.configureTestingModule({
        });
    });

    it('should check PlannedGridMeasureDateValidator correctly', async(() => {
        spyOn(toasterMessageService, 'showWarn').and.callThrough();
        const gm1: GridMeasure = JSON.parse(JSON.stringify(GRIDMEASURE[0]));
        const gm2: GridMeasure = JSON.parse(JSON.stringify(GRIDMEASURE[0]));
        gm1.plannedStarttimeFirstSinglemeasure = '2016-01-15T11:11:00z';
        gm1.endtimeGridmeasure = '2017-01-15T11:11:00z';
        gm2.plannedStarttimeFirstSinglemeasure = '2017-01-15T11:11:00z';
        gm2.endtimeGridmeasure = '2016-01-15T11:11:00z';


        const entityValidator: EntityValidator<GridMeasure> = GridMeasureValidatorFactory.createGM(toasterMessageService);

        entityValidator.validateEntity(gm1, true);
        expect(toasterMessageService.showWarn).not.toHaveBeenCalled();

        gm1.plannedStarttimeFirstSinglemeasure = gm1.endtimeGridmeasure;
        entityValidator.validateEntity(gm1, true);
        expect(toasterMessageService.showWarn).not.toHaveBeenCalled();

        entityValidator.validateEntity(gm2, true);
        expect(toasterMessageService.showWarn).toHaveBeenCalled();

        gm1.plannedStarttimeFirstSinglemeasure = null;
        gm1.endtimeGridmeasure = '2017-01-15T11:11:00z';
        expect(entityValidator.validateEntity(gm1, true)).toBeTruthy();


        gm1.plannedStarttimeFirstSinglemeasure = '';
        gm1.endtimeGridmeasure = '2017-01-15T11:11:00z';
        expect(entityValidator.validateEntity(gm1, true)).toBeTruthy();

        gm1.plannedStarttimeFirstSinglemeasure = '2017-01-15T11:11:00z';
        gm1.endtimeGridmeasure = null;
        expect(entityValidator.validateEntity(gm1, true)).toBeTruthy();


        gm1.plannedStarttimeFirstSinglemeasure = '2017-01-15T11:11:00z';
        gm1.endtimeGridmeasure = '';
        expect(entityValidator.validateEntity(gm1, true)).toBeTruthy();

        gm1.plannedStarttimeFirstSinglemeasure = '';
        gm1.endtimeGridmeasure = '';
        expect(entityValidator.validateEntity(gm1, true)).toBeTruthy();

    }));

    it('should check SingleGridMeasureDateValidator correctly', async(() => {
        spyOn(toasterMessageService, 'showWarn').and.callThrough();
        const gm1: GridMeasure = JSON.parse(JSON.stringify(GRIDMEASURE[0]));
        const gm2: GridMeasure = JSON.parse(JSON.stringify(GRIDMEASURE[0]));
        gm1.listSingleGridmeasures[0].plannedStarttimeSinglemeasure = '2016-01-15T11:11:00z';
        gm1.listSingleGridmeasures[0].plannedEndtimeSinglemeasure = '2017-01-15T11:11:00z';
        gm2.listSingleGridmeasures[0].plannedStarttimeSinglemeasure = '2017-01-15T11:11:00z';
        gm2.listSingleGridmeasures[0].plannedEndtimeSinglemeasure = '2016-01-15T11:11:00z';


        const entityValidator2: EntityValidator<SingleGridMeasure> = GridMeasureValidatorFactory.createsingleGM(toasterMessageService);

        entityValidator2.validateEntity(gm1.listSingleGridmeasures[0], true);
        expect(toasterMessageService.showWarn).not.toHaveBeenCalled();

        gm1.listSingleGridmeasures[0].plannedStarttimeSinglemeasure = gm1.listSingleGridmeasures[0].plannedEndtimeSinglemeasure;
        entityValidator2.validateEntity(gm1.listSingleGridmeasures[0], true);
        expect(toasterMessageService.showWarn).not.toHaveBeenCalled();

        entityValidator2.validateEntity(gm2.listSingleGridmeasures[0], true);
        expect(toasterMessageService.showWarn).toHaveBeenCalled();

        gm1.listSingleGridmeasures[0].plannedStarttimeSinglemeasure = null;
        gm1.listSingleGridmeasures[0].plannedEndtimeSinglemeasure = '2017-01-15T11:11:00z';
        expect(entityValidator2.validateEntity(gm1.listSingleGridmeasures[0], true)).toBeTruthy();


        gm1.listSingleGridmeasures[0].plannedStarttimeSinglemeasure = '';
        gm1.listSingleGridmeasures[0].plannedEndtimeSinglemeasure = '2017-01-15T11:11:00z';
        expect(entityValidator2.validateEntity(gm1.listSingleGridmeasures[0], true)).toBeTruthy();

        gm1.listSingleGridmeasures[0].plannedStarttimeSinglemeasure = '2017-01-15T11:11:00z';
        gm1.listSingleGridmeasures[0].plannedEndtimeSinglemeasure = null;
        expect(entityValidator2.validateEntity(gm1.listSingleGridmeasures[0], true)).toBeTruthy();


        gm1.listSingleGridmeasures[0].plannedStarttimeSinglemeasure = '2017-01-15T11:11:00z';
        gm1.listSingleGridmeasures[0].plannedEndtimeSinglemeasure = '';
        expect(entityValidator2.validateEntity(gm1.listSingleGridmeasures[0], true)).toBeTruthy();

        gm1.listSingleGridmeasures[0].plannedStarttimeSinglemeasure = '';
        gm1.listSingleGridmeasures[0].plannedEndtimeSinglemeasure = '';
        expect(entityValidator2.validateEntity(gm1.listSingleGridmeasures[0], true)).toBeTruthy();

        gm1.listSingleGridmeasures[0].plannedStarttimeSinglemeasure = '2017-01-15T11:11:00z';
        gm1.listSingleGridmeasures[0].plannedEndtimeSinglemeasure = '2017-01-15T10:11:00z';
        entityValidator2.validateEntity(gm1.listSingleGridmeasures[0], false);
        expect(toasterMessageService.showWarn).toHaveBeenCalled();
    }));

});
