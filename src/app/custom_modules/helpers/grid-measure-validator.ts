/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { EntityValidator, ValidationFunc } from './entity-validator';
import { GridMeasure } from '../../model/grid-measure';
import { SingleGridMeasure } from '../../model/single-grid-measure';
import { ToasterMessageService } from '../../services/toaster-message.service';

class PlannedGridMeasureDateValidator extends ValidationFunc<GridMeasure> {
    validate(gridMeasure: GridMeasure, messageService: ToasterMessageService): boolean {
        const startDate = new Date(gridMeasure.plannedStarttimeFirstSinglemeasure || '1990-01-01T00:00:00z');
        const endDate = new Date(gridMeasure.endtimeGridmeasure || '2199-01-01T00:00:00z');
        if (startDate.getTime() > endDate.getTime()) {
            messageService.showWarn('Beginn der ersten geplanten Einzelmaßnahme ' +
                'muss vor Ende der Netzmaßnahme liegen');
            return false;
        }
        return true;
    }
}

class SingleGridMeasureDateValidator extends ValidationFunc<SingleGridMeasure> {
    validate(singleGridMeasure: SingleGridMeasure, messageService: ToasterMessageService): boolean {
        const startDate = new Date(singleGridMeasure.plannedStarttimeSinglemeasure || '1990-01-01T00:00:00z');
        const endDate = new Date(singleGridMeasure.plannedEndtimeSinglemeasure || '2199-01-01T00:00:00z');
        if (startDate.getTime() > endDate.getTime()) {
            messageService.showWarn('Beginn der geplanten Einzelmaßnahme ' +
                'muss vor Ende der geplanten Einzelmaßnahme liegen');
            return false;
        }
        return true;
    }
}

// Add more Validators here
export class GridMeasureValidatorFactory {
    public static createGM(messageService: ToasterMessageService) {
        return new EntityValidator<GridMeasure>(messageService,
            [
                new PlannedGridMeasureDateValidator(),
                // , new ... -> add more Validators here
            ]);
    }

    public static createsingleGM(messageService: ToasterMessageService) {
        return new EntityValidator<SingleGridMeasure>(messageService,
            [
                new SingleGridMeasureDateValidator()
                // , new ... -> add more Validators here
            ]);
    }
}

