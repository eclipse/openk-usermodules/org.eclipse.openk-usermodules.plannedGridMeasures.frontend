/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { ToasterMessageService } from '../../services/toaster-message.service';

export abstract class ValidationFunc<T> {
    public abstract validate(item: T, messageService: ToasterMessageService): boolean;
}

export class EntityValidator<T> {
    constructor(private messageService: ToasterMessageService,
        private validationList: ValidationFunc<T>[]) { }

    public validateEntity(entity: T, stopOnFirstError: boolean): boolean {
        let errorOccured = false;
        this.validationList.forEach(validator => {
            if (!(errorOccured && stopOnFirstError)) {
                errorOccured = !validator.validate(entity, this.messageService);
            }
        });
        return !errorOccured;
    }

}
