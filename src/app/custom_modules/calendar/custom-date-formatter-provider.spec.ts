/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/


import { CustomDateFormatter } from './custom-date-formatter-provider';

describe('CustomDateFormatter', () => {
  const customDateFormatter: CustomDateFormatter = new CustomDateFormatter();

  it('should format date to week view', () => {
    expect(customDateFormatter.weekViewTitle({date: new Date('12-12-2012'), locale: 'de'})).toBe('Kalenderwoche 50 / 2012');
    expect(customDateFormatter.weekViewTitle({date: null, locale: 'de'})).toBe(undefined);
  });

  it('should format date to day view', () => {
    expect(customDateFormatter.dayViewHour({date: new Date('01-01-2001 16:41'), locale: 'de'})).toBe('16:41');
  });
});
