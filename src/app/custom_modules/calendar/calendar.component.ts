/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { GridMeasureService } from './../../services/grid-measure.service';
import { GridMeasure } from './../../model/grid-measure';

import {
  startOfDay,
  endOfDay,
  subDays,
  addDays,
  endOfMonth,
  isSameDay,
  isSameMonth,
  addHours
} from 'date-fns';
import {
  CalendarEvent,
  CalendarEventAction,
  CalendarEventTimesChangedEvent,
  CalendarDateFormatter
} from 'angular-calendar';
import { Subject } from 'rxjs/Subject';
import { UndoRedoStackComponent } from '../undo-redo-stack/undo-redo-stack.component';
import { CustomDateFormatter } from './custom-date-formatter-provider';
import { Router } from '@angular/router';
import { ModeValidator } from '../helpers/mode-validator';
import { SessionContext } from '../../common/session-context';
import { Globals } from '../../common/globals';

@Component({
  selector: 'app-custom-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.css'],
  providers: [
    {
      provide: CalendarDateFormatter,
      useClass: CustomDateFormatter
    }
  ]
})


export class CustomCalendarComponent implements OnInit {
  @ViewChild('modalContent') modalContent: TemplateRef<any>;

  colorsForStatus: any;
  weekStartsOn = 1;
  locale = 'de';
  undoRedoStack = new UndoRedoStackComponent();
  eventTypes = this.colorsForStatus;
  activeDayIsOpen = true;
  refresh: Subject<any> = new Subject();

  modalData: {
    action: string;
    event: CalendarEvent;
  };

  view: any = 'week';
  currDate: Date = new Date();
  currentEditEventStart: Date = new Date();
  currentEditEventEnd: Date = new Date();
  currentEditEvent: CalendarEvent;
  events = new Array<CalendarEvent>();

  constructor(
    private gridMeasureService: GridMeasureService,
    private router: Router,
    public modeValidator: ModeValidator,
    private sessionContext: SessionContext) { }

  ngOnInit() {
    this.setColors();
    this.gridMeasureService.getCalender()
      .subscribe(singleGridMeasures => {
        const editAction = <CalendarEventAction>{
          label: '<span class="glyphicon glyphicon-pencil"></span>',
          onClick: ({ event }: { event: CalendarEvent }): void => {
            this.eventClicked('edit', event);
          }
        };
        const viewAction = <CalendarEventAction>{
          label: '<span class="glyphicon glyphicon-eye-open"></span>',
          onClick: ({ event }: { event: CalendarEvent }): void => {
            this.eventClicked('view', event);
          }
        };
        singleGridMeasures.forEach(singleGM => {
          if (!singleGM.plannedStarttimSinglemeasure || !singleGM.plannedEndtimeSinglemeasure) {
            console.log('No valide date values for ' + singleGM.singleGridMeasureTitle);
            console.log('Planned End Time Gridmeasure ' + singleGM.plannedEndtimeSinglemeasure);
            console.log('Start Time First Sequence: ' + singleGM.plannedStarttimSinglemeasure);
          } else {
            this.events.push(<CalendarEvent><GridMeasure>{
              id: singleGM.gridMeasureId,
              title: singleGM.singleGridMeasureTitle || 'TITLE NOT DEFINED',
              start: new Date(singleGM.plannedStarttimSinglemeasure),
              end: new Date(singleGM.plannedEndtimeSinglemeasure),
              color: this.colorsForStatus[singleGM.gridMeasureStatusId],
              draggable: false,
              actions: [this.modeValidator.isEditModeAllowed(singleGM.gridMeasureStatusId) ? editAction : viewAction],
              resizable: {
                beforeStart: false,
                afterEnd: false
              },
              meta: singleGM
            });
          }
        });

        this.currDate = new Date();
        this.showGridMeasuresIfThere();
      });
  }

  private showGridMeasuresIfThere() {
    const numberOfGMsInActualDay = this.events.filter(evt =>
      (evt.start.getDate() <= this.currDate.getDate() && evt.end.getDate() >= this.currDate.getDate()));

    this.activeDayIsOpen = numberOfGMsInActualDay.length > 0;
  }

  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    this.currentEditEvent = null;
    if (isSameMonth(date, this.currDate)) {
      if (
        (isSameDay(this.currDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ) {
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
        this.currDate = date;
      }
    }
  }

  eventClicked(action: string, event: CalendarEvent<GridMeasure>): void {
    const isEditModeAllowed = this.modeValidator.isEditModeAllowed(event.meta.statusId);
    this.router.navigate(['/gridMeasureDetail/', event.id, isEditModeAllowed ? 'edit' : 'view']);
  }

  eventTimesChanged(eventchanged: CalendarEventTimesChangedEvent): void {
    const event = <CalendarEvent><GridMeasure>eventchanged.event;
    event.meta.plannedStarttimSinglemeasure = eventchanged.newStart;
    event.meta.plannedEndtimeSinglemeasure = eventchanged.newEnd;

    event.start = eventchanged.newStart;
    event.end = eventchanged.newEnd;
    this.refresh.next();
  }


  setColors() {
    const newColor = this.sessionContext.getStatusById(Globals.STATUS.NEW).colorCode;
    const appliedColor = this.sessionContext.getStatusById(Globals.STATUS.APPLIED).colorCode;
    const canceledColor = this.sessionContext.getStatusById(Globals.STATUS.CANCELED).colorCode;
    const forApprovalColor = this.sessionContext.getStatusById(Globals.STATUS.FORAPPROVAL).colorCode;
    const approvedColor = this.sessionContext.getStatusById(Globals.STATUS.APPROVED).colorCode;
    const requestedColor = this.sessionContext.getStatusById(Globals.STATUS.REQUESTED).colorCode;
    const releasedColor = this.sessionContext.getStatusById(Globals.STATUS.RELEASED).colorCode;
    const activeColor = this.sessionContext.getStatusById(Globals.STATUS.ACTIVE).colorCode;
    const inWorkColor = this.sessionContext.getStatusById(Globals.STATUS.IN_WORK).colorCode;
    const workFinishedColor = this.sessionContext.getStatusById(Globals.STATUS.WORK_FINISHED).colorCode;
    const finishedColor = this.sessionContext.getStatusById(Globals.STATUS.FINISHED).colorCode;
    const closedColor = this.sessionContext.getStatusById(Globals.STATUS.CLOSED).colorCode;
    const rejectedColor = this.sessionContext.getStatusById(Globals.STATUS.REJECTED).colorCode;

    this.colorsForStatus = {
      0: {
        primary: newColor,
        secondary: this.setRGBAColorTransparency(newColor)
      },
      1: {
        primary: appliedColor,
        secondary: this.setRGBAColorTransparency(appliedColor)
      },
      2: {
        primary: canceledColor,
        secondary: this.setRGBAColorTransparency(canceledColor)
      },
      3: {
        primary: forApprovalColor,
        secondary: this.setRGBAColorTransparency(forApprovalColor)
      },
      4: {
        primary: approvedColor,
        secondary: this.setRGBAColorTransparency(approvedColor)
      },
      5: {
        primary: requestedColor,
        secondary: this.setRGBAColorTransparency(requestedColor)
      },
      6: {
        primary: releasedColor,
        secondary: this.setRGBAColorTransparency(releasedColor)
      },
      7: {
        primary: activeColor,
        secondary: this.setRGBAColorTransparency(activeColor)
      },
      8: {
        primary: inWorkColor,
        secondary: this.setRGBAColorTransparency(inWorkColor)
      },
      9: {
        primary: workFinishedColor,
        secondary: this.setRGBAColorTransparency(workFinishedColor)
      },
      10: {
        primary: finishedColor,
        secondary: this.setRGBAColorTransparency(finishedColor)
      },
      11: {
        primary: closedColor,
        secondary: this.setRGBAColorTransparency(closedColor)
      },
      12: {
        primary: rejectedColor,
        secondary: this.setRGBAColorTransparency(rejectedColor)
      }
    };
  }

  setRGBAColorTransparency(colorRGBA: string): string {
    return colorRGBA.replace('1)', Globals.CALENDAR_COLOR_TRANSPARENCY_VALUE + ')');
  }

}
