/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { ComponentFixture, TestBed, async, fakeAsync, tick, inject } from '@angular/core/testing';
import { AbstractMockObservableService } from '../../testing/abstract-mock-observable.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MockComponent } from '../../testing/mock.component';
import { registerLocaleData } from '@angular/common';
import localeDe from '@angular/common/locales/de';
import { FormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { SessionContext } from './../../common/session-context';
import { Globals } from '../../common/globals';
import { CustomCalendarComponent } from './calendar.component';
import { CustomDateFormatter } from './custom-date-formatter-provider';
import { GridMeasureService } from '../../services/grid-measure.service';
import { UserSettingsService } from '../../services/user-settings.service';
import { RoleAccessHelperService } from '../../services/jobs/role-access-helper.service';
import { RoleAccess } from '../../model/role-access';
import { GridMeasure } from '../../model/grid-measure';
import { UserSettings } from './../../model/user-settings';
import { USERS } from './../../test-data/users';

import {
  startOfDay,
  endOfDay,
  subDays,
  addDays,
  endOfMonth,
  isSameDay,
  isSameMonth,
  addHours
} from 'date-fns';

import {
  CalendarEventTitleFormatter,
  CalendarModule,
  CalendarEvent,
  CalendarEventAction,
  CalendarEventTimesChangedEvent,
  CalendarDateFormatter,
  CalendarDayModule,
  CalendarWeekViewComponent,
  CalendarMonthViewComponent,
  CalendarDayViewComponent
} from 'angular-calendar';
import { ModeValidator } from '../helpers/mode-validator';
import { CalendarEntry } from '../../model/calendar-entry';
import { CALENDARENTRY } from '../../test-data/calendar-entry';

class FakeRouter {
  navigate(commands: any[]) {
    return commands[0];
  }
}

describe('CustomCalendarComponent', () => {
  registerLocaleData(localeDe);
  let component: CustomCalendarComponent;
  let fixture: ComponentFixture<CustomCalendarComponent>;
  const calendarEntries: CalendarEntry[] = JSON.parse(JSON.stringify(CALENDARENTRY));
  let routerStub: FakeRouter;
  let sessionContext;

  routerStub = {
    navigate: jasmine.createSpy('navigate').and.callThrough()
  };

  class GridmeasureToEventHelper {
    public createEventsFromSingleGridMeasures(singleGMs: CalendarEntry[], mode: string) {
      const events: CalendarEvent<GridMeasure>[] = new Array<CalendarEvent>();
      const editAction = <CalendarEventAction>{
        label: '<i class="glyphicon glyphicon-pencil"></i>'
      };
      const viewAction = <CalendarEventAction>{
        label: '<i class="glyphicon glyphicon-eye-open"></i>'
      };
      singleGMs.forEach(singleGM => {
        if (!singleGM.plannedStarttimSinglemeasure || !singleGM.plannedEndtimeSinglemeasure) {
          console.log('No valide date values for ' + singleGM.singleGridMeasureTitle);
          console.log('Planned End Time Gridmeasure ' + singleGM.plannedEndtimeSinglemeasure);
          console.log('Start Time First Sequence: ' + singleGM.plannedStarttimSinglemeasure);
        } else {
          events.push(<CalendarEvent><GridMeasure>{
            id: singleGM.gridMeasureId,
            title: singleGM.singleGridMeasureTitle || 'TITLE NOT DEFINED',
            start: new Date(singleGM.plannedStarttimSinglemeasure),
            end: new Date(singleGM.plannedEndtimeSinglemeasure),
            color: {
              primary: '#ad2121',
              secondary: '#FAE3E3'
            },
            draggable: true,
            actions: [mode === 'edit' ? editAction : viewAction],
            resizable: {
              beforeStart: true,
              afterEnd: true
            },
            meta: singleGM
          });
        }
      });
      return events;
    }
  }

  class MockGridMeasureService extends AbstractMockObservableService {
    getGridMeasures() {
      return this;
    }
    getCalender() {
      return this;
    }
  }

  class MockUserSettingService extends AbstractMockObservableService {
    savedUserSettings: UserSettings;
    getUserSettings(gridId: string) {
      return this;
    }
    setUserSettings(userSettings: UserSettings) {
      this.savedUserSettings = userSettings;
      return this;
    }
  }

  let mockUserSettingService;
  let roleAccessHelper: RoleAccessHelperService;
  let mockGridMeasureService;

  beforeEach(async(() => {
    sessionContext = new SessionContext();
    mockGridMeasureService = new MockGridMeasureService();
    mockUserSettingService = new MockUserSettingService();
    roleAccessHelper = new RoleAccessHelperService();

    sessionContext.setCurrUser(USERS[1]);
    sessionContext.setAllUsers(USERS);

    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        BrowserAnimationsModule,
        CalendarModule.forRoot({
          dateFormatter: {
            provide: CalendarDateFormatter,
            useClass: CustomDateFormatter
          }
        })

      ],
      declarations: [
        CustomCalendarComponent,
        MockComponent({ selector: 'input', inputs: ['options'] }),
        MockComponent({ selector: 'app-grid-measures', inputs: ['gridId', 'withEditButtons'] }),
        MockComponent({ selector: 'app-loading-spinner', inputs: [] }),
        MockComponent({
          selector: 'app-buttons-container',
          inputs: ['activeButtons', 'isValidForm', 'isValidForSave', 'isReadOnlyForm', 'gridMeasureStatusId']
        })
      ],
      providers: [
        ModeValidator,
        { provide: SessionContext, useValue: sessionContext },
        { provide: Router, useValue: routerStub },
        { provide: GridMeasureService, useValue: mockGridMeasureService },
        { provide: RoleAccessHelperService, useValue: roleAccessHelper },
        { provide: UserSettingsService, useValue: mockUserSettingService },
        { provide: CalendarDateFormatter, useClass: CustomDateFormatter }
      ]
    }).compileComponents();
  })
  );

  let eventTitle: CalendarEventTitleFormatter;
  beforeEach(
    inject([CalendarEventTitleFormatter], _eventTitle_ => {
      eventTitle = _eventTitle_;
    })
  );

  beforeEach(fakeAsync(() => {
    fixture = TestBed.createComponent(CustomCalendarComponent);
    tick();
    component = fixture.componentInstance;
    component.currDate = new Date('2017-01-15');
    sessionContext.setCurrUser(USERS[1]);
    sessionContext.setAllUsers(USERS);
    fixture.detectChanges();

    // we need to init the component and the path... because of OnInit
    mockGridMeasureService.content = JSON.parse(JSON.stringify(CALENDARENTRY[0]));
    const roleAcess: RoleAccess = {
      editRoles: [{
        name: 'planned-policies-measureplanner',
        gridMeasureStatusIds: [
          0,
          1
        ]
      }, {
        name: 'planned-policies-superuser',
        gridMeasureStatusIds: [
          0,
          1
        ]
      }, {
        name: 'planned-policies-measureapplicant',
        gridMeasureStatusIds: [
          0,
          1
        ]
      }],
      controls: [{
        gridMeasureStatusId: 0,
        activeButtons: [
          'save',
          'apply',
          'cancel'
        ],
        inactiveFields: [
          'titeldermassnahme'
        ]
      },
      {
        gridMeasureStatusId: 1,
        activeButtons: [
          'save',
          'cancel',
          'forapproval'
        ],
        inactiveFields: [
          'titeldermassnahme'
        ]
      }],
      stornoSection:
      {
        'stornoRoles': [
          'planned-policies-measureapplicant',
          'planned-policies-measureplanner',
          'planned-policies-measureapprover',
          'planned-policies-requester',
          'planned-policies-clearance'
        ]
      },
      duplicateSection:
      {
        'duplicateRoles': [
          'planned-policies-measureapplicant'
        ]
      }

    };
    roleAccessHelper.init(roleAcess);


  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should generate the day view', () => {
    const fixtureDayView: ComponentFixture<CalendarDayViewComponent> = TestBed.createComponent(CalendarDayViewComponent);
    fixtureDayView.componentInstance.viewDate = new Date('2017-01-15');
    fixtureDayView.componentInstance.events = [
      {
        start: new Date('2017-01-15'),
        title: 'Test grid-measure',
        color: {
          primary: '',
          secondary: ''
        }
      }
    ];
    fixtureDayView.componentInstance.ngOnChanges({ viewDate: {}, events: {} });
    expect(fixtureDayView.componentInstance.view.events.length).toBe(1);
    expect(fixtureDayView.componentInstance.view.events[0].event).toBe(
      fixtureDayView.componentInstance.events[0]
    );
    expect(fixtureDayView.componentInstance.hours.length).toBe(24);

  });

  it('should generate the week view', () => {
    const fixtureWeekView: ComponentFixture<CalendarWeekViewComponent> = TestBed.createComponent(CalendarWeekViewComponent);
    fixtureWeekView.componentInstance.viewDate = new Date('2017-01-15');
    fixtureWeekView.componentInstance.events = [
      {
        start: new Date('2017-01-15'),
        title: 'Test grid-measure',
        color: {
          primary: '',
          secondary: ''
        }
      }
    ];
    fixtureWeekView.componentInstance.ngOnChanges({ viewDate: {}, events: {} });
    expect(fixtureWeekView.componentInstance.view.eventRows.length).toBe(1);
    expect(fixtureWeekView.componentInstance.view.eventRows[0].row[0].event).toBe(
      fixtureWeekView.componentInstance.events[0]
    );

  });

  it('should generate the month view', () => {
    const fixtureMonthView: ComponentFixture<CalendarMonthViewComponent> = TestBed.createComponent(CalendarMonthViewComponent);
    fixtureMonthView.componentInstance.viewDate = new Date('2017-01-15');
    fixtureMonthView.componentInstance.events = [
      {
        start: new Date('2017-01-15'),
        end: new Date('2017-01-20'),
        title: 'Test grid-measure',
        color: {
          primary: '',
          secondary: ''
        }
      }
    ];
    fixtureMonthView.componentInstance.ngOnChanges({ viewDate: {}, events: {} });
    expect(fixtureMonthView.componentInstance.events.length).toBe(1);
    expect(fixtureMonthView.componentInstance.events[0]).toBe(
      fixtureMonthView.componentInstance.events[0]
    );

  });

  it('should set trasparency to rgba color if this has a vlaue from 1', () => {
    spyOn(component, 'setRGBAColorTransparency').and.callThrough();
    fixture.detectChanges();
    component.setRGBAColorTransparency('rgba(111,222,333,1)');
    fixture.detectChanges();
    expect(component.setRGBAColorTransparency('rgba(111,222,333,1)')).toBe('rgba(111,222,333,' +
      Globals.CALENDAR_COLOR_TRANSPARENCY_VALUE + ')');
  });
});
