/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { DebugElement } from '@angular/core';
import { tick, ComponentFixture } from '@angular/core/testing';

// export * from './jasmine-matchers';
export * from './router-stubs';

///// Short utilities /////

/** Wait a tick, then detect changes */
export function advance(f: ComponentFixture<any>): void {
  tick();
  f.detectChanges();
}

/**
 * Create custom DOM event the old fashioned way
 *
 * https://developer.mozilla.org/en-US/docs/Web/API/Event/initEvent
 * Although officially deprecated, some browsers (phantom) don't accept the preferred "new Event(eventName)"
 */
export function newEventDepr(eventName: string, bubbles = false, cancelable = false) {
  const evt = document.createEvent('CustomEvent');  // MUST be 'CustomEvent'
  evt.initCustomEvent(eventName, bubbles, cancelable, null);
  return evt;
}

export function newEvent(eventName: string, bubbles = false, cancelable = false) {
  const evt = new CustomEvent(eventName, {
    'bubbles': bubbles,
    'cancelable': cancelable
  });
  return evt;
}

// See https://developer.mozilla.org/en-US/docs/Web/API/MouseEvent/button
/** Button events to pass to `DebugElement.triggerEventHandler` for RouterLink event handler */
export const ButtonClickEvents = {
  left: { button: 0 },
  right: { button: 2 }
};

/** Simulate element click. Defaults to mouse left-button click event. */
export function click(el: DebugElement | HTMLElement, eventObj: any = ButtonClickEvents.left): void {
  if (el instanceof HTMLElement) {
    el.click();
  } else {
    el.triggerEventHandler('click', eventObj);
  }
}

/** Simulate element focus. */
export function focus(el: DebugElement | HTMLElement, eventObj: any = null): void {
  if (el instanceof HTMLElement) {
    el.focus();
  } else {
    el.triggerEventHandler('focus', eventObj);
  }
}

/** Simulate pressing a key with keyCode. */
export function pressKey(de: DebugElement, event: string, keyCode: number): void {
  const keyEventObj = new Event(event);
  Object.defineProperty(keyEventObj, 'keyCode', { 'value': keyCode });
  de.triggerEventHandler(event, keyEventObj);
}

/*
Copyright 2017 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/
