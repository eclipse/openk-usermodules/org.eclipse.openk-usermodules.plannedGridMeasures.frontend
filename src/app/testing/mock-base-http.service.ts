/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { SessionContext } from '../common/session-context';
import { BaseHttpServiceInterface, HttpCallInfo } from '../services/base-http.service';
import { AbstractMockObservableService } from './abstract-mock-observable.service';




export class MockBaseHttpService extends AbstractMockObservableService implements BaseHttpServiceInterface {
    public lastHttpCallInfo: HttpCallInfo;
    public lastSessionContext: SessionContext;

    constructor() {
        super();
    }

    public callService(callInfo: HttpCallInfo, sessionContext: SessionContext) {
        this.lastHttpCallInfo = callInfo;
        this.lastSessionContext = sessionContext;
        return this;
    }
}
