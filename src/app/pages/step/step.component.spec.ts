/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { ComponentFixture, TestBed, async, fakeAsync, tick } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { SimpleChange } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AbstractMockObservableService } from '../../testing/abstract-mock-observable.service';
import { MockComponent } from '../../testing/mock.component';
import { ActivatedRouteStub } from '../../testing/router-stubs';
import { Globals } from './../../common/globals';
import { StepComponent } from './step.component';
import { SessionContext } from './../../common/session-context';
import { CimCacheService } from './../../services/cim-cache.service';
import { GridMeasureService } from '../../services/grid-measure.service';
import { USERS } from '../../test-data/users';
import { Step } from '../../model/step';
import { GRIDMEASURE } from '../../test-data/grid-measures';
import {
    RESSOURCEWITHTYPERESPONSEEMPTYLIST,
    RESSOURCETYPESRESPONSE, RESSOURCEWITHTYPERESPONSE, RESSOURCEWITHTYPERESPONSE_2
} from '../../test-data/cim-cache-responses';
import { TREE_STRUCTURE } from '../../test-data/tree';
import { POWERSYSTEMRESOURCES } from '../../test-data/power-system-resources';
import { ToasterMessageService } from '../../services/toaster-message.service';
import { MessageService } from 'primeng/api';

class FakeRouter {
    navigate(commands: any[]) {
        return commands[0];
    }
}

describe('StepComponent', () => {
    let component: StepComponent;
    let fixture: ComponentFixture<StepComponent>;
    let routerStub: FakeRouter;
    let activatedStub: ActivatedRouteStub;
    let sessionContext: SessionContext;

    let mockCimCacheService;
    let mockGridmeasureService;
    let toasterMessageService: ToasterMessageService;
    let messageService: MessageService;


    routerStub = {
        navigate: jasmine.createSpy('navigate').and.callThrough()
    };

    class MockCimCacheService extends AbstractMockObservableService {
        getRessourceTypes() {
            return this;
        }

        getRessourcesWithType() {
            return this;
        }

    }

    class MockGridmeasureService extends AbstractMockObservableService {
        storeGridMeasure() {
            return this;
        }
        getGridMeasure(id: number) {
            return this;
        }
    }

    beforeEach(async(() => {
        messageService = new MessageService;
        activatedStub = new ActivatedRouteStub();
        mockCimCacheService = new MockCimCacheService();
        sessionContext = new SessionContext();
        toasterMessageService = new ToasterMessageService(sessionContext, messageService);
        mockGridmeasureService = new MockGridmeasureService();

        TestBed.configureTestingModule({
            imports: [
                FormsModule
            ],
            declarations: [
                StepComponent,
                MockComponent({ selector: 'input', inputs: ['options'] }),
                MockComponent({ selector: 'tree', inputs: ['tree', 'settings'] })
            ],
            providers: [
                { provide: ActivatedRoute, useValue: activatedStub },
                { provide: Router, useValue: routerStub },
                { provide: SessionContext, useValue: sessionContext },
                { provide: ToasterMessageService, useValue: toasterMessageService },
                { provide: CimCacheService, useValue: mockCimCacheService },
                { provide: GridMeasureService, useValue: mockGridmeasureService }
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(StepComponent);
        component = fixture.componentInstance;
        sessionContext.setCurrUser(USERS[0]);
        sessionContext.setAllUsers(USERS);
        component.isReadOnlyForm = false;
        component.dateTimePattern =
            '^(([0-2]?[0-9]|3[0-1])\.([0]?[1-9]|1[0-2])\.[1-2][0-9]{3}) (20|21|22|23|[0-1]?[0-9]{1}):([0-5]?[0-9]{1})$';
        component.dateFormatLocale = 'dd.MM.yyyy HH:mm';
        component.isCollapsible = true;
        component.singleGridMeasure = GRIDMEASURE[0].listSingleGridmeasures[0];
        activatedStub.testParams = { id: 555, mode: Globals.MODE.EDIT };
        component.ngOnInit();
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should set form in readonly mode', () => {
        component.readOnlyForm = false;
        fixture.detectChanges();
        component.ngOnChanges({
            isReadOnlyForm: new SimpleChange(component.readOnlyForm, true, true)
        });
        fixture.detectChanges();
        expect(component.readOnlyForm).toBeTruthy();
    });

    it('should handle cim-service error on init', fakeAsync(() => {
        spyOn(console, 'log').and.callThrough();
        component.singleGridMeasure = GRIDMEASURE[0].listSingleGridmeasures[0];
        mockCimCacheService.error = 'CimCache error';
        mockCimCacheService.content = [];

        component.ngOnInit();
        tick();

        fixture.detectChanges();
        expect(console.log).toHaveBeenCalled();
    }));

    it('should call getRessourceTypes on init', fakeAsync(() => {

        spyOn((component as any), 'getRessourceTypes').and.callThrough();
        spyOn((component as any), 'processRessourceTypesResponse').and.callThrough();
        spyOn((component as any), 'getRessourceTypesWithType').and.callThrough();
        (component as any).cimCacheService.content = RESSOURCETYPESRESPONSE;
        component.singleGridMeasure = GRIDMEASURE[0].listSingleGridmeasures[0];

        component.ngOnInit();
        tick();

        fixture.detectChanges();
        expect((component as any).getRessourceTypes).toHaveBeenCalled();

        fixture.whenStable().then(() => {
            fixture.detectChanges();
            expect((component as any).processRessourceTypesResponse).toHaveBeenCalled();

            component.onChangeResourceGroup(null);
            fixture.detectChanges();
            expect(component.stepAffectedResourcesList.length).toBe(0);

            component.onChangeResourceGroup('ac-line-segment');
            fixture.detectChanges();
            expect((component as any).getRessourceTypesWithType).toHaveBeenCalled();
        });
    }));

    it('should convert xmlstring to json correctly', () => {
        const xmlstring = `<root>
        <child><textNode>First &amp; Child</textNode></child>
        <child><textNode>Second Child</textNode></child>
        <testAttrs attr1='attr1Value'/>
        </root>`;
        const jsonstring =
            `{"root":{"child":[{"textNode":"First & Child"},{"textNode":"Second Child"}],"testAttrs":{"_attr1":"attr1Value"}}}`;
        expect(JSON.stringify(component.convertXmlToJsonObj(xmlstring))).toBe(jsonstring);
    });

    it('should processRessourceWithTypeResponse with empty result correctly', () => {
        const res_input = RESSOURCEWITHTYPERESPONSEEMPTYLIST;
        component.stepAffectedResourcesList = [];
        component.tmpPowerSystemResource = null;
        const tmp = component.processRessourceWithTypeResponse(res_input);
        fixture.detectChanges();
        expect(tmp[0].cimName).toBe('keine Daten');
    });

    it('should processRessourceWithTypeResponse correctly', () => {
        const res_input = RESSOURCEWITHTYPERESPONSE;

        component.tmpPowerSystemResource = null;
        const tmp = component.processRessourceWithTypeResponse(res_input);
        fixture.detectChanges();
        expect(tmp[0].cimName).toBe('PowerTransformer');
    });

    it('should processRessourceWithTypeResponse with array type correctly', () => {
        const res_input = RESSOURCEWITHTYPERESPONSE_2;

        component.stepAffectedResourcesList = [];
        const tmp = component.processRessourceWithTypeResponse(res_input);
        fixture.detectChanges();
        expect(tmp.length).toBe(2);
    });

    it('should processRessourceTypesResponse correctly', () => {
        const res_input = RESSOURCETYPESRESPONSE;
        component.stepAffectedResourcesGroupList = [];
        component.processRessourceTypesResponse(res_input);
        fixture.detectChanges();
        expect(component.stepAffectedResourcesGroupList.length).toBe(21);
    });

    it('should not be in able to add step if not all fileds are completed', () => {
        component.singleGridMeasure.listSteps = [];
        component.step = new Step();
        // component.step.singleGridmeasureId = 3;
        // component.step.switchingObject = 'Regler 123';
        // component.step.targetState = '42';
        component.processAddStep();
        fixture.detectChanges();
        expect(component.singleGridMeasure.listSteps.length).toBe(0);
    });

    it('should add step correctly', () => {
        component.step = new Step();
        component.step.singleGridmeasureId = 3;
        component.step.switchingObject = 'Regler 123';
        component.step.targetState = '42';
        component.step.presentTime = '22:44';
        component.step.operator = 'otto';
        component.step.type = 'supertype';
        component.step.presentState = 'top';
        const numberOflistSteps = component.singleGridMeasure.listSteps.length;
        component.processAddStep();
        fixture.detectChanges();
        expect(component.singleGridMeasure.listSteps.length).toBe(numberOflistSteps + 1);
    });

    it('should emit warning message for empty step', async(() => {
        spyOn(toasterMessageService, 'showWarn').and.callThrough();

        component.step = new Step();
        component.step.singleGridmeasureId = 3;
        component.step.switchingObject = null;
        component.step.targetState = null;

        component.processAddStep();

        fixture.whenStable().then(() => {
            fixture.detectChanges();
            expect(toasterMessageService.showWarn).toHaveBeenCalled();
        });
    }));

    it('should handleSelected element from tree', () => {
        const tree = JSON.parse(JSON.stringify(TREE_STRUCTURE));
        component.handleSelected(tree);
        fixture.detectChanges();
        expect(component.step.switchingObject).toBe(tree.node.value + '');
    });

    it('should handleSelected element from tree', () => {
        spyOn(component, 'handleNextLevel').and.callThrough();
        const tree = JSON.parse(JSON.stringify(TREE_STRUCTURE));
        component.handleNextLevel(tree);
        fixture.detectChanges();
        expect(component.handleNextLevel).toHaveBeenCalled();
    });

    it('should validate step form', () => {
        spyOn(component, 'onStepFormValidation').and.callThrough();
        component.onStepFormValidation(true);
        fixture.detectChanges();
        expect(component.step._isValide).toBeTruthy();
    });

    it('should validate step form', () => {
        spyOn(component, 'onStepFormValidation').and.callThrough();
        component.onStepFormValidation(true);
        fixture.detectChanges();
        expect(component.step._isValide).toBeTruthy();
    });

});
