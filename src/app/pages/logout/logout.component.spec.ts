/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NgModule } from '@angular/core';
import { LogoutPageComponent } from './logout.component';
import { LoggedoutPageComponent } from '../loggedout/loggedout.component';
import { MockComponent } from '../../testing/mock.component';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../services/authentication.service';
import { AbstractMockObservableService } from '../../testing/abstract-mock-observable.service';
import { Observable } from 'rxjs/Observable';
import { SessionContext } from './../../common/session-context';
import { MessageService } from 'primeng/api';
import { ToasterMessageService } from '../../services/toaster-message.service';

class FakeRouter {
  navigate(commands: any[]) {
    return commands[0];
  }
}
@NgModule({
  declarations: [LoggedoutPageComponent]
})
class TestModule { }

class MockAuthService extends AbstractMockObservableService {

  logout() {
    return this;
  }
}

describe('LogoutComponent', () => {
  let router: Router;
  let component: LogoutPageComponent;
  let fixture: ComponentFixture<LogoutPageComponent>;
  let routerStub: FakeRouter;
  let mockAuthService;
  let toasterMessageService;
  let sessionContext;
  let messageService;
  routerStub = {
    navigate: jasmine.createSpy('navigate').and.callThrough()
  };

  beforeEach(async(() => {
    messageService = new MessageService();
    sessionContext = new SessionContext();
    router = new FakeRouter() as any as Router;
    mockAuthService = new MockAuthService();
    toasterMessageService = new ToasterMessageService(sessionContext, messageService);

    TestBed.configureTestingModule({
      declarations: [LogoutPageComponent,
        LoggedoutPageComponent,
        MockComponent({ selector: 'app-version-info' })
      ],
      providers: [
        { provide: Router, useValue: routerStub },
        { provide: ToasterMessageService, useValue: toasterMessageService },
        { provide: AuthenticationService, useValue: mockAuthService },
        { provide: SessionContext, useClass: SessionContext }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LogoutPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should navigate to Overview after click on no-button', async(() => {
    spyOn(component, 'goToOverview').and.callThrough();

    fixture.detectChanges();

    fixture.whenStable().then(() => {
      const button = fixture.debugElement.nativeElement.querySelector('button#homebutton');
      button.click();
      fixture.detectChanges();
      expect(component.goToOverview).toHaveBeenCalled();
      expect(routerStub.navigate).toHaveBeenCalledWith(['/overview']);
    });
  }));

  it('should logout after click on yes-button', async(() => {
    mockAuthService.content = Observable.of('');
    spyOn(component, 'logout').and.callThrough();

    fixture.detectChanges();

    fixture.whenStable().then(() => {
      const button = fixture.debugElement.nativeElement.querySelector('button#logoutbutton');
      button.click();
      fixture.detectChanges();
      expect(component.logout).toHaveBeenCalled();
      expect(routerStub.navigate).toHaveBeenCalledWith(['/loggedout']);
    });
  }));

  it('should logout on error response', async(() => {
    mockAuthService.error = 'MOCKERROR';
    spyOn(component, 'logout').and.callThrough();

    fixture.detectChanges();

    fixture.whenStable().then(() => {
      const button = fixture.debugElement.nativeElement.querySelector('button#logoutbutton');
      button.click();
      fixture.detectChanges();
      expect(component.loggedOut).toBeFalsy();
      expect(component.logout).toHaveBeenCalled();
      expect(routerStub.navigate).toHaveBeenCalledWith(['/loggedout']);
    });
  }));

});
