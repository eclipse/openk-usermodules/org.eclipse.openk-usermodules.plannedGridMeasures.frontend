/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { MessageDefines, ToasterMessageService } from '../../services/toaster-message.service';
import { AuthenticationService } from '../../services/authentication.service';
import { SessionContext } from '../../common/session-context';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutPageComponent implements OnInit {

  @ViewChild('yesBtn') button: ElementRef;
  loggedOut = false;

  ngOnInit() {
    this.button.nativeElement.focus();
  }


  constructor(
    private msgService: ToasterMessageService,
    private authService: AuthenticationService,
    private router: Router,
    private sessionContext: SessionContext
  ) { }

  logout() {
    this.authService.logout().subscribe(res => {
      this.msgService.loginLogoff$.emit(MessageDefines.MSG_LOG_OFF);
      this.loggedOut = true;
      this.router.navigate(['/loggedout']);
      this.sessionContext.clearStorage();
    },
      error => {
        console.log(error);
        this.router.navigate(['/loggedout']);
      }
    );
  }

  goToOverview(): void {
    this.router.navigate(['/overview']);
  }
}
