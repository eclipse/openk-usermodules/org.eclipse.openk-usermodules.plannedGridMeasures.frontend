/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { Component, OnInit } from '@angular/core';
import { Globals } from '../../common/globals';
import { SessionContext } from '../../common/session-context';
import { GridMeasure } from '../../model/grid-measure';
import { GridMeasureValidatorFactory } from '../../custom_modules/helpers/grid-measure-validator';
import { ErrorType } from '../../common/enums';
import { GridMeasureService } from '../../services/grid-measure.service';
import { Router } from '@angular/router';
import { ToasterMessageService } from '../../services/toaster-message.service';

@Component({
  selector: 'app-cancel-grid-measure',
  templateUrl: './cancel-grid-measure.component.html',
  styleUrls: ['./cancel-grid-measure.component.css']
})
export class CancelGridMeasureComponent implements OnInit {

  Globals = Globals;
  storageInProgress = false;
  gridMeasureDetail: GridMeasure;
  cancelReasonText: string;

  constructor(
    public sessionContext: SessionContext,
    private gridMeasureService: GridMeasureService,
    public router: Router,
    private toasterMessageService: ToasterMessageService) { }

  ngOnInit() {
    this.gridMeasureDetail = this.sessionContext.getGridMeasureDetail();
  }

  doCancel() {
    if (this.cancelReasonText) {
      let oldRemark: string;
      this.gridMeasureDetail = this.sessionContext.getGridMeasureDetail();
      if (!this.gridMeasureDetail.remark) {
        oldRemark = '';
      } else {
        oldRemark = this.gridMeasureDetail.remark + '\n\n';
      }
      this.gridMeasureDetail.remark = oldRemark + '----- Storno Begründung -----\n' + this.cancelReasonText + '\n';
      this.updateGridMeasureStatus(Globals.STATUS.CANCELED);
      this.sessionContext.setCancelStage(false);
    } else {
      this.toasterMessageService.showWarn('Bitte geben Sie eine Begründung ein!');
    }

  }


  private updateGridMeasureStatus(status: number) {
    if (this.storageInProgress) {
      return;
    } else {
      this.storageInProgress = true;
    }
    this.gridMeasureDetail.createUser = this.gridMeasureDetail.createUser || this.sessionContext.getCurrUser().username;
    this.gridMeasureDetail.statusId = status;

    if (!GridMeasureValidatorFactory.createGM(this.toasterMessageService).validateEntity(this.gridMeasureDetail, true)) {
      this.storageInProgress = false;
      return;
    }
    this.mergeDeletedStepsAndResetMinusIds();
    this.gridMeasureService.storeGridMeasure(this.gridMeasureDetail).subscribe(gm => {
      this.storageInProgress = false;
      // this.messageService.emitInfo('Netzmaßnahmes "' + gm.title + '" Status erfolgreich zu "'
      //   + this.sessionContext.getStatusById(gm.statusId).name + '" geändert!', MessageScopeEn.global);
      this.toasterMessageService.showSuccess('Netzmaßnahmes "' + gm.title + '" Status erfolgreich zu "'
        + this.sessionContext.getStatusById(gm.statusId).name + '" geändert!');
      this.goToOverview();
    },
      error => {
        this.storageInProgress = false;
        // this.messageService.emitError('Netzmaßnahme', ErrorType.stornoLocked);
        this.toasterMessageService.showError(ErrorType.stornoLocked, 'Netzmaßnahme');
        console.log(error);
      }
    );
  }

  private mergeDeletedStepsAndResetMinusIds() {
    this.gridMeasureDetail.listSingleGridmeasures.forEach(sgm => {
      if (sgm.listSteps) {
        sgm.listSteps.forEach(step => {
          if (step.id === Globals.TEMP_ID_TO_SHOW_NEW_STEPS) {
            delete step.id;
          }
        });
      }
      if (sgm.listStepsDeleted) {
        sgm.listSteps.push.apply(sgm.listSteps, sgm.listStepsDeleted);
      }
    });
  }

  goToOverview() {
    this.router.navigate(['/overview']);
  }

}
