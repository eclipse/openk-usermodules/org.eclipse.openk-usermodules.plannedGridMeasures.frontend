/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';

import { CancelGridMeasureComponent } from './cancel-grid-measure.component';
import { SessionContext } from '../../common/session-context';
import { Router } from '../../../../node_modules/@angular/router';
import { GridMeasureService } from '../../services/grid-measure.service';
import { AbstractMockObservableService } from '../../testing/abstract-mock-observable.service';
import { RouterStub } from '../../testing';
import { FormsModule } from '../../../../node_modules/@angular/forms';
import { GRIDMEASURE } from '../../test-data/grid-measures';
import { Globals } from '../../common/globals';
import { GridMeasure } from '../../model/grid-measure';
import { ToasterMessageService } from '../../services/toaster-message.service';
import { MessageService } from 'primeng/api';

describe('CancelGridMeasureComponent', () => {

  class MockService extends AbstractMockObservableService {
    storeGridMeasure() {
      return this;
    }
  }

  const gridmeasures: GridMeasure[] = JSON.parse(JSON.stringify(GRIDMEASURE));
  let routerStub: RouterStub;
  let component: CancelGridMeasureComponent;
  let fixture: ComponentFixture<CancelGridMeasureComponent>;
  let messageService: MessageService;
  let mockService: MockService;
  let sessionContext: SessionContext;
  let toasterMessageService: ToasterMessageService;
  routerStub = {
    navigate: jasmine.createSpy('navigate').and.callThrough()
  };

  beforeEach(async(() => {
    messageService = new MessageService;
    sessionContext = new SessionContext();
    mockService = new MockService();
    toasterMessageService = new ToasterMessageService(sessionContext, messageService);

    TestBed.configureTestingModule({
      imports: [
        FormsModule
      ],
      declarations: [CancelGridMeasureComponent],

      providers: [
        SessionContext,
        { provide: ToasterMessageService, useValue: toasterMessageService },
        { provide: Router, useValue: routerStub },
        { provide: GridMeasureService, useValue: mockService }

      ]
    })
      .compileComponents();

    sessionContext.setGridMeasureDetail(GRIDMEASURE[0]);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CancelGridMeasureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should cancel the grid measure if there is a reason is and the submit button clicked', async(() => {
    component.cancelReasonText = 'im a reason to cancel the grid measure';
    fixture.detectChanges();
    spyOn(component, 'doCancel').and.callThrough();
    const button = fixture.debugElement.nativeElement.querySelector('button#submitCancelBtn');
    button.click();
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(component.doCancel).toHaveBeenCalled();
      expect(component.gridMeasureDetail.statusId).toBe(Globals.STATUS.CANCELED);
    });

  }));

  it('should not cancel the grid measure if there is not a reason is and the submit button clicked', async(() => {
    component.cancelReasonText = null;
    fixture.detectChanges();
    spyOn(component, 'doCancel').and.callThrough();
    const button = fixture.debugElement.nativeElement.querySelector('button#submitCancelBtn');
    button.click();
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(component.doCancel).toHaveBeenCalled();
      expect(component.gridMeasureDetail.statusId).not.toBe(Globals.STATUS.CANCELED);
    });

  }));

  it('should create a GridMeasure after click on applybutton', async(() => {
    spyOn(mockService, 'storeGridMeasure').and.callThrough();
    mockService.content = JSON.parse(JSON.stringify(gridmeasures[2]));

    component.gridMeasureDetail = JSON.parse(JSON.stringify(gridmeasures[0]));
    component.storageInProgress = true;

    fixture.detectChanges();

    (component as any).updateGridMeasureStatus(Globals.STATUS.CANCELED);
    expect(mockService.storeGridMeasure).not.toHaveBeenCalled();

    component.storageInProgress = false;


    fixture.detectChanges();

    (component as any).updateGridMeasureStatus(Globals.STATUS.CANCELED);

    expect(mockService.storeGridMeasure).toHaveBeenCalled();
  }));

  it('should handle a service error correctly after approve button', fakeAsync(() => {
    spyOn(console, 'log').and.callThrough();
    mockService.error = 'Error';

    component.gridMeasureDetail = JSON.parse(JSON.stringify(gridmeasures[0]));
    fixture.detectChanges();
    tick();
    (component as any).updateGridMeasureStatus(Globals.STATUS.CANCELED);

    tick();

    expect(console.log).toHaveBeenCalled();
  }));

});
