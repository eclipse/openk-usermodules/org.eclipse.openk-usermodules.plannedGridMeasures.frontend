/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import {
  Component, OnInit, Input, OnChanges, SimpleChanges, ViewChild, AfterViewChecked,
  AfterViewInit, ElementRef, OnDestroy
} from '@angular/core';
import { GridMeasure } from '../../model/grid-measure';
import { EmailDistributionEntry } from '../../model/email-distribution-entry';
import * as X2JS from '../../../assets/js/xml2json.min.js';
import { FormGroup } from '@angular/forms';
import { SessionContext } from './../../common/session-context';
import { Subscription } from 'rxjs/Subscription';
import { BaseDataService } from '../../services/base-data.service';
import { ToasterMessageService } from '../../services/toaster-message.service';

@Component({
  selector: 'app-email-distribution-entry',
  templateUrl: './email-distribution-entry.component.html',
  styleUrls: ['./email-distribution-entry.component.css']
})
export class EmailDistributionEntryComponent implements OnInit, OnChanges, OnDestroy, AfterViewChecked, AfterViewInit {
  @Input() isReadOnlyForm: boolean;
  @Input() isCollapsible = true;
  @Input() gridMeasureDetail: GridMeasure;

  form: HTMLFormElement;
  readOnlyForm: boolean;
  emailDistributionEntryFormValid: boolean;
  isStatusCollapsed = true;
  storageInProgress: boolean;
  emailDistributionEntry: EmailDistributionEntry = new EmailDistributionEntry();
  inactiveFields: Array<string> = [];
  subscription: Subscription;
  emailsFromGridMeasure: string[];

  @ViewChild('emailDistributionEntryFormContainer') emailDistributionEntryFormContainer: ElementRef;
  @ViewChild('emailDistributionEntryForm') emailDistributionEntryForm: FormGroup;
  constructor(
    public sessionContext: SessionContext,
    private baseDataService: BaseDataService,
    private toasterMessageService: ToasterMessageService) { }

  ngOnInit() {
    this.inactiveFields = this.sessionContext.getInactiveFields();
    this.getEmailsFromGridMeasures();
  }

  ngAfterViewInit() {
    this.initInactiveFields();
  }

  ngAfterViewChecked() {
    if (this.emailDistributionEntryForm) {
      this.emailDistributionEntry._isValide = this.emailDistributionEntryForm.valid;
    }
  }

  ngOnChanges(changes: SimpleChanges): void {

    if (changes['isReadOnlyForm']) {
      this.readOnlyForm = changes['isReadOnlyForm'].currentValue;
      this.initInactiveFields();
    }

  }

  private getEmailsFromGridMeasures() {
    this.baseDataService.getEmailAddressesFromGridmeasures()
      .subscribe(emails => this.emailsFromGridMeasure = emails, error => {
        console.log(error);
      });
  }

  public initInactiveFields() {
    const el: HTMLElement = this.emailDistributionEntryFormContainer.nativeElement as HTMLElement;
    const fields = el.querySelectorAll('*[id]');
    for (let index = 0; index < fields.length; index++) {
      const field = fields[index];
      if (this.readOnlyForm || this.isFieldInactive(field['id'])) {
        field.setAttribute('disabled', 'disabled');
      } else {
        field.removeAttribute('disabled');
      }
    }
  }

  private isFieldInactive(fieldName: string): boolean {
    return this.inactiveFields.filter(field => field === fieldName).length > 0;
  }

  ngOnDestroy() {
  }


  public processAddEmailDistributionEntry() {

    if (!this.isEmailDistributionEntryEmpty()) {
      const emailDistributionEntryDeepCopy = JSON.parse(JSON.stringify(this.emailDistributionEntry));
      emailDistributionEntryDeepCopy.id = -1;
      emailDistributionEntryDeepCopy.delete = false;

      if (!this.isMailAddressInTheList(emailDistributionEntryDeepCopy)) {
        this.storageInProgress = true;
        if (!this.gridMeasureDetail.listEmailDistribution || this.gridMeasureDetail.listEmailDistribution.length === 0) {
          this.gridMeasureDetail.listEmailDistribution = new Array<EmailDistributionEntry>();

        }

        /* A simple push on listEmailDistribution (this.gridMeasureDetail.listEmailDistribution.push(emailDistributionEntryDeepCopy))
        doesn't trigger Angular to refresh the view-model. This is why you have to use the following way
        which creates a "new" array (copy of the old) and appends it. */
        this.gridMeasureDetail.listEmailDistribution = [...this.gridMeasureDetail.listEmailDistribution, emailDistributionEntryDeepCopy];
        this.storageInProgress = false;
      } else {
        this.toasterMessageService.showWarn('Die Email-Adresse ist schon in der Liste vorhanden!');
      }

    } else {
      this.toasterMessageService.showWarn('Bitte valide Email-Adresse eintragen!');
    }

  }

  private isMailAddressInTheList(email: EmailDistributionEntry): boolean {
    let is: boolean;
    this.gridMeasureDetail.listEmailDistribution.forEach(emailInList => {
      if (emailInList.emailAddress === email.emailAddress) {
        is = true;
        return;
      }
    });
    return is;

  }

  onEmailDistributionEntryFormValidation(valid: boolean) {
    this.emailDistributionEntry._isValide = valid;
  }

  isEmailDistributionEntryEmpty() {
    if (!this.emailDistributionEntry.emailAddress) {
      return true;
    } else {
      return false;
    }
  }

  convertXmlToJsonObj(xml: string) {
    return new X2JS().xml_str2json(xml);
  }

}

