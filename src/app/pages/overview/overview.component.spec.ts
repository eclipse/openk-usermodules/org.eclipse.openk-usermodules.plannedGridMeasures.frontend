/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/


import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { MainNavigationComponent } from '../../common-components/main-navigation/main-navigation.component';
import { MockComponent } from '../../testing/mock.component';
import { OverviewComponent } from './overview.component';
import { Router } from '@angular/router';
import { SessionContext } from '../../common/session-context';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { VersionInfoComponent } from '../../common-components/version-info/version-info.component';
import { USERS } from '../../test-data/users';

class FakeRouter {
  navigate(commands: any[]) {
    return commands[0];
  }
}

@NgModule({
  declarations: [],
  entryComponents: [
    VersionInfoComponent
  ]
})
class TestModule { }

describe('OverviewComponent', () => {
  const sessionContext: SessionContext = new SessionContext();
  let component: OverviewComponent;
  let fixture: ComponentFixture<OverviewComponent>;
  let routerStub: FakeRouter;

  routerStub = {
    navigate: jasmine.createSpy('navigate').and.callThrough()
  };

  beforeEach(async(() => {

    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        BrowserAnimationsModule
        // ,
        // NoopAnimationsModule
      ],
      declarations: [
        OverviewComponent,
        MainNavigationComponent,
        MockComponent({ selector: 'app-custom-calendar', inputs: ['view', 'viewDate', 'viewDateChange', 'viewDate'] }),
        MockComponent({ selector: 'app-grid-measures', inputs: ['gridId', 'withEditButtons'] }),
        MockComponent({ selector: 'app-version-info' })
      ],
      providers: [
        { provide: Router, useValue: routerStub },
        { provide: SessionContext, useValue: sessionContext }
      ],
    }).compileComponents();

  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OverviewComponent);
    component = fixture.componentInstance;
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should navigate to grid-measure-detail on button-click', async(() => {
    sessionContext.setCurrUser(USERS[1]);

    fixture.whenStable().then(() => {
      fixture.detectChanges();
      const button = fixture.debugElement.nativeElement.querySelector('div#goToGridMeasureDetailBtn');
      button.click();
      fixture.detectChanges();
      expect(routerStub.navigate).toHaveBeenCalledWith(['/gridMeasureDetail']);
    });

  }));

});
