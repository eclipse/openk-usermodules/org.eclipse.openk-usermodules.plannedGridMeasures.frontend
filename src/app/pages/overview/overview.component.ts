/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { Globals } from './../../common/globals';
import { Component, OnInit } from '@angular/core';
import { SessionContext } from '../../common/session-context';
import { User } from '../../model/user';
import { Router } from '@angular/router';

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.css']
})

export class OverviewComponent implements OnInit {

  user: User;
  view: any = 'list';
  createGridMeasureAllowed = false;

  constructor(public router: Router,
    public sessionContext: SessionContext) { }

  ngOnInit() {
    this.user = this.sessionContext.getCurrUser();
    this.createGridMeasureAllowed = this.user.roles.includes(Globals.OAUTH2CONF_SUPERUSER_ROLE) ||
      this.user.roles.includes(Globals.OAUTH2CONF_MEASUREAPPLICANT_ROLE);
    this.sessionContext.setCancelStage(false);
  }

  goToGridMeasureDetail() {
    this.router.navigate(['/gridMeasureDetail']);
  }
}
