/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { GridMeasureDetailHeaderComponent } from './grid-measure-detail-header.component';
import { SessionContext } from '../../common/session-context';
import { MockComponent } from '../../testing/mock.component';
import { FormsModule } from '@angular/forms';
import { BaseDataService } from '../../services/base-data.service';
import { TERRITORY } from '../../test-data/territories';
import { SimpleChange } from '@angular/core';
import { AbstractMockObservableService } from '../../testing/abstract-mock-observable.service';
import { USERS } from '../../test-data/users';
import { GridMeasureService } from '../../services/grid-measure.service';

describe('GridMeasureDetailHeaderComponent', () => {
  let component: GridMeasureDetailHeaderComponent;
  let fixture: ComponentFixture<GridMeasureDetailHeaderComponent>;
  let sessionContext: SessionContext;
  let mockBaseDataService;
  let mockGridmeasureService;
  class MockBaseDataService extends AbstractMockObservableService {
    getBranchLevels(id: number) {
      return this;
    }
  }

  class MockGridmeasureService extends AbstractMockObservableService {
    getUserDepartmentsCreated() {
      return this;
    }
  }

  beforeEach(async(() => {
    sessionContext = new SessionContext();
    mockBaseDataService = new MockBaseDataService();
    mockGridmeasureService = new MockGridmeasureService();
    TestBed.configureTestingModule({
      imports: [FormsModule],
      declarations: [
        GridMeasureDetailHeaderComponent,
        MockComponent({ selector: 'app-loading-spinner', inputs: [] })],
      providers: [
        { provide: BaseDataService, useValue: mockBaseDataService },
        { provide: GridMeasureService, useValue: mockGridmeasureService },
        SessionContext
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GridMeasureDetailHeaderComponent);
    sessionContext.setTerritories(TERRITORY);
    sessionContext.setCurrUser(USERS[0]);
    sessionContext.setAllUsers(USERS);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should enable save button after filling required field', () => {
    spyOn(component, 'onGridMeasureTitleChange').and.callThrough();
    const newVal = 'TitleTest';
    component.gridMeasureDetail.title = newVal;
    component.onGridMeasureTitleChange(newVal);
    fixture.detectChanges();

    fixture.whenRenderingDone().then(() => {
      fixture.detectChanges();
      expect(component.isValidForSave).toBeTruthy('enabled for saving');
      expect(component.onGridMeasureTitleChange).toHaveBeenCalled();
    });

  });



  it('should disable save button after cleaning required field', () => {
    spyOn(component, 'onGridMeasureTitleChange').and.callThrough();
    const newVal = '';
    component.gridMeasureDetail.title = newVal;
    component.onGridMeasureTitleChange(newVal);
    fixture.detectChanges();

    fixture.whenRenderingDone().then(() => {
      fixture.detectChanges();
      expect((component as any).validForSave).toBeFalsy();
      expect(component.onGridMeasureTitleChange).toHaveBeenCalled();
    });

  });

  it('should set single grid measure title on blur if empty', () => {
    spyOn(component, 'onGridMeasureTitleBlur').and.callThrough();

    const newVal = '';
    component.gridMeasureDetail.title = newVal;

    component.onGridMeasureTitleBlur(newVal);
    fixture.detectChanges();

    fixture.whenRenderingDone().then(() => {
      fixture.detectChanges();
      expect(component.onGridMeasureTitleBlur).toHaveBeenCalled();
    });

  });

  it('should set form to readonly', () => {
    component.readOnlyForm = false;
    fixture.detectChanges();
    component.ngOnChanges({
      isReadOnlyForm: new SimpleChange(component.readOnlyForm, true, true)
    });
    fixture.detectChanges();
    expect(component.isValidForSave).toBeTruthy();
  });

});
