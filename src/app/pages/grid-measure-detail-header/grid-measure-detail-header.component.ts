/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import {
  Component, OnInit, Input, AfterViewChecked, ViewChild, Output,
  EventEmitter, OnChanges, SimpleChanges, ElementRef, AfterViewInit
} from '@angular/core';
import { GridMeasure } from '../../model/grid-measure';
import { FormGroup } from '@angular/forms';
import { Status } from '../../model/status';
import { SessionContext } from '../../common/session-context';
import { Branch } from '../../model/branch';
import { BranchLevel } from '../../model/branch-level';
import { BaseDataService } from '../../services/base-data.service';
import { GridMeasureService } from '../../services/grid-measure.service';

@Component({
  selector: 'app-grid-measure-detail-header',
  templateUrl: './grid-measure-detail-header.component.html',
  styleUrls: ['./grid-measure-detail-header.component.css', '../grid-measure-detail/grid-measure-detail.component.css']
})
export class GridMeasureDetailHeaderComponent implements OnInit, AfterViewChecked, OnChanges, AfterViewInit {

  @Input() showSpinnerGrid: boolean;
  @Input() gridMeasureDetail: GridMeasure = new GridMeasure();
  @Input() isReadOnlyForm: boolean;
  @Output() isValidForSave: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() titleChange: EventEmitter<string> = new EventEmitter<string>();
  @ViewChild('gridMeasureDetailHeaderForm') gridMeasureDetailHeaderForm: FormGroup;
  @ViewChild('gridMeasureDetailHeaderContainer') gridMeasureDetailHeaderContainer: ElementRef;

  statusList: Status[];
  inactiveFields: Array<string> = [];
  readOnlyForm: boolean;
  brancheList: Branch[];
  branchLevelList: Array<BranchLevel> = [];
  areaOfSwitchingList: string[];
  departmentList: string[];
  costCenters: any;
  isBranchLevelActive: boolean;
  constructor(public sessionContext: SessionContext,
    private baseDataService: BaseDataService,
    private gridMeasuresService: GridMeasureService) { }

  ngOnInit() {
    this.inactiveFields = this.sessionContext.getInactiveFields();
    this.getBranchLevelsByBranch(this.gridMeasureDetail.branchId);
    this.statusList = this.sessionContext.getStatuses();
    this.brancheList = this.sessionContext.getBranches();
    this.areaOfSwitchingList = this.sessionContext.getTerritories().map(ter => ter.name);
    this.gridMeasuresService.getUserDepartmentsCreated()
      .subscribe(departments => this.departmentList = departments,
        error => {
          console.log(error);
        });
    this.costCenters = this.sessionContext.getCostCenters();

  }

  ngAfterViewInit() {
    this.initInactiveFields();
  }

  ngAfterViewChecked() {

    if (this.gridMeasureDetailHeaderForm) {
      this.gridMeasureDetail._isHeaderValide = this.gridMeasureDetailHeaderForm.valid;
    }

  }

  onGridMeasureHeaderFormValidation(valid: boolean) {
    this.gridMeasureDetail._isHeaderValide = valid;
  }

  onGridMeasureTitleChange(value) {
    if (value) {
      this.isValidForSave.emit(true);
    } else {
      this.isValidForSave.emit(false);
    }
  }

  onGridMeasureTitleBlur(value) {
    this.titleChange.emit(value);
  }

  ngOnChanges(changes: SimpleChanges): void {

    if (changes['isReadOnlyForm']) {
      this.readOnlyForm = changes['isReadOnlyForm'].currentValue;
      this.initInactiveFields();
    }

  }

  public initInactiveFields() {
    const el: HTMLElement = this.gridMeasureDetailHeaderContainer.nativeElement as HTMLElement;
    const fields = el.querySelectorAll('*[id]');
    for (let index = 0; index < fields.length; index++) {
      const field = fields[index];
      if (this.readOnlyForm || this.isFieldInactive(field['id'])) {
        field.setAttribute('disabled', 'disabled');
      } else {
        field.removeAttribute('disabled');
      }
    }
  }

  private isFieldInactive(fieldName: string): boolean {
    return this.inactiveFields.filter(field => field === fieldName).length > 0;
  }

  public getBranchLevelsByBranch(branchId: number) {
    if (branchId && !this.isFieldInactive('level')) {
      this.baseDataService.getBranchLevels(branchId)
        .subscribe(branchLevels => {
          this.branchLevelList = branchLevels,
            this.isBranchLevelActive = (!branchId || branchLevels.length === 0) ? false : true;
        },
          error => {
            console.log(error);
          });
    } else {
      this.gridMeasureDetail.branchId = null;
      this.gridMeasureDetail.branchLevelId = null;
      this.branchLevelList = null;
      this.isBranchLevelActive = false;
    }

  }
}
