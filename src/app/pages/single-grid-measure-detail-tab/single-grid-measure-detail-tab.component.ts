/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import {
  Component, OnInit, Input, OnChanges, SimpleChanges, ViewChild, AfterViewChecked, ElementRef, Output,
  EventEmitter, OnDestroy, AfterViewInit
} from '@angular/core';
import { GridMeasure } from '../../model/grid-measure';
import { SingleGridMeasure } from '../../model/single-grid-measure';
import * as X2JS from '../../../assets/js/xml2json.min.js';
import { ErrorType } from '../../common/enums';
import { CimCacheService } from '../../services/cim-cache.service';
import { PowerSystemResource } from './../../model/power-system-resource';
import { FormGroup } from '@angular/forms';
import { Util } from '../../common/util';
import { ToasterButtonEventEn } from './../../common/enums';
import { Globals } from './../../common/globals';
import { Subscription } from 'rxjs/Subscription';
import { CloneGridMeasureHelper } from '../../custom_modules/helpers/clone-grid-measure-helper';
import { RoleAccessHelperService } from '../../services/jobs/role-access-helper.service';
import { SessionContext } from '../../common/session-context';
import { ToasterMessageService } from '../../services/toaster-message.service';
import { GridMeasureService } from '../../services/grid-measure.service';

@Component({
  selector: 'app-single-grid-measure-detail-tab',
  templateUrl: './single-grid-measure-detail-tab.component.html',
  styleUrls: ['./single-grid-measure-detail-tab.component.css']
})
export class SingleGridMeasureDetailTabComponent implements OnInit, OnChanges, OnDestroy, AfterViewChecked, AfterViewInit {
  @Input() isReadOnlyForm: boolean;
  @Input() gridMeasureDetail: GridMeasure = new GridMeasure();
  @Input() dateTimePattern: string;
  @Input() dateFormatLocale: string;
  @Input() singleGridMeasure: SingleGridMeasure = new SingleGridMeasure();
  @Output() singleGridMeasureChanged = new EventEmitter<SingleGridMeasure>();

  form: HTMLFormElement;
  readOnlyForm: boolean;
  singleGridMeasureFormValid: boolean;
  storageInProgress: boolean;

  currentPowerSystemResourceDB = new PowerSystemResource();
  tmpPowerSystemResource = new PowerSystemResource();
  currSelectedResourceGroup: string;
  singleGMAffectedResourcesGroupList = [''];
  singleGMAffectedResourcesList: Array<PowerSystemResource> = [];
  calcDatepickerDropOrientation = Util.calcDatepickerDropOrientation;
  subscription: Subscription;
  inactiveFields: Array<string> = [];
  responsibleOnSiteNameList: string[];
  networkControlsList: string[];
  responsibleOnSiteDepartmentList: string[];

  @ViewChild('singleGridMeasureDetailFormCotainer') singleGridMeasureDetailFormContainer: ElementRef;
  @ViewChild('singleGridForm') singleGridForm: FormGroup;
  constructor(private cimCacheService: CimCacheService,
    public roleAccessHelper: RoleAccessHelperService,
    public sessionContext: SessionContext,
    private toasterMessageService: ToasterMessageService,
    private gridMeasuresService: GridMeasureService) { }

  ngOnInit() {
    this.gridMeasuresService.getNetworkControlsFromSingleGridmeasures()
      .subscribe(net => this.networkControlsList = net,
        error => {
          console.log(error);
        });
    this.gridMeasuresService.getResponsiblesOnSiteFromSingleGridmeasures()
      .subscribe(res => this.responsibleOnSiteNameList = res,
        error => {
          console.log(error);
        });
    this.gridMeasuresService.getUserDepartmentsResponsibleOnSite()
      .subscribe(departments => this.responsibleOnSiteDepartmentList = departments,
        error => {
          console.log(error);
        });
    this.inactiveFields = this.sessionContext.getInactiveFields();
    this.getRessourceTypes();
    if (this.singleGridMeasure.powerSystemResource) {
      this.currentPowerSystemResourceDB = JSON.parse(JSON.stringify(this.singleGridMeasure.powerSystemResource));
    }
    this.subscription = this.toasterMessageService.toasterEmitter$.subscribe(
      toasterButtonEventEn => this.processToasterButtonEvent(toasterButtonEventEn));

  }

  private processToasterButtonEvent(toasterButtonEventEn: any) {
    if (toasterButtonEventEn === ToasterButtonEventEn.deleteSingleGridMeasure) {
      this.processDeleteSingleGridMeasure();
    }
  }

  ngAfterViewInit() {
    this.initInactiveFields();
  }

  ngAfterViewChecked() {
    if (this.singleGridForm) {
      this.singleGridMeasure._isValide = this.singleGridForm.valid;
    }
  }

  ngOnChanges(changes: SimpleChanges): void {

    if (changes['isReadOnlyForm']) {
      this.readOnlyForm = changes['isReadOnlyForm'].currentValue;
      this.initInactiveFields();
    }
    if (changes['gridMeasureDetail'] && changes['gridMeasureDetail'].currentValue) {
      if (!this.gridMeasureDetail.listSingleGridmeasures || this.gridMeasureDetail.listSingleGridmeasures.length === 0) {
        this.gridMeasureDetail.listSingleGridmeasures = new Array();
        const singleGM = new SingleGridMeasure();
        singleGM.sortorder = 1;
        this.gridMeasureDetail.listSingleGridmeasures.push(singleGM);
      }

      this.singleGridMeasure = this.gridMeasureDetail.listSingleGridmeasures[0];
    }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  public selectedDate(value: any, datepicker?: any) {
    this.gridMeasureDetail[datepicker] = value.start._d;
  }

  public onDeleteBtnClick() {
    this.toasterMessageService.showSingleGMDeleteConfirm('Wollen Sie die Einzelmaßnahme wirklich löschen?');
  }

  public duplicateSingleGM() {
    const newReversedSingleGridMeasure = CloneGridMeasureHelper.cloneSingleGridMeasure(
      this.gridMeasureDetail, this.singleGridMeasure);

    this.gridMeasureDetail.listSingleGridmeasures.push(newReversedSingleGridMeasure);
    this.singleGridMeasure = newReversedSingleGridMeasure;
    this.singleGridMeasureChanged.emit(this.singleGridMeasure);
    Util.showSingleGridmeasureTab(newReversedSingleGridMeasure.sortorder);
  }

  private processDeleteSingleGridMeasure() {
    if (this.singleGridMeasure.id) {
      this.singleGridMeasure.delete = true;
    } else {
      this.gridMeasureDetail.listSingleGridmeasures = this.gridMeasureDetail.listSingleGridmeasures.filter(singleGridMeasure => {
        return singleGridMeasure.sortorder !== this.singleGridMeasure.sortorder;
      });
    }

    this.toasterMessageService.showSuccess('Einzelmaßnahme wurde gelöscht.');

    const previousSingleGridMeasure: SingleGridMeasure = this.getPreviousSingleGridmeasure();

    if (previousSingleGridMeasure) {
      this.singleGridMeasure = previousSingleGridMeasure;
      this.singleGridMeasureChanged.emit(this.singleGridMeasure);
      Util.showSingleGridmeasureTab(previousSingleGridMeasure.sortorder);
    } else {
      Util.showGridmeasureTab();
    }
  }

  onSingleGridFormValidation(valid: boolean) {
    this.singleGridMeasure._isValide = valid;
  }

  private getRessourceTypes() {
    this.cimCacheService.getRessourceTypes().subscribe(res => {
      this.processRessourceTypesResponse(res);
    },
      error => {
        this.toasterMessageService.showError(ErrorType.retrieve, 'CimCache');
        console.log(error);
      });
  }

  onChangeResourceGroup(val) {
    if (val) {
      this.currSelectedResourceGroup = val;
      this.singleGMAffectedResourcesList = [];
      this.getRessourceTypesWithType(val);
    } else {
      this.singleGMAffectedResourcesList = [];
    }

  }

  onChangeResourceType(val) {
    this.singleGridMeasure.powerSystemResource.cimName = this.currSelectedResourceGroup + ' - ' + val.cimName;
  }

  private getRessourceTypesWithType(value: string) {
    this.cimCacheService.getRessourcesWithType(value).subscribe(res => {
      this.processRessourceWithTypeResponse(res);
    },
      error => {
        // this.messageService.emitError('CimCache', ErrorType.retrieve);
        this.toasterMessageService.showError(ErrorType.retrieve, 'CimCache');
        console.log(error);
      });
  }
  public initInactiveFields() {
    const el: HTMLElement = this.singleGridMeasureDetailFormContainer.nativeElement as HTMLElement;
    const fields = el.querySelectorAll('*[id]');
    for (let index = 0; index < fields.length; index++) {
      const field = fields[index];
      if (this.readOnlyForm || this.isFieldInactive(field['id'])) {
        field.setAttribute('disabled', 'disabled');
      } else {
        if (!(field.id === 'deleteSingleGridMeasureBtn' && this.isLastSingleGridmeasure())) {
          field.removeAttribute('disabled');
        }
      }
    }
  }

  private isFieldInactive(fieldName: string): boolean {
    return this.inactiveFields.filter(field => field === fieldName).length > 0;
  }
  convertXmlToJsonObj(xml: string) {
    return new X2JS().xml_str2json(xml);
  }

  processRessourceWithTypeResponse(res: string): void {
    const jsonObj = this.convertXmlToJsonObj(res);
    const powerSystemResources = jsonObj.ResponseMessage.Payload.PowerSystemResources;

    /* tslint:disable */
    for (const prop in powerSystemResources) {
      const anonymousPowerSystemResources = powerSystemResources[prop];

      if (Array.isArray(anonymousPowerSystemResources)) {
        anonymousPowerSystemResources.forEach(element => {
          const powerSystemResource = new PowerSystemResource();
          powerSystemResource.cimId = element.mRID;
          powerSystemResource.cimName = element.name;
          powerSystemResource.cimDescription = element.description;
          this.singleGMAffectedResourcesList.push(powerSystemResource);
        });
      } else {
        const powerSystemResource = new PowerSystemResource();
        powerSystemResource.cimId = anonymousPowerSystemResources.mRID;
        powerSystemResource.cimName = anonymousPowerSystemResources.name;
        powerSystemResource.cimDescription = anonymousPowerSystemResources.description;
        this.singleGMAffectedResourcesList.push(powerSystemResource);
      }

    }
    if (this.singleGMAffectedResourcesList.length === 0) {
      // keine Daten "Objekt"
      const powerSystemResource = new PowerSystemResource();
      powerSystemResource.cimId = '-1';
      powerSystemResource.cimName = 'keine Daten';
      this.singleGMAffectedResourcesList.push(powerSystemResource);
    }
    this.tmpPowerSystemResource = this.singleGMAffectedResourcesList[0];
    this.singleGridMeasure.powerSystemResource = JSON.parse(JSON.stringify(this.tmpPowerSystemResource));

    this.singleGridMeasure.powerSystemResource.cimName = this.currSelectedResourceGroup + ' - ' + this.tmpPowerSystemResource.cimName;

    /* tslint:enable */

  }

  processRessourceTypesResponse(res: string): void {
    const jsonObj = this.convertXmlToJsonObj(res);
    const PSRTypeList = jsonObj.ResponseMessage.Payload.PowerSystemResourceTypes.PSRType;
    for (let index = 0; index < PSRTypeList.length; index++) {
      this.singleGMAffectedResourcesGroupList.push(PSRTypeList[index].name);
    }
  }

  isSingleGridmeasureLimitReached(): boolean {
    if (this.gridMeasureDetail && this.gridMeasureDetail.listSingleGridmeasures) {
      return this.gridMeasureDetail.listSingleGridmeasures.filter(singleGridMeasure =>
        !singleGridMeasure.delete).length >= Globals.MAX_NUMBER_OF_TABS;
    } else {
      return false;
    }
  }

  isLastSingleGridmeasure(): boolean {
    if (this.gridMeasureDetail && this.gridMeasureDetail.listSingleGridmeasures) {
      return this.gridMeasureDetail.listSingleGridmeasures.filter(singleGridMeasure => !singleGridMeasure.delete).length <= 1;
    } else {
      return true;
    }
  }


  getPreviousSingleGridmeasure(): SingleGridMeasure {
    const previousSingleGridmeasures: SingleGridMeasure[] = this.gridMeasureDetail.listSingleGridmeasures.filter(singleGridMeasure => {
      return !singleGridMeasure.delete && singleGridMeasure.sortorder < this.singleGridMeasure.sortorder;
    });

    return previousSingleGridmeasures
      && previousSingleGridmeasures.length > 0 ?
      previousSingleGridmeasures[previousSingleGridmeasures.length - 1] : null;
  }
}
