/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GridConfigModifierComponent } from './grid-config-modifier.component';
import { Router } from '@angular/router';
import { ModifyGridConfigService } from '../../services/modify-grid-config.service';
import { AbstractMockObservableService } from '../../testing/abstract-mock-observable.service';
import { SessionContext } from '../../common/session-context';
import { FormsModule } from '@angular/forms';
import { ToasterMessageService } from '../../services/toaster-message.service';

describe('GridConfigModifierComponent', () => {
  let component: GridConfigModifierComponent;
  let fixture: ComponentFixture<GridConfigModifierComponent>;
  let routerStub: FakeRouter;
  let mockService: MockService;
  let sessionContext: SessionContext;

  routerStub = {
    navigate: jasmine.createSpy('navigate').and.callThrough()
  };

  class FakeRouter {
    navigate(commands: any[]) {
      return commands[0];
    }
  }

  class MockService extends AbstractMockObservableService {
    setGridConfig() {
      return this;
    }
  }

  beforeEach(async(() => {
    sessionContext = new SessionContext();
    mockService = new MockService();

    TestBed.configureTestingModule({
      imports: [
        FormsModule
      ],
      declarations: [GridConfigModifierComponent],
      providers: [
        SessionContext,
        { provide: Router, useValue: routerStub },
        { provide: ModifyGridConfigService, useValue: mockService },
        { provide: ToasterMessageService }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GridConfigModifierComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should react on changeStatusBtn button click', async(() => {
    spyOn(component, 'requestGridConfigChange').and.callThrough();
    component.requestGridConfigChange(true);
    fixture.detectChanges();
    fixture.whenRenderingDone().then(() => {
      expect(routerStub.navigate).toHaveBeenCalledWith(['/overview']);
      expect(component.requestGridConfigChange).toHaveBeenCalled();
    });

  }));

  it('should react on backBtn button click and go to overview', async(() => {
    const button = fixture.debugElement.nativeElement.querySelector('div#backBtn');
    button.click();
    fixture.detectChanges();
    fixture.whenRenderingDone().then(() => {
      expect(routerStub.navigate).toHaveBeenCalledWith(['/overview']);
    });

  }));

});
