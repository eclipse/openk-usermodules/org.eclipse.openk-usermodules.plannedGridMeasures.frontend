/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { Component, OnInit } from '@angular/core';
import { GridConfig } from '../../model/grid-config';
import { ModifyGridConfigService } from '../../services/modify-grid-config.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-status-change',
  templateUrl: './grid-config-modifier.component.html',
  styleUrls: ['./grid-config-modifier.component.css']
})
export class GridConfigModifierComponent implements OnInit {

  modifyGridConfig = new GridConfig();

  constructor(
    public router: Router,
    private modifyGridConfigService: ModifyGridConfigService
  ) { }

  ngOnInit() {

  }

  requestGridConfigChange(isTest?: boolean) {
    this.modifyGridConfigService.setGridConfig(this.modifyGridConfig).subscribe(async (modGridConfService) => {
      this.modifyGridConfig = modGridConfService;
    });
    this.goToOverview();
    if (!isTest) {
      window.location.reload();
    }
  }

  goToOverview() {
    this.router.navigate(['/overview']);
  }
}
