/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { LoggedoutPageComponent } from './loggedout.component';

describe('LoggedoutPageComponent', () => {
  let component: LoggedoutPageComponent;
  let fixture: ComponentFixture<LoggedoutPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LoggedoutPageComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoggedoutPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
