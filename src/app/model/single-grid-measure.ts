/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { PowerSystemResource } from './power-system-resource';
import { Step } from './step';

export class SingleGridMeasure {
    id?: number;
    sortorder?: number;
    title?: string;
    switchingObject?: string;
    powerSystemResource?: PowerSystemResource;
    plannedStarttimeSinglemeasure?: string;
    plannedEndtimeSinglemeasure?: string;
    description?: string;
    gridmeasureId?: number;
    _isValide ?= true;
    delete?: boolean;
    listSteps?: Array<Step>;
    listStepsDeleted?: Array<Step>;
    responsibleOnSiteName?: string;
    responsibleOnSiteDepartment?: string;
    networkControl?: string;
}
