/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { SingleGridMeasure } from './single-grid-measure';
import { EmailDistributionEntry } from './email-distribution-entry';

export class GridMeasure {
    id?: number;
    descriptiveId?: string;
    title?: string;
    affectedResource?: string;
    remark?: string;
    createUser?: string;
    createUserDepartment?: string;
    createDate?: Date;
    modUser?: string;
    modUserDepartment?: string;
    modDate?: Date;
    statusId?: number;
    switchingObject?: string;
    costCenter?: string;
    approvalBy?: string;
    areaOfSwitching?: string;
    appointmentRepetition?: string;
    appointmentStartdate?: string;
    appointmentNumberOf?: number;
    plannedStarttimeFirstSinglemeasure?: string;
    endtimeGridmeasure?: string;
    timeOfReallocation?: string;
    description?: string;
    branchId?: number;
    branchLevelId?: number;
    listSingleGridmeasures?: Array<SingleGridMeasure>;
    emailAddresses?: string;
    listEmailDistribution?: Array<EmailDistributionEntry>;
    listEmailDistributionDeleted?: Array<EmailDistributionEntry>;
    selected?: boolean;
    _isValide ?= true;
    _isHeaderValide ?= true;
}
