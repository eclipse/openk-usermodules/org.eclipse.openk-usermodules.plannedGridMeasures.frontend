/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

export class RoleAccess {
    editRoles: Array<EditRoleItems>;
    controls: Array<Controls>;
    stornoSection: StornoSection;
    duplicateSection: DuplicateSection;
}
export class EditRoleItems {
    name: string;
    gridMeasureStatusIds: Array<number>;
}
export class Controls {
    gridMeasureStatusId: number;
    activeButtons: Array<string>;
    inactiveFields: Array<string>;
}
export class StornoSection {
    stornoRoles: Array<string>;
}
export class DuplicateSection {
    duplicateRoles: Array<string>;
}
