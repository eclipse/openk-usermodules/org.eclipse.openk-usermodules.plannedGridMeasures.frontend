/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { Status } from '../model/status';

export const STATUSES: Status[] = [
    {
        id: 1,
        name: 'offen',
        colorCode: '#990000',
    },
    {
        id: 2,
        name: 'in Bearbeitung',
        colorCode: '#990000',
    },
    {
        id: 3,
        name: 'beendet',
        colorCode: '#990000',
    }
];
