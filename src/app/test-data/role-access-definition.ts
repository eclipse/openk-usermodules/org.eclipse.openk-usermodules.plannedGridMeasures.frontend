/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/


import { RoleAccess } from '../model/role-access';

export const ROLE_ACCESS: RoleAccess = {
    editRoles: [{
        name: 'planned-policies-measureplanner',
        gridMeasureStatusIds: [
            0,
            1
        ]
    }],
    controls: [
        {
            'gridMeasureStatusId': 0,
            'activeButtons': [
                'apply',
                'save'
            ],
            'inactiveFields': [
                'titeldermassnahme'
            ]
        },
        {
            'gridMeasureStatusId': 1,
            'activeButtons': [
                'cancel',
                'forapproval',
                'save'
            ],
            'inactiveFields': [
                'titeldermassnahme'
            ]
        },
        {
            'gridMeasureStatusId': 2,
            'activeButtons': [],
            'inactiveFields': [
                'titeldermassnahme'
            ]
        },
        {
            'gridMeasureStatusId': 3,
            'activeButtons': [
                'save',
                'cancel',
                'reject',
                'approve'
            ],
            'inactiveFields': [
                'titeldermassnahme'
            ]
        },
        {
            'gridMeasureStatusId': 4,
            'activeButtons': [
                'save',
                'request'
            ],
            'inactiveFields': [
                'titeldermassnahme'
            ]
        },
        {
            'gridMeasureStatusId': 5,
            'activeButtons': [
                'save',
                'release'
            ],
            'inactiveFields': [
                'titeldermassnahme'
            ]
        },
        {
            'gridMeasureStatusId': 6,
            'activeButtons': [
                'save',
                'activate'
            ],
            'inactiveFields': [
                'titeldermassnahme'
            ]
        },
        {
            'gridMeasureStatusId': 7,
            'activeButtons': [
                'save',
                'inwork'
            ],
            'inactiveFields': [
                'titeldermassnahme'
            ]
        },
        {
            'gridMeasureStatusId': 8,
            'activeButtons': [
                'save',
                'workfinish'
            ],
            'inactiveFields': [
                'titeldermassnahme'
            ]
        },
        {
            'gridMeasureStatusId': 9,
            'activeButtons': [
                'save',
                'finish'
            ],
            'inactiveFields': [
                'titeldermassnahme'
            ]
        },
        {
            'gridMeasureStatusId': 10,
            'activeButtons': [
                'save',
                'close'
            ],
            'inactiveFields': [
                'titeldermassnahme'
            ]
        },
        {
            'gridMeasureStatusId': 11,
            'activeButtons': [
                'save'
            ],
            'inactiveFields': [
                'titeldermassnahme'
            ]
        },
        {
            'gridMeasureStatusId': 11,
            'activeButtons': [
                'save'
            ],
            'inactiveFields': [
                'titeldermassnahme'
            ]
        },
        {
            'gridMeasureStatusId': 20,
            'activeButtons': [
                'quit'
            ],
            'inactiveFields': [
                'titeldermassnahme'
            ]
        }
    ],
    stornoSection:
    {
        'stornoRoles': [
            'planned-policies-measureapplicant',
            'planned-policies-measureplanner',
            'planned-policies-measureapprover',
            'planned-policies-requester',
            'planned-policies-clearance'
        ]
    },
    duplicateSection:
    {
        'duplicateRoles': [
            'planned-policies-measureapplicant'
        ]
    }
};
