/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { GridMeasure } from '../model/grid-measure';

export const GRIDMEASURE: GridMeasure[] = [

    {
        id: 1111,
        descriptiveId: 'descriptiveId1',
        branchId: 1,
        plannedStarttimeFirstSinglemeasure: '2017-01-15T11:11:00z',
        title: 'T1',
        affectedResource: 'TESTRESOURCE1',
        remark: 'TESTREMARK1',
        createUser: 'Tim Fischer',
        createUserDepartment: 'Abteilung A',
        statusId: 1,
        costCenter: 'Kostenstelle',
        approvalBy: 'Freigabe durch',
        areaOfSwitching: 'Gebiet (Region) der Schaltung',
        appointmentRepetition: 'Terminwiederholung täglich',
        appointmentStartdate: '2017-01-15T11:11:00z',
        appointmentNumberOf: 3,
        timeOfReallocation: '2017-01-15T11:11:00z',
        modUserDepartment: 'Meisterabteilung',
        description: 'BeschreibungText',
        listSingleGridmeasures: [
            {
                id: 3, title: 'title 1', sortorder: 11, gridmeasureId: 1111, listSteps: [
                    { id: 1, sortorder: 1, switchingObject: 'Schalter 1', targetState: 'aus', singleGridmeasureId: 3, delete: false },
                    { id: 2, sortorder: 2, switchingObject: 'Schalter 2', targetState: 'aus', singleGridmeasureId: 3, delete: false },
                    { id: 3, sortorder: 3, switchingObject: 'Schalter 3', targetState: 'aus', singleGridmeasureId: 3, delete: false }
                ]
            },
            {
                id: 5, title: 'title 2', sortorder: 35, gridmeasureId: 1111, listSteps: [
                    { id: 4, sortorder: 1, switchingObject: 'Schalter 42', targetState: 'aus', singleGridmeasureId: 5, delete: false },
                    { id: 5, sortorder: 2, switchingObject: 'Schalter 43', targetState: 'aus', singleGridmeasureId: 5, delete: false },
                    { id: 6, sortorder: 3, switchingObject: 'Schalter 44', targetState: 'aus', singleGridmeasureId: 5, delete: false },
                    { id: 7, sortorder: 4, switchingObject: 'Schalter 45', targetState: 'aus', singleGridmeasureId: 5, delete: false }
                ]
            }
        ],
        emailAddresses: 'test@test.de;test2@test.de',
        listEmailDistribution: [
            { id: 1, emailAddress: 'test@test.de', _isValide: true, delete: false },
            { id: 2, emailAddress: 'test2@test.de', _isValide: true, delete: false }
        ]

    },
    {
        id: 2222,
        branchId: 2,
        descriptiveId: 'descriptiveId2',
        plannedStarttimeFirstSinglemeasure: '2017-01-15T11:11:00z',
        title: 'T2',
        affectedResource: 'TESTRESOURCE2',
        remark: 'TESTREMARK2',
        createUser: 'Max Maye',
        createUserDepartment: '',
        statusId: 2,
        listSingleGridmeasures: [{ id: 3, title: 'title' }, { id: 5, title: 'title' }],
        emailAddresses: 'test@test.de;test2@test.de',
        listEmailDistribution: [
            { id: 1, emailAddress: 'test@test.de', _isValide: true, delete: false },
            { id: 2, emailAddress: 'test2@test.de', _isValide: true, delete: false }
        ]
    },
    {
        id: 3333,
        descriptiveId: 'descriptiveId3',
        branchId: 3,
        plannedStarttimeFirstSinglemeasure: '2017-01-15T11:11:00z',
        title: 'Rohr warten',
        affectedResource: 'Rohr',
        remark: '',
        createUser: 'Tim Tester',
        createUserDepartment: 'Wasser',
        statusId: 3,
        listSingleGridmeasures: [{ id: 3, title: 'title' }, { id: 5, title: 'title' }],
        emailAddresses: 'test@test.de;test2@test.de',
        listEmailDistribution: [
            { id: 1, emailAddress: 'test@test.de', _isValide: true, delete: false },
            { id: 2, emailAddress: 'test2@test.de', _isValide: true, delete: false }
        ]
    },
    {
        id: 4444,
        descriptiveId: 'descriptiveId4',
        branchId: 3,
        title: 'Keine Zeit zum Warten',
        affectedResource: 'Zeit',
        remark: 'Teste die Zeiten',
        createUser: 'Tom Tester',
        createUserDepartment: 'Wasser',
        statusId: 1,
        costCenter: 'Kostenstelle',
        approvalBy: 'Freigabe durch',
        areaOfSwitching: 'Gebiet (Region) der Schaltung',
        appointmentRepetition: 'Terminwiederholung täglich',
        appointmentNumberOf: 3,
        modUserDepartment: 'Meisterabteilung',
        description: 'BeschreibungText',
        listSingleGridmeasures: [{ id: 3, title: 'title' }, { id: 5, title: 'title' }],
        emailAddresses: 'test@test.de;test2@test.de',
        listEmailDistribution: [
            { id: 1, emailAddress: 'test@test.de', _isValide: true, delete: false },
            { id: 2, emailAddress: 'test2@test.de', _isValide: true, delete: false }
        ]
    }

];
