/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { Branch } from '../model/branch';

export const BRANCHES: Branch[] = [
    { 'id': 1, 'name': 'S', 'description': 'Strom', 'colorCode': '' },
    { 'id': 2, 'name': 'G', 'description': 'Gas', 'colorCode': '' },
    { 'id': 4, 'name': 'W', 'description': 'Wasser', 'colorCode': '' },
    { 'id': 3, 'name': 'F', 'description': 'Fernwärme', 'colorCode': '' }
];
