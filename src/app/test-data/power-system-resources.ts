/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { PowerSystemResource } from '../model/power-system-resource';

export const POWERSYSTEMRESOURCES: PowerSystemResource[] = [
    {
        cimId: '"abddaf44-13e6-46a3-8f87-0d675ea78659"',
        cimName: 'PowerTransformer',
        cimDescription: 'desc 1'
    },
    {
        cimId: '"abddaf44-13e6-46a3-8f87-0d675ea78659"',
        cimName: 'PowerTransformer',
        cimDescription: 'desc 2'
    },
    {
        cimId: '"abddaf44-13e6-46a3-8f87-0d675ea78659"',
        cimName: 'PowerTransformer',
        cimDescription: 'desc 3'
    }
];
