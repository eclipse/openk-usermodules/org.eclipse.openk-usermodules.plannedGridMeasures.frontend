/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { User } from '../model/user';

export const USERS: User[] = [
    {
        id: '1',
        itemName: 'max',
        username: 'max',
        password: 'max',
        name: 'Max Mustermann',
        roles: ['planned-policies-measureplanner', 'test', 'test2'],
        firstName: 'Max',
        lastName: 'Mustermann',
    },
    {
        id: '2',
        itemName: 'admin',
        username: 'admin',
        password: 'admin',
        name: 'Administrator',
        roles: ['test', 'planned-policies-superuser'],
        firstName: 'Administrator',
        lastName: '',
    },
    {
        id: '3',
        itemName: 'otto',
        username: 'otto',
        password: 'otto',
        name: 'Otto Normalverbraucher',
        roles: ['test', 'planned-policies-measureapplicant'],
        firstName: 'Otto',
        lastName: 'Normalverbraucher',
    }
];
