/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { TreeModel, Tree } from 'ng2-tree';

const TREE_MODEL: TreeModel = {
    value: 'tranformator',
    id: 2
};

export const TREE_STRUCTURE: Tree = new Tree(TREE_MODEL);
TREE_STRUCTURE.id = 2;
TREE_STRUCTURE.value = 'tranformator';
TREE_STRUCTURE.node = TREE_MODEL;




