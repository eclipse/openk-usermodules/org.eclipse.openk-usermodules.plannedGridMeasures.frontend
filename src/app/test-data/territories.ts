/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { Territory } from '../model/territory';

export const TERRITORY: Territory[] = [
    { 'id': 1, 'name': 'h1', 'description': 'Strom' },
    { 'id': 2, 'name': 'h2', 'description': 'Gas' },
    { 'id': 4, 'name': 'h3', 'description': 'Wasser' }
];
