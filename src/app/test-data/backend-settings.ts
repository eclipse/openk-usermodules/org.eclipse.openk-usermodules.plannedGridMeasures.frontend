/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { BackendSettings } from '../model/backend-settings';

export const BACKENDSETTINGS: BackendSettings = {
    reminderPeriod: 48,
    appointmentRepetition: [
        {
            id: 1,
            name: 'täglich'
        },
        {
            id: 2,
            name: 'wöchentlich'
        },
        {
            id: 3,
            name: 'monatlich'
        },
        {
            id: 4,
            name: 'jährlich'
        }
    ]
};
