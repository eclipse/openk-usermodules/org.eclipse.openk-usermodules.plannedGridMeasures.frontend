/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/


export const INPUTDATESTRINGARRAY0: string[] = [];

export const INPUTDATESTRINGARRAY1: string[] = ['', ''];

export const INPUTDATESTRINGARRAY2: string[] = ['', 'q'];

export const INPUTDATESTRINGARRAY3: string[] = ['', '2017-01-15T11:11:00z'];

export const INPUTDATESTRINGARRAY4: string[] = ['2017-01-16T11:11:00z', ''];

export const INPUTDATESTRINGARRAY5: string[] = [
    '2017-01-15T11:11:00z',
    '2017-01-15T12:11:00z',
    '2017-02-15T11:11:00z',
    '2017-01-01T11:11:00z'];

export const INPUTDATESTRINGARRAY6: string[] = [
    '2017-01-15T11:11:00z',
    '2017-01-15T12:11:00z',
    '2016-02-15T11:11:00z',
    '2017-01-01T11:11:00z',
    '2017-01-01T11:11:00z',
    '2018-01-01T11:11:00z'];

