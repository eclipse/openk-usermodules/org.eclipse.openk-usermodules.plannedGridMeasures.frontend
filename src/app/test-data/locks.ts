/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { Lock } from '../model/lock';

export const LOCKS: Lock[] = [
    { id: 1, key: 1, username: 'hugo', info: 'gridmeasure' },
    { id: 2, key: 2, username: 'otto', info: 'gridmeasure' }
];
