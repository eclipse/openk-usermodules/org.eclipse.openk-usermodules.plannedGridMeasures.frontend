/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { CalendarEntry } from '../model/calendar-entry';

export const CALENDARENTRY: CalendarEntry[] = [

    {
        gridMeasureId: 1,
        gridMeasureTitle: 'title 1',
        gridMeasureStatusId: 2,
        singleGridMeasureId: 1,
        singleGridMeasureTitle: 'single title',
        plannedStarttimSinglemeasure: '2017-01-15T11:11:00z',
        plannedEndtimeSinglemeasure: '2017-01-15T11:11:00z'

    },
    {
        gridMeasureId: 1,
        gridMeasureTitle: 'title 1',
        gridMeasureStatusId: 6,
        singleGridMeasureId: 2,
        singleGridMeasureTitle: 'single title',
        plannedStarttimSinglemeasure: '2017-01-15T11:11:00z',
        plannedEndtimeSinglemeasure: '2017-01-15T11:11:00z'
    },
    {
        gridMeasureId: 2,
        gridMeasureTitle: 'title 1',
        gridMeasureStatusId: 7,
        singleGridMeasureId: 1,
        singleGridMeasureTitle: 'single title',
        plannedStarttimSinglemeasure: '2017-01-15T11:11:00z',
        plannedEndtimeSinglemeasure: '2017-01-15T11:11:00z'
    },
    {
        gridMeasureId: 3,
        gridMeasureTitle: 'title 1',
        gridMeasureStatusId: 3,
        singleGridMeasureId: 1,
        singleGridMeasureTitle: 'single title',
        plannedStarttimSinglemeasure: '2017-01-15T11:11:00z',
        plannedEndtimeSinglemeasure: '2017-01-15T11:11:00z'
    }

];
