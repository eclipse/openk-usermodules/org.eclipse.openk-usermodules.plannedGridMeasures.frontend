/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { BaseDataLoaderService } from './services/jobs/base-data-loader.service';
import { HttpResponseInterceptorService } from './services/http-response-interceptor.service';
import { SessionContext } from './common/session-context';
import { ToasterMessageService, MessageDefines } from './services/toaster-message.service';
import { User } from './model/user';
import { JwtHelperService } from '@auth0/angular-jwt';
import { HttpClient } from '@angular/common/http';
import { ReminderCallerJobService } from './services/jobs/reminder-caller-job.service';
import { Globals } from './common/globals';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'app';

  constructor(private http: HttpClient, public router: Router,
    public baseDataLoader: BaseDataLoaderService,
    public reminderCallerJobService: ReminderCallerJobService,
    private activatedRoute: ActivatedRoute,
    public httpInterceptor: HttpResponseInterceptorService,
    private sessionContext: SessionContext,
    private messageService: ToasterMessageService) {

    this.http.get('assets/settings.json')
      .subscribe(res => this.sessionContext.settings = res);

    this.sessionContext.centralHttpResultCode$.subscribe(rc => {
      this.onRcFromHttpService(rc);
    });
  }
  ngOnInit() {
    this.extractTokenFromParameters();
    this.processDirectLinkToGridMeasureDetail();
  }

  private processAccessToken(accessToken: string) {
    const jwtHelper: JwtHelperService = new JwtHelperService();
    const decoded: any = jwtHelper.decodeToken(accessToken);
    const user: User = new User();
    user.id = decoded.sub;
    user.username = decoded.preferred_username;
    user.itemName = decoded.preferred_username;
    let firstName = decoded.given_name;
    if (!firstName) {
      firstName = '';
    }
    let lastName = decoded.family_name;
    if (!lastName) {
      lastName = '';
    }

    user.name = firstName + ' ' + lastName;
    user.roles = decoded.realm_access.roles.filter(r => r.includes('planned-policies'));

    this.sessionContext.setCurrUser(user);
    this.sessionContext.setAccessToken(accessToken);
  }

  /**
   * Extract the params (suscribe to router event) and store them in the sessionContext.
   */
  private extractTokenFromParameters() {
    this.activatedRoute.params.subscribe((params) => {
      const accessToken = this.getParametersFromUrl('accessToken');

      if (accessToken) {
        this.processAccessToken(accessToken);
      }
      this.messageService.loginLogoff$.emit(MessageDefines.MSG_LOG_IN_SUCCEEDED);
    });

  }

  private processDirectLinkToGridMeasureDetail() {
    const fwdId = this.getParametersFromUrl('fwdId');
    if (fwdId) {
      this.router.navigate(['/gridMeasureDetail/', fwdId, Globals.MODE.EDIT]);
    }
  }

  private getParametersFromUrl(findParam) {
    const parameterUrl = window.location.search.substr(1);
    return parameterUrl != null && parameterUrl !== '' ? this.readParamAccessToken(parameterUrl, findParam) : null;
  }


  private readParamAccessToken(prmstr, findParam) {
    const params = {};
    const prmarr = prmstr.split('&');
    for (let i = 0; i < prmarr.length; i++) {
      const tmparr = prmarr[i].split('=');
      params[tmparr[0]] = tmparr[1];
    }
    return params[findParam];
  }

  private onRcFromHttpService(rc: number): void {
    if (rc === 401) {
      this.router.navigate(['/logout']);
    }
  }

}
