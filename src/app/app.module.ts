/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { BrowserModule } from '@angular/platform-browser';
import { CalendarModule } from 'angular-calendar';
import { Daterangepicker } from 'ng2-daterangepicker';
import { AppComponent } from './app.component';
import { ButtonsContainerComponent } from './common-components/buttons-container/buttons-container.component';
import { LoadingSpinnerComponent } from './common-components/loading-spinner/loading-spinner.component';
import { MainNavigationComponent } from './common-components/main-navigation/main-navigation.component';
import { FormattedDatePipe } from './common-components/pipes/formatted-date.pipe';
import { FormattedTimestampPipe } from './common-components/pipes/formatted-timestamp.pipe';
import { StringToDatePipe } from './common-components/pipes/string-to-date.pipe';
import { UniquePipe } from './common-components/pipes/unique.pipe';
import { VersionInfoComponent } from './common-components/version-info/version-info.component';
import { SessionContext } from './common/session-context';
import { CustomCalendarComponent } from './custom_modules/calendar/calendar.component';
import { CustomCalendarModule } from './custom_modules/calendar/calendar.module';
import { AppRoutingModule } from './custom_modules/routing/app-routing.module';
import { UndoRedoStackModule } from './custom_modules/undo-redo-stack/undo-redo-stack.module';
import { AbstractListComponent } from './lists/common-components/abstract-list/abstract-list.component';
import { GridMeasuresComponent } from './lists/grid-measures/grid-measures.component';
import { GridMeasureDetailComponent } from './pages/grid-measure-detail/grid-measure-detail.component';
import { StatusChangesComponent } from './lists/status-changes/status-changes.component';
import { StepsComponent } from './lists/steps/steps.component';
import { StepComponent } from './pages/step/step.component';
import { EmailDistributionListComponent } from './lists/email-distribution-list/email-distribution-list.component';
import { EmailDistributionEntryComponent } from './pages/email-distribution-entry/email-distribution-entry.component';
import { LoggedoutPageComponent } from './pages/loggedout/loggedout.component';
import { LogoutPageComponent } from './pages/logout/logout.component';
import { OverviewComponent } from './pages/overview/overview.component';
import { AuthenticationService } from './services/authentication.service';
import { BaseDataService } from './services/base-data.service';
import { BaseHttpService } from './services/base-http.service';
import { DocumentService } from './services/document.service';
import { GridMeasureService } from './services/grid-measure.service';
import { HttpResponseInterceptorService } from './services/http-response-interceptor.service';
import { BaseDataLoaderService } from './services/jobs/base-data-loader.service';
import { LockHelperService } from './services/lock-helper.service';
import { LockService } from './services/lock.service';
import { ReminderCallerJobService } from './services/jobs/reminder-caller-job.service';
import { ReminderService } from './services/reminder.service';
import { UserSettingsService } from './services/user-settings.service';
import { UserService } from './services/user.service';
import { VersionInfoService } from './services/version-info.service';
import { RoleAccessHelperService } from './services/jobs/role-access-helper.service';
import { RequestInterceptor } from './services/request-interceptor.service';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { LOCALE_ID, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import localeDe from '@angular/common/locales/de';
import { registerLocaleData } from '@angular/common';
import { ModeValidator } from './custom_modules/helpers/mode-validator';
import { SingleGridMeasureDetailTabComponent } from './pages/single-grid-measure-detail-tab/single-grid-measure-detail-tab.component';
import { BackendSettingsService } from './services/backend-settings.service';
import { GridMeasureDetailTabComponent } from './pages/grid-measure-detail-tab/grid-measure-detail-tab.component';
import { RoleAccessService } from './services/role-access.service';
import { AgGridModule } from 'ag-grid-angular';
import { CimCacheService } from './services/cim-cache.service';
import { AuthGuard } from './services/auth-guard.service';
import { GridConfigModifierComponent } from './pages/grid-config-modifier/grid-config-modifier.component';
import { ModifyGridConfigService } from './services/modify-grid-config.service';
import { CancelGridMeasureComponent } from './pages/cancel-grid-measure/cancel-grid-measure.component';
import { GridMeasureDetailHeaderComponent } from './pages/grid-measure-detail-header/grid-measure-detail-header.component';
import { TreeModule } from 'ng2-tree';
import { ToasterComponent } from './common-components/toaster/toaster.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastModule } from 'primeng/toast';
import { MessageService } from 'primeng/api';
import { ToasterMessageService } from './services/toaster-message.service';

registerLocaleData(localeDe, 'de');

@NgModule({
  declarations: [
    AppComponent,
    CustomCalendarComponent,
    MainNavigationComponent,
    VersionInfoComponent,
    LoadingSpinnerComponent,
    GridMeasuresComponent,
    StatusChangesComponent,
    StepsComponent,
    StepComponent,
    EmailDistributionListComponent,
    EmailDistributionEntryComponent,
    OverviewComponent,
    GridMeasureDetailComponent,
    ButtonsContainerComponent,
    LogoutPageComponent,
    LoggedoutPageComponent,
    AbstractListComponent,
    CustomCalendarComponent,
    FormattedDatePipe,
    FormattedTimestampPipe,
    StringToDatePipe,
    UniquePipe,
    GridMeasureDetailTabComponent,
    SingleGridMeasureDetailTabComponent,
    GridConfigModifierComponent,
    CancelGridMeasureComponent,
    GridMeasureDetailHeaderComponent,
    ToasterComponent
  ],
  imports: [
    BrowserModule,
    AgGridModule.withComponents([]),
    FormsModule,
    Daterangepicker,
    CalendarModule.forRoot(),
    UndoRedoStackModule,
    CustomCalendarModule,
    HttpClientModule,
    AppRoutingModule,
    ReactiveFormsModule,
    TreeModule,
    BrowserAnimationsModule,
    ToastModule
  ],
  providers: [
    SessionContext,
    ModeValidator,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: RequestInterceptor,
      multi: true
    },
    { provide: LOCALE_ID, useValue: 'de' },
    MessageService,
    ToasterMessageService,
    RoleAccessService,
    BaseHttpService,
    BaseDataLoaderService,
    BaseDataService,
    ReminderCallerJobService,
    ReminderService,
    UserService,
    DocumentService,
    UserSettingsService,
    HttpResponseInterceptorService,
    VersionInfoService,
    AuthenticationService,
    GridMeasureService,
    LockService,
    LockHelperService,
    RoleAccessHelperService,
    BackendSettingsService,
    CimCacheService,
    AuthGuard,
    ModifyGridConfigService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
