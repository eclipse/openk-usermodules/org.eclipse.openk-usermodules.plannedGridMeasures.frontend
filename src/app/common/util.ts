/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { Globals } from './globals';
import { StringToDatePipe } from '../common-components/pipes/string-to-date.pipe';
import { isDate } from '../../../node_modules/moment';
import { isNull } from 'util';
declare var $: any;

export class Util {
    static calcDatepickerDropOrientation(element: HTMLElement): string {
        if ( element instanceof HTMLElement ) {
            return element.getBoundingClientRect().top > Globals.DATEPICKER_HEIGHT ? 'up' : 'down';
        } else {
            return '';
        }
    }

    static showGridmeasureTab(): void {
        $('.nav-tabs a[href="#gridmeasurepanel"]').tab('show');
    }

    static showSingleGridmeasureTab(sortorder: number): void {
        // just wait a bit till the eventually new tab is rendered
        setTimeout(() => $('.nav-tabs a[href="#singlegridmeasure"][id="' + sortorder + '"]').tab('show'));
    }

    static getEarliestValidDateString(dates: string[]): string {
        let earliestDateString = null;
        const pipe = new StringToDatePipe();

        if (dates && dates.length > 0) {

            for (const d of dates) {
                const earliestDate = pipe.transform(earliestDateString);
                const actualDate = pipe.transform(d);

                if (actualDate) {
                    if (isNull(earliestDate) || actualDate.valueOf() < earliestDate.valueOf()) {
                        earliestDateString = d;
                    }
                }
            }
        }

        return isNull(earliestDateString) ? '' : earliestDateString;
    }

    static getLatestValidDateString(dates: string[]): string {
        let latestDateString = null;
        const pipe = new StringToDatePipe();

        if (dates && dates.length > 0) {

            for (const d of dates) {
                const latestDate = pipe.transform(latestDateString);
                const actualDate = pipe.transform(d);

                if (actualDate) {
                    if (isNull(latestDate) || actualDate.valueOf() > latestDate.valueOf()) {
                        latestDateString = d;
                    }
                }
            }
        }

        return isNull(latestDateString) ? '' : latestDateString;
    }
}
