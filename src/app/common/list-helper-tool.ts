/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

export class ListHelperTool {

public checkIfToday( compareDateString: string ): boolean {
    const now = new Date();
    const compareDate: Date = new Date( Date.parse(compareDateString) );

    if ( !compareDate ) {
      return false;
    }

    return  compareDate.getFullYear() === now.getFullYear() &&
            compareDate.getMonth() === now.getMonth() &&
            compareDate.getDate() === now.getDate();
  }
}
