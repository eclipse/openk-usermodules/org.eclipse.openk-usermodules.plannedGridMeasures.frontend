/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/


import { TestBed, async, inject } from '@angular/core/testing';
import { USERS } from '../test-data/users';
import { UserMap } from './user-map';


describe('UserMap', () => {
    beforeEach(() => {
      TestBed.configureTestingModule({
      });

    });

    it('can render a valid user', () => {
        const userMap = new UserMap( USERS );
        expect( userMap.findAndRenderUser('otto')).toBe('Otto Normalverbraucher');
    });

    it('can render a unknown user', () => {
        const userMap = new UserMap( USERS );
        expect( userMap.findAndRenderUser('Unknown')).toBe('[Unknown]');
    });

    it('throws an exception when not initialized', () => {
        let errorOccured: boolean;

        try {
            const userMap = new UserMap(null);
        } catch (e) {
            errorOccured = true;
        }

        expect(errorOccured).toBeTruthy();
    });

    it('handle empty user', () => {
        const userMap = new UserMap( USERS );
        expect( userMap.findAndRenderUser( null )).toBe('');
    });
});

