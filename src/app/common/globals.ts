/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

export class Globals {
    static ROLE_ACCESS = 'ROLE_ACCESS';
    static FRONTEND_VERSION = '0.0.1_SNAPSHOT';
    static SESSION_TOKEN_TAG = 'X-XSRF-TOKEN';
    static BACKEND_SETTINGS = 'BACKEND_SETTINGS';
    static CURRENT_USER = 'CURRENT_USER';
    static ALL_USERS = 'ALL_USERS';
    static ALL_USER_DEPARTMENTS = 'ALL_USER_DEPARTMENTS';
    static BASE_URL = '/mics-home-service/rest/mics/home';
    static BRANCHESNAME = 'BRANCHES';
    static BRANCHLEVELSNAME = 'BRANCHLEVELS';
    static RESPONSIBILITIESNAME = 'RESPONSIBILITIES';
    static NETWORKCONTROLSNAME = 'NETWORKCONTROLS';
    static TERRITORY = 'TERRITORY';
    static USER_SETTINGS = 'USER_SETTINGS';
    static FILTERING_SEARCH_TEXT = 'FILTERING_SEARCH_TEXT';
    static COLUMN_STATE = 'COLUMN_STATE';
    static STATUS_MAIN_FILTER = 'STATUS_MAIN_FILTER';
    static DIRTY_STATE_FILTER = 'DIRTY_STATE_FILTER';
    static COLLAPSE_STATE = 'COLLAPSE_STATE';
    static GRID_MEASURE_COLLAPSABLE = 'GRID_MEASURE_COLLAPSABLE';
    static GRID_MEASURE = 'GRID_MEASURE';
    static READ_ONLY_FORM = 'READ_ONLY_FORM';
    static CANCEL_STAGE = 'CANCEL_STAGE';
    static GRID_MEASURE_MODE = 'GRID_MEASURE_MODE';
    static GRID_MEASURES_SERVICE_NAME = 'planned-grid-measures.openK';
    static CIM_CACHE_SERVICE = 'cim-cache';
    static LOCK_SERVICE_NAME = 'planned-grid-measures.lock.openK';
    static STATUSES = 'STATUSES';
    static OVERDUE_REMINDERS = 'OVERDUE_REMINDERS';
    static UPCOMING_REMINDERS = 'UPCOMING_REMINDERS';
    static CURRENT_REMINDERS = 'CURRENT_REMINDERS';
    static EXPIRED_REMINDERS = 'EXPIRED_REMINDERS';
    static COSTCENTERS = 'COSTCENTERS';
    static EMAILADDRESSES_FROM_TEMPLATE = 'EMAILADDRESSES_FROM_TEMPLATE';
    static EMAILADDRESSES_FROM_GREIDMEASURE = 'EMAILADDRESSES_FROM_GREIDMEASURE';
    static TAB_FILTERING_TEXT = 'TAB_FILTERING_TEXT';
    static GRIDMEASURE_LOCK_TAG = 'gridmeasure';
    static FORCE_UNLOCK = 'FORCE';
    static NO_FORCE_UNLOCK = 'NO_FORCE';
    static APPOINTMENT_NUMBER_OF_MAX_VALUE = 999;
    static MAX_NUMBER_OF_TABS = 10;
    static GRID_ROW_HEIGHT = 32;
    static AUTH_AND_AUTH_SERVICE_NAME = 'authNauth.openK';

    static OAUTH2CONF_SUPERUSER_ROLE = 'planned-policies-superuser';
    static OAUTH2CONF_MEASUREPLANNER_ROLE = 'planned-policies-measureplanner';
    static OAUTH2CONF_MEASUREAPPLICANT_ROLE = 'planned-policies-measureapplicant';

    static ACCESS_TOKEN = 'ACCESS_TOKEN';
    static TIME_TO_HIDE_MESSAGE_BANNER = 20 * 1000;
    static MEGABYT_UNIT = 1000 * 1000;
    static MAX_UPLOADFILE_SIZE = 20;

    static CALENDAR_COLOR_TRANSPARENCY_VALUE = '0.4';

    static BRANCHES = class Branches {
        static power = '1';
        static gas = '2';
        static heating = '3';
        static water = '4';

    };

    static MODE = class Mode {
        static EDIT = 'edit';
        static VIEW = 'view';
        static CANCEL = 'cancel';
    };

    static STATUS = class Status {
        static NEW = 0;
        static APPLIED = 1;
        static CANCELED = 2;
        static FORAPPROVAL = 3;
        static APPROVED = 4;
        static REQUESTED = 5;
        static RELEASED = 6;
        static ACTIVE = 7;
        static IN_WORK = 8;
        static WORK_FINISHED = 9;
        static FINISHED = 10;
        static CLOSED = 11;
        static REJECTED = 12;
    };

    static STATUS_TEXT = class StatusText {
        static CANCEL = 'cancel';
        static QUIT = 'quit';
        static REJECT = 'reject';
        static SAVE = 'save';
        static APPLY = 'apply';
        static FORAPPROVAL = 'forapproval';
        static APPROVE = 'approve';
        static REQUEST = 'request';
        static RELEASE = 'release';
        static ACTIVE = 'activate';
        static IN_WORK = 'inwork';
        static WORK_FINISH = 'workfinish';
        static FINISH = 'finish';
        static CLOSE = 'close';
        static DUPLICATE = 'duplicate';
    };

    static STATUS_BUTTON_LABEL = {
        'cancel': 'Stornieren',
        'quit': 'Abbrechen',
        'reject': 'Zurückweisen',
        'save': 'Speichern',
        'apply': 'Beantragen',
        'forapproval': 'Zur Genehmigung',
        'approve': 'Genehmigen',
        'request': 'Anfordern',
        'release': 'Freigeben',
        'activate': 'Schalten aktiv',
        'inwork': 'In Arbeit',
        'workfinish': 'Arbeit beenden',
        'finish': 'Maßnahme beenden',
        'close': 'Maßnahme schließen',
        'duplicate': 'Netzmaßnahme duplizieren'
    };

    static LOCALSTORAGE_SESSION_ID = '/elogbook/session-id';
    static SORTING_STATE = 'SORTING_STATE';

    static TYPE_WHITELIST = ['application/pdf',
        'application/vnd.ms-excel',
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        'application/vnd.ms-excel.sheet.macroEnabled.12',
        'application/msword',
        'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
        'application/vnd.ms-word.document.macroEnabled.12',
        'image/png',
        'image/jpeg'
    ];

    static REMINDER_TIME = 48;
    static ONE_HOUR = 1000 * 60 * 60;
    static REMINDER_JOB_POLLING_INTERVALL = 60000;
    static REMINDER_JOB_POLLING_START_DELAY = 2000;

    static DATEPICKER_HEIGHT = 360;

    static TEMP_ID_TO_SHOW_NEW_STEPS = -1;
    static TEMP_ID_TO_SHOW_NEW_EMAILDISTRIBUTIONENTRYS = -1;
}
