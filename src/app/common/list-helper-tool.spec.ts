/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { TestBed, async, inject } from '@angular/core/testing';
import { ListHelperTool } from './list-helper-tool';

describe('ListHelperTool', () => {
    beforeEach(() => {
      TestBed.configureTestingModule({
       });

    });
    it('should detect today correctly', () => {
        const tool = new ListHelperTool();
        expect( tool.checkIfToday(null)).toBeFalsy();
        const today = new Date();
        today.setHours( 12, 13, 14, 59 );
        expect( tool.checkIfToday(today.toISOString()) ).toBeTruthy();

        today.setHours( 25, 1, 1, 1 );
        expect( tool.checkIfToday(today.toISOString()) ).toBeFalsy();
    });
});
