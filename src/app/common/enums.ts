/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

export enum StatusEn {
    open = 1,
    inWork = 2,
    done = 3,
    closed = 4
}

export enum ErrorType {
    create = 1,
    update = 2,
    delete = 3,
    retrieve = 4,
    authentication = 5,
    upload = 6,
    locked = 7,
    datedependency = 8,
    stornoLocked = 9
}

export enum ToasterButtonEventEn {

    unlockGridMeasure = 1,
    deleteSingleGridMeasure = 2
}

