/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { Util } from './util';
import { Globals } from './globals';
import {
    INPUTDATESTRINGARRAY0,
    INPUTDATESTRINGARRAY1,
    INPUTDATESTRINGARRAY2,
    INPUTDATESTRINGARRAY3,
    INPUTDATESTRINGARRAY4,
    INPUTDATESTRINGARRAY5,
    INPUTDATESTRINGARRAY6
} from './../test-data/datestringarrays';

describe('Util', () => {
    let element: HTMLElement;

    beforeEach(() => {
        element = document.createElement('div');
    });

    it('calculates the correct drop orientation', () => {
        spyOn(element, 'getBoundingClientRect').and.returnValue({'top': Globals.DATEPICKER_HEIGHT});
        expect(Util.calcDatepickerDropOrientation(element)).toBe('down');
    });

    it('calculates the correct drop orientation', () => {
        spyOn(element, 'getBoundingClientRect').and.returnValue({'top': Globals.DATEPICKER_HEIGHT - 1});
        expect(Util.calcDatepickerDropOrientation(element)).toBe('down');
    });

    it('calculates the correct drop orientation', () => {
        spyOn(element, 'getBoundingClientRect').and.returnValue({'top': Globals.DATEPICKER_HEIGHT + 1});
        expect(Util.calcDatepickerDropOrientation(element)).toBe('up');
    });

    it('calculates the correct drop orientation for no valid HTML element', () => {
        expect(Util.calcDatepickerDropOrientation(null)).toBe('');
    });

    it('should calculate earliest datestring', () => {
        expect(Util.getEarliestValidDateString(INPUTDATESTRINGARRAY0)).toBe('');
        expect(Util.getEarliestValidDateString(INPUTDATESTRINGARRAY1)).toBe('');
        expect(Util.getEarliestValidDateString(INPUTDATESTRINGARRAY2)).toBe('');
        expect(Util.getEarliestValidDateString(INPUTDATESTRINGARRAY3)).toBe('2017-01-15T11:11:00z');
        expect(Util.getEarliestValidDateString(INPUTDATESTRINGARRAY4)).toBe('2017-01-16T11:11:00z');
        expect(Util.getEarliestValidDateString(INPUTDATESTRINGARRAY5)).toBe('2017-01-01T11:11:00z');
        expect(Util.getEarliestValidDateString(INPUTDATESTRINGARRAY6)).toBe('2016-02-15T11:11:00z');
    });

    it('should calculate latest datestring', () => {
        expect(Util.getLatestValidDateString(INPUTDATESTRINGARRAY0)).toBe('');
        expect(Util.getLatestValidDateString(INPUTDATESTRINGARRAY1)).toBe('');
        expect(Util.getLatestValidDateString(INPUTDATESTRINGARRAY2)).toBe('');
        expect(Util.getLatestValidDateString(INPUTDATESTRINGARRAY3)).toBe('2017-01-15T11:11:00z');
        expect(Util.getLatestValidDateString(INPUTDATESTRINGARRAY4)).toBe('2017-01-16T11:11:00z');
        expect(Util.getLatestValidDateString(INPUTDATESTRINGARRAY5)).toBe('2017-02-15T11:11:00z');
        expect(Util.getLatestValidDateString(INPUTDATESTRINGARRAY6)).toBe('2018-01-01T11:11:00z');
    });
});
