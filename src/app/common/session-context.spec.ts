/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { SessionContext } from './session-context';
import { User } from '../model/user';
import { USERS } from '../test-data/users';
import { UserMap } from './user-map';
import { Branch } from '../model/branch';
import { CostCenter } from '../model/cost-center';
import { GRIDMEASURE } from '../test-data/grid-measures';
import { GridMeasure } from '../model/grid-measure';
import { StatusMainFilter } from '../model/status-main-filter';
import { UserSettings, SettingType, SettingValue } from '../model/user-settings';
import { Territory } from '../model/territory';
import { Component } from 'ag-grid';
import { componentFactoryName } from '@angular/compiler';
import { Globals } from './globals';


describe('SessionContext', () => {
  const gridmeasures: GridMeasure[] = JSON.parse(JSON.stringify(GRIDMEASURE));
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SessionContext]
    });
  });

  it('can instatiate service when inject service', inject([SessionContext], (service: SessionContext) => {
    expect(service instanceof SessionContext);
  }));

  it('can store a SessionId and a User', inject([SessionContext], (service: SessionContext) => {
    service.setCurrSessionId('SpecialSessionId');
    const usr = new User();
    usr.id = '44';
    usr.name = 'Rudi';
    service.setCurrUser(usr);
    expect(service.getCurrSessionId()).toBe('SpecialSessionId');
    expect(service.getCurrUser()).not.toBeNull();
    expect(service.getCurrUser().id).toBe('44');
    expect(service.getCurrUser().name).toBe('Rudi');
  }));

  it('create a usermapper should fail when empty', inject([SessionContext], (service: SessionContext) => {
    let errorOccured = false;
    try {
      const um = service.getUserMap();
    } catch (ea) {
      errorOccured = true;
    }

    expect(errorOccured).toBe(true);
  }));

  it('create a usermapper should work with all users set', inject([SessionContext], (service: SessionContext) => {
    let errorOccured = false;
    let um: UserMap;
    service.setAllUsers(USERS);
    try {
      um = service.getUserMap();
    } catch (ea) {
      errorOccured = true;
    }

    expect(errorOccured).toBe(false);
    expect(um.findUser('otto').username).toBe('otto');
  }));

  it('should return the correct statusById', inject([SessionContext], (sessionContext: SessionContext) => {
    sessionContext.setStatuses([{ id: 1, name: 'offen', colorCode: '#990000' }, { id: 2, name: 'in Bearbeitung', colorCode: '#990000' },
    { id: 3, name: 'beendet', colorCode: '#990000' }]);
    expect(sessionContext.getStatusById(666)).toBeNull();
    expect(sessionContext.getStatusById(3).id).toBe(3);

    sessionContext.setStatuses(null);
    expect(sessionContext.getStatusById(45).name).toBe('NOSTATUS');
  }));

  it('should return the correct branchClassByName', inject([SessionContext], (sessionContext: SessionContext) => {
    const branches: Branch[] = [
      { 'id': 1, 'name': 'S', 'description': 'Strom', 'colorCode': '' },
      { 'id': 2, 'name': 'G', 'description': 'Gas', 'colorCode': '' },
      { 'id': 4, 'name': 'W', 'description': 'Wasser', 'colorCode': '' },
      { 'id': 3, 'name': 'F', 'description': 'Fernwärme', 'colorCode': '' }
    ];
    sessionContext.setBranches(branches);
    expect(sessionContext.getBranchById(1).name).toBe('S');
    expect(sessionContext.getBranchById(666)).toBeNull();
    expect(sessionContext.getBranchById(undefined)).toBeNull();

    sessionContext.setBranches(null);
    expect(sessionContext.getBranchById(45).name).toBe('NOBRANCHES');
  }));

  it('should get and set territory', inject([SessionContext], (sessionContext: SessionContext) => {
    const territories: any = [];
    sessionContext.setTerritories(territories);

    expect(sessionContext.getTerritories()).toBeDefined();
  }));

  it('should get and set sorting state', inject([SessionContext], (sessionContext: SessionContext) => {
    const sortState: any = {};
    sessionContext.setSortingState(JSON.stringify(sortState));

    expect(sessionContext.getSortingState()).toBeDefined();
  }));

  it('should get and set filtering search text', inject([SessionContext], (sessionContext: SessionContext) => {
    const filterText = 'text';
    sessionContext.setFilteringSearchText(JSON.stringify(filterText));

    expect(sessionContext.getFilteringSearchText()).toBeDefined();
    expect(sessionContext.getFilteringSearchText()).toBe('text');
  }));

  it('should get and set column state', inject([SessionContext], (sessionContext: SessionContext) => {
    const sortState: any = {};
    sessionContext.setColumnState(JSON.stringify(sortState));

    expect(sessionContext.getColumnState()).toBeDefined();
  }));

  it('should get and set status main filter state', inject([SessionContext], (sessionContext: SessionContext) => {
    const filterState = new StatusMainFilter();
    filterState.item.isCanceledStatusActive = true;
    filterState.item.isClosedStatusActive = true;

    sessionContext.setStatusMainFilter(filterState);

    expect(sessionContext.getStatusMainFilter()).toBeDefined();
    expect(sessionContext.getStatusMainFilter().item.isCanceledStatusActive).toBe(true);
    expect(sessionContext.getStatusMainFilter().item.isClosedStatusActive).toBe(true);
  }));

  it('should get and set filters dirty state', inject([SessionContext], (sessionContext: SessionContext) => {
    const dirtyState = true;

    sessionContext.setFilterDirtyState(dirtyState);

    expect(sessionContext.getFilterDirtyState()).toBeTruthy();
  }));

  it('should get and set user settings', inject([SessionContext], (sessionContext: SessionContext) => {
    const usersettings = new UserSettings();
    usersettings.settingType = new SettingType();
    usersettings.username = 'otto';
    usersettings.value = new SettingValue();

    sessionContext.setUserSettings(JSON.stringify(usersettings));

    expect(sessionContext.getUserSettings()).toBeDefined();
  }));

  it('should get and set tabFilterState', inject([SessionContext], (sessionContext: SessionContext) => {
    const filterState = {
      branchId: '3',
      title: 'Bruno',
      statusId: '5'

    };
    sessionContext.setTabFilteringState(filterState);

    expect(sessionContext.getTabFilteringState().title).toBe('Bruno');
  }));


  it('should get and set FilterExpansionState', inject([SessionContext], (sessionContext: SessionContext) => {
    sessionContext.setFilterExpansionState(true);
    expect(sessionContext.getFilterExpansionState()).toBeTruthy();

    sessionContext.setFilterExpansionState(false);
    expect(sessionContext.getFilterExpansionState()).toBeFalsy();
  }));

  it('should get and set CostCenters correctly', inject([SessionContext], (sessionContext: SessionContext) => {
    const cc: CostCenter[] = [{ id: 666, name: 'Claudio' }, { id: 4711, name: 'Bertil' }];
    sessionContext.setCostCenters(null);
    expect(sessionContext.getCostCenterById(333).name).toBe('NOCOSTCENTER');
    sessionContext.setCostCenters(cc);
    expect(sessionContext.getCostCenters().length).toBe(2);
    expect(sessionContext.getCostCenterById(4711).name).toBe('Bertil');
    expect(sessionContext.getCostCenterById(333)).toBeNull();

  }));

  it('should get and set EmailAddresses correctly', inject([SessionContext], (sessionContext: SessionContext) => {
    const emailAddresses: string[] = ['testmail@test.de', 'testmail2@test.de'];
    sessionContext.setEmailAddressesFromTemplates(null);
    expect(sessionContext.getEmailAddressesFromTemplates()).toBe(null);
    sessionContext.setEmailAddressesFromTemplates(emailAddresses);
    expect(sessionContext.getEmailAddressesFromTemplates().length).toBe(2);
    expect(sessionContext.getEmailAddressesFromTemplates()[0]).toBe('testmail@test.de');

  }));

  it('should return the correct info isShorttermNotification', inject([SessionContext], (sessionContext: SessionContext) => {

    sessionContext.setCurrentReminders([1111, 2222]);
    expect(sessionContext.getCurrentReminders().length).toBe(2);
    expect(sessionContext.isShorttermNotification(666)).toBeFalsy();
    expect(sessionContext.isShorttermNotification(2222)).toBeTruthy();

    sessionContext.setCurrentReminders(null);
    expect(sessionContext.getCurrentReminders().length).toBe(0);
    expect(sessionContext.isShorttermNotification(2222)).toBeFalsy();
  }));

  it('should get and set isReminder', inject([SessionContext], (sessionContext: SessionContext) => {

    sessionContext.setUpcomingReminder(false);
    expect(sessionContext.getUpcomingReminder()).toBe(false);
    sessionContext.setUpcomingReminder(true);
    expect(sessionContext.getUpcomingReminder()).toBe(true);
  }));

  it('should get and set AccessToken', inject([SessionContext], (sessionContext: SessionContext) => {
    const accessToken = 'eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJodVl0e' +
      'VByUEVLQ1phY3FfMW5sOGZscENETnFHdmZEZHctYUxGQXNoWHZVIn0.eyJqdGkiOiI4ZmY5NTlhZC' +
      '02ODQ1LTRlOGEtYjRiYi02ODQ0YjAwMjU0ZjgiLCJleHAiOjE1MDY2MDA0NTAsIm5iZiI6MCwiaWF' +
      '0IjoxNTA2NjAwMTUwLCJpc3MiOiJodHRwOi8vZW50amF2YTAwMjo4MDgwL2F1dGgvcmVhbG1zL2Vs' +
      'b2dib29rIiwiYXVkIjoiZWxvZ2Jvb2stYmFja2VuZCIsInN1YiI6IjM1OWVmOWM5LTc3ZGYtNGEzZ' +
      'C1hOWM5LWY5NmQ4MzdkMmQ1NyIsInR5cCI6IkJlYXJlciIsImF6cCI6ImVsb2dib29rLWJhY2tlbm' +
      'QiLCJhdXRoX3RpbWUiOjAsInNlc3Npb25fc3RhdGUiOiI5NjVmNzM1MS0yZThiLTQ1MjgtOWYzZC1' +
      'lZTYyODNhOTViMTYiLCJhY3IiOiIxIiwiYWxsb3dlZC1vcmlnaW5zIjpbIioiXSwicmVhbG1fYWNj' +
      'ZXNzIjp7InJvbGVzIjpbImVsb2dib29rLXN1cGVydXNlciIsImVsb2dib29rLW5vcm1hbHVzZXIiL' +
      'CJ1bWFfYXV0aG9yaXphdGlvbiJdfSwicmVzb3VyY2VfYWNjZXNzIjp7InJlYWxtLW1hbmFnZW1lbn' +
      'QiOnsicm9sZXMiOlsidmlldy11c2VycyIsInF1ZXJ5LWdyb3VwcyIsInF1ZXJ5LXVzZXJzIl19LCJ' +
      'hY2NvdW50Ijp7InJvbGVzIjpbIm1hbmFnZS1hY2NvdW50IiwibWFuYWdlLWFjY291bnQtbGlua3Mi' +
      'LCJ2aWV3LXByb2ZpbGUiXX19LCJuYW1lIjoiQWRtaW5pc3RyYXRvciBBZG1pbmlzdHJhdG93aWNoI' +
      'iwicHJlZmVycmVkX3VzZXJuYW1lIjoiYWRtaW4iLCJnaXZlbl9uYW1lIjoiQWRtaW5pc3RyYXRvci' +
      'IsImZhbWlseV9uYW1lIjoiQWRtaW5pc3RyYXRvd2ljaCIsImVtYWlsIjoic2VyZ2VqLmtlcm5AcHRh' +
      'LmRlIiwicm9sZXN0ZXN0IjoiW2Vsb2dib29rLXN1cGVydXNlciwgZWxvZ2Jvb2stbm9ybWFsdXNlc' +
      'iwgdW1hX2F1dGhvcml6YXRpb24sIG9mZmxpbmVfYWNjZXNzLCB1bWFfYXV0aG9yaXphdGlvbiwgZW' +
      'xvZ2Jvb2stbm9ybWFsdXNlcl0ifQ.o94Bl43oqyLNzZRABvIq9z-XI8JQjqj2FSDdUUEZGZPTN4uw' +
      'D5fyi0sONbDxmTFvgWPh_8ZhX6tlDGiupVDBY4eRH43Eettm-t4CDauL7FzB3w3dDPFMB5DhP4rrp' +
      'k_kATwnY2NKLRbequnh8Z6wLXjcmQNLgrgknXB_gogWAqH29dqKexwceMNIbq-kjaeLsmHSXM9TE9' +
      'q7_Ln9el04OlkpOVspVguedfINcNFg0DmYLJWyD2ORkOHLmYigN6YnyB9P2NFOnKGlLuQ87GjosI0' +
      '0zBniRGi3PhE9NGd51Qggdbcsm0aM8GiMaZ7SO5i8iQWL10TRFRFyTEfy6hSO8g';
    sessionContext.setAccessToken(accessToken);
    expect(sessionContext.getAccessTokenDecoded().name).toBe('Administrator Administratowich');

  }));

  it('should get and set the grid measure correctly', inject([SessionContext], (sessionContext: SessionContext) => {
    const gridMeasureDetail = JSON.parse(JSON.stringify(gridmeasures[0]));
    sessionContext.setGridMeasureDetail(gridMeasureDetail);
    expect(sessionContext.getGridMeasureDetail().title).toBe('T1');
    expect(sessionContext.getGridMeasureDetail().remark).toBe('TESTREMARK1');
  }));

  it('should get and set the cancel page stage', inject([SessionContext], (sessionContext: SessionContext) => {
    sessionContext.setCancelStage(true);
    expect(sessionContext.isInCancelPage()).toBeTruthy();

    sessionContext.setCancelStage(false);
    expect(sessionContext.isInCancelPage()).toBeFalsy();
  }));

  it('should set bell color', inject([SessionContext], (sessionContext: SessionContext) => {
    spyOn(sessionContext, 'setBellColor').and.callThrough();
    // error case - red color
    sessionContext.setOverdueReminder(true);
    sessionContext.setUpcomingReminder(false);
    const errorcolor: string = sessionContext.setBellColor();
    expect(errorcolor).toBe('red');

    // warning case - orange color
    sessionContext.setOverdueReminder(false);
    sessionContext.setUpcomingReminder(true);
    const warningcolor: string = sessionContext.setBellColor();
    expect(warningcolor).toBe('#f79e60');

    // nothing case - grey color
    sessionContext.setOverdueReminder(false);
    sessionContext.setUpcomingReminder(false);
    const nothingcolor: string = sessionContext.setBellColor();
    expect(nothingcolor).toBe('grey');
  }));

  it('should return false if it is new or applied staus', inject([SessionContext], (sessionContext: SessionContext) => {
    spyOn(sessionContext, 'isReadOnlyForStatus').and.callThrough();
    const gridMeasureDetail = JSON.parse(JSON.stringify(gridmeasures[0]));
    gridMeasureDetail.statusId = Globals.STATUS.NEW;

    const isReadOnly: boolean = sessionContext.isReadOnlyForStatus(gridMeasureDetail);
    expect(isReadOnly).toBeFalsy();
  }));

  it('should return true if it is NOT new or applied staus', inject([SessionContext], (sessionContext: SessionContext) => {
    spyOn(sessionContext, 'isReadOnlyForStatus').and.callThrough();
    const gridMeasureDetail = JSON.parse(JSON.stringify(gridmeasures[0]));
    gridMeasureDetail.statusId = Globals.STATUS.IN_WORK;

    const isReadOnly: boolean = sessionContext.isReadOnlyForStatus(gridMeasureDetail);
    expect(isReadOnly).toBeTruthy();
  }));

});
