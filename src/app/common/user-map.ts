/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { User } from '../model/user';

export class UserMap {
    private mappedUsers: User[];

    constructor(allUsers: User[]) {
        this.mappedUsers = [];
        if (!allUsers) {
            console.log('UserMap was created without any Users!');
            throw new EvalError('UserMap was created without any Users!');
        }
        for (const usr of allUsers) {
            this.mappedUsers[usr.username] = usr;
        }
    }

    public findUser(usrShort: string): User {
        return this.mappedUsers[usrShort];
    }

    public findAndRenderUser(shortUsr: string): string {
        if ( !shortUsr ) {
            return '';
        }

        const usr = this.findUser(shortUsr);
        if (!usr) {
            return '[' + shortUsr + ']';
        } else {
            return usr.firstName + (usr.lastName ? ' ' + usr.lastName : '');
        }
    }
}
