/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ToasterComponent } from './toaster.component';
import { MessageService } from 'primeng/api';
import { MockComponent } from '../../testing/mock.component';
import { ToasterMessageService } from '../../services/toaster-message.service';
import { SessionContext } from '../../common/session-context';
import { ToasterButtonEventEn } from '../../common/enums';

describe('ToasterComponent', () => {
  let component: ToasterComponent;
  let fixture: ComponentFixture<ToasterComponent>;
  let messageService: MessageService;
  let toasterMessageService: ToasterMessageService;
  let sessionStorage: SessionContext;

  beforeEach(async(() => {
    sessionStorage = new SessionContext;
    messageService = new MessageService;
    toasterMessageService = new ToasterMessageService(sessionStorage, messageService);
    TestBed.configureTestingModule({
      declarations: [ToasterComponent,
        MockComponent({ selector: 'p-toast', inputs: ['modal', 'baseZIndex'] })],
      providers: [
        { provide: ToasterMessageService, useValue: toasterMessageService }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ToasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should emit delete single grid measure after click on yes button', () => {
    spyOn(component, 'onSingleGMDeleteConfirm').and.callThrough();

    component.onSingleGMDeleteConfirm();
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect((component as any).toasterMessageService.clear).toHaveBeenCalledWith('deletec');
      expect((component as any).toasterMessageService.toasterEmitter$.emit).
        toHaveBeenCalledWith(ToasterButtonEventEn.deleteSingleGridMeasure);
    });
  });

  it('should clear toaster if question rejected', () => {
    spyOn(component, 'onSingleGMReject').and.callThrough();

    component.onSingleGMReject();
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect((component as any).toasterMessageService.clear).toHaveBeenCalledWith('deletec');
    });
  });

  it('should emit unlock grid measure after click on button', () => {
    spyOn(component, 'onUnlockConfirm').and.callThrough();

    component.onUnlockConfirm();
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect((component as any).toasterMessageService.clear).toHaveBeenCalledWith('unlockc');
      expect((component as any).toasterMessageService.toasterEmitter$.emit).
        toHaveBeenCalledWith(ToasterButtonEventEn.unlockGridMeasure);
    });
  });

  it('should clear toaster if question rejected', () => {
    spyOn(component, 'onUnlcokReject').and.callThrough();

    component.onUnlcokReject();
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect((component as any).toasterMessageService.clear).toHaveBeenCalledWith('unlockc');
    });
  });

  it('should clear toaster', () => {
    spyOn(component, 'clear').and.callThrough();

    component.clear();
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect((component as any).toasterMessageService.clear).toHaveBeenCalled();
    });
  });
});
