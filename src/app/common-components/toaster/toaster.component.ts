/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { Component, NgModule } from '@angular/core';
import { ToasterButtonEventEn } from '../../common/enums';
import { ToasterMessageService } from '../../services/toaster-message.service';
import { ToastModule } from 'primeng/toast';

@Component({
  selector: 'app-toaster',
  templateUrl: './toaster.component.html',
  styleUrls: ['./toaster.component.css']
})

@NgModule({
  exports: [ToastModule],
  declarations: [ToastModule],
})

export class ToasterComponent {

  constructor(public messageService: ToasterMessageService) { }


  onSingleGMDeleteConfirm() {
    this.messageService.toasterEmitter$.emit(ToasterButtonEventEn.deleteSingleGridMeasure);
    this.messageService.clear('deletec');
  }

  onSingleGMReject() {
    this.messageService.clear('deletec');
  }

  onUnlockConfirm() {
    this.messageService.toasterEmitter$.emit(ToasterButtonEventEn.unlockGridMeasure);
    this.messageService.clear('unlockc');

  }

  onUnlcokReject() {
    this.messageService.clear('unlockc');
  }

  clear() {
    this.messageService.clear();
  }

}
