/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NgModule } from '@angular/core';
import { MockComponent } from '../../testing/mock.component';
import { RouterTestingModule } from '@angular/router/testing';
import { Router } from '../../testing/router-stubs';
import { SessionContext } from '../../common/session-context';
import { MainNavigationComponent } from './main-navigation.component';
import { VersionInfoComponent } from '../../common-components/version-info/version-info.component';

class FakeRouter {
  navigate(commands: any[]) {
    return commands[0];
  }
}

@NgModule({
  declarations: [],
  entryComponents: [
    VersionInfoComponent
    // LogoutComponent
  ]
})
class TestModule { }

describe('MainNavigationComponent', () => {
  let component: MainNavigationComponent;
  let fixture: ComponentFixture<MainNavigationComponent>;
  let routerStub: FakeRouter;
  let sessionContext: SessionContext;

  routerStub = {
    navigate: jasmine.createSpy('navigate').and.callThrough()
  };


  beforeEach(async(() => {
    sessionContext = new SessionContext();
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes([]),
      ],
      declarations: [
        MainNavigationComponent,
        MockComponent({ selector: 'input', inputs: ['options'] }),
        MockComponent({ selector: 'app-version-info' })
      ],
      providers: [
        { provide: Router, useValue: routerStub },
        { provide: SessionContext, useValue: sessionContext }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainNavigationComponent);
    component = fixture.componentInstance;
  });

  it('should navigate to overview on home-button click', () => {
    component.goToOverview();
    expect(routerStub.navigate).toHaveBeenCalledWith(['/overview']);
  });

  it('should navigate to mainview on logout', () => {
    spyOn(sessionContext, 'clearStorage').and.callThrough();
    component.logout();
    expect(sessionContext.clearStorage).toHaveBeenCalled();
  });

  it('should navigate to Logout on logout click', () => {
    component.goToLogout();
    expect(routerStub.navigate).toHaveBeenCalledWith(['/logout']);
  });

});
