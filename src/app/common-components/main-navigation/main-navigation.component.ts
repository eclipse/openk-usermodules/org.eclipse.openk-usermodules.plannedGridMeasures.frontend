/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { Component, OnInit } from '@angular/core';
import { SessionContext } from '../../common/session-context';
import { Router } from '@angular/router';

@Component({
  selector: 'app-main-navigation',
  templateUrl: './main-navigation.component.html',
  styleUrls: ['./main-navigation.component.css']
})
export class MainNavigationComponent implements OnInit {

  constructor(
    public sessionContext: SessionContext,

    public router: Router
  ) { }

  ngOnInit() {
  }

  logout() {
    this.sessionContext.clearStorage();
  }

  goToOverview() {
    this.router.navigate(['/overview']);
  }

  goToLogout() {
    this.router.navigate(['/logout']);
  }

}
