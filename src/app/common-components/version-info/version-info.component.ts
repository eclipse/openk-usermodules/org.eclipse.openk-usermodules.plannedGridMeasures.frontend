/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { Component, OnInit } from '@angular/core';
import { VersionInfo } from '../../model/version-info';
import { VersionInfoService } from '../../services/version-info.service';
import { Globals } from '../../common/globals';

@Component({
  selector: 'app-version-info',
  templateUrl: './version-info.component.html',
  styleUrls: ['./version-info.component.css'],
})
export class VersionInfoComponent implements OnInit {
  currVersion: VersionInfo = {
    frontendVersion: '?',
    backendVersion: '?',
    dbVersion: '?'
  };

  constructor(private _btbService: VersionInfoService) { }

  ngOnInit() {
    this._btbService.loadBackendServerInfo().subscribe(vinfo => this.setVersionInfo(vinfo),
      error => console.log(error));
  }

  private setVersionInfo(vinfo: VersionInfo) {
    this.currVersion = vinfo;
    this.currVersion.frontendVersion = Globals.FRONTEND_VERSION;
  }

}
