/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { AbstractMockObservableService } from '../../testing/abstract-mock-observable.service';
import { VersionInfoComponent } from './version-info.component';
import { VersionInfo } from '../../model/version-info';
import { VersionInfoService } from '../../services/version-info.service';
import { Globals } from '../../common/globals';


describe('VersionInfoComponent', () => {
  let component: VersionInfoComponent;
  let fixture: ComponentFixture<VersionInfoComponent>;
  let de: DebugElement;  // the DebugElement with the welcome message
  let el: HTMLElement; // the DOM element with the welcome message
  class MockService extends AbstractMockObservableService {
    loadBackendServerInfo() {
      return this;
    }
  }
  let mockService;

  beforeEach(async(() => {
    mockService = new MockService();

    TestBed.configureTestingModule({
      declarations: [VersionInfoComponent],
      providers: [{ provide: VersionInfoService, useValue: mockService }],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VersionInfoComponent);
    component = fixture.componentInstance;

  });

  it('should show VersionString after loadBackendServerInfo', () => {
    const vinfo = { frontendVersion: 'AVersion', backendVersion: '1.0', dbVersion: '2.0' };
    mockService.content = vinfo;
    de = fixture.debugElement.query(By.css('.version-info'));
    el = de.nativeElement;

    fixture.detectChanges();

    const targetString = Globals.FRONTEND_VERSION + ' / ' + vinfo.backendVersion + ' / ' + vinfo.dbVersion;
    expect(el.textContent).toContain(targetString);
  });

  it('should show ??? while not init', () => {
    mockService.error = 'any Error';
    de = fixture.debugElement.query(By.css('.version-info'));
    el = de.nativeElement;

    fixture.detectChanges();

    const targetString = '? / ? / ?';
    expect(el.textContent).toContain(targetString);
  });


});
