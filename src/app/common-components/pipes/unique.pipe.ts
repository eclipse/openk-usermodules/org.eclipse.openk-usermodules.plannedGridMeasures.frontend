/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/


import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'unique'
})
export class UniquePipe implements PipeTransform {

  transform(items: any[], fieldName: string): any[] {
    const cArray = [];
    if (!items) {
      return [];
    }
    return items.filter((item, index, arr) => {
      const itemExists = cArray.find(ia => {
        return ia === item[fieldName];
      }
      );
      if ((item[fieldName] || item[fieldName] === 0) && !itemExists && itemExists !== 0) {
        cArray.push(item[fieldName]);
        return true;
      } else {
        return false;
      }
    });
  }
}
