/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { SessionContext } from './../../common/session-context';
import { Controls } from './../../model/role-access';
import { Component, Input, EventEmitter, Output, SimpleChanges, OnChanges } from '@angular/core';
import { Globals } from '../../common/globals';
import { RoleAccessHelperService } from '../../services/jobs/role-access-helper.service';

@Component({
  selector: 'app-buttons-container',
  templateUrl: './buttons-container.component.html',
  styleUrls: ['./buttons-container.component.css']
})
export class ButtonsContainerComponent implements OnChanges {
  Globals = Globals;
  @Output() clickSaveButton: EventEmitter<string> = new EventEmitter();
  @Output() clickApplyButton: EventEmitter<string> = new EventEmitter();
  @Output() clickQuitButton: EventEmitter<string> = new EventEmitter();
  @Output() clickCancelButton: EventEmitter<string> = new EventEmitter();
  @Output() clickForApprovalButton: EventEmitter<string> = new EventEmitter();
  @Output() clickApprovedButton: EventEmitter<string> = new EventEmitter();
  @Output() clickRejectButton: EventEmitter<string> = new EventEmitter();
  @Output() clickReleaseButton: EventEmitter<string> = new EventEmitter();
  @Output() clickActivateButton: EventEmitter<string> = new EventEmitter();
  @Output() clickInWorkButton: EventEmitter<string> = new EventEmitter();
  @Output() clickWorkFinishButton: EventEmitter<string> = new EventEmitter();
  @Output() clickCloseButton: EventEmitter<string> = new EventEmitter();
  @Output() clickRequestButton: EventEmitter<string> = new EventEmitter();
  @Output() clickFinishButton: EventEmitter<string> = new EventEmitter();
  @Output() clickDuplicateButton: EventEmitter<string> = new EventEmitter();

  @Input() isValidForSave: boolean;
  @Input() isValidForm: boolean;
  @Input() isReadOnlyForm: boolean;
  @Input() gridMeasureStatusId: string;
  @Input() gridMeasureId: number;

  validForSave: boolean;
  validForm: boolean;
  readOnlyForm: boolean;
  gmStatus: number;
  hideSaveButton = false;
  hideDuplicateButtonForRole = true;
  hideRejectButton = false;
  positiveStatusChange: string;
  noActivePositiveBtn = false;
  positiveBtnLabel: string;
  lisOfActiveButtons: string[] = [];
  controls: Controls;

  constructor(
    public sessionContext: SessionContext,
    public roleAccessHelper: RoleAccessHelperService
  ) { }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['isValidForSave']) {
      this.validForSave = changes['isValidForSave'].currentValue;
    }
    if (changes['isValidForm']) {
      this.validForm = changes['isValidForm'].currentValue;
    }
    if (changes['isReadOnlyForm']) {
      this.readOnlyForm = changes['isReadOnlyForm'].currentValue;
      this.gmStatus = Number(this.gridMeasureStatusId);
      this.getButtonsForStatus();
    }
  }

  getButtonsForStatus() {
    if (isNaN(this.gmStatus)) {
      this.gmStatus = Globals.STATUS.NEW;
    }
    this.controls = this.roleAccessHelper.getRoleAccessDefinitions().controls.
      filter(statusId => statusId.gridMeasureStatusId === this.gmStatus)[0];
    if (!this.readOnlyForm) {
      this.lisOfActiveButtons = this.controls.activeButtons;
    } else {
      this.lisOfActiveButtons = [];
    }
    this.handleNegativeButtonsForStatus();
    this.handlePositiveButtonsForStatus();
    this.handleDuplicateButton();
  }

  handleNegativeButtonsForStatus() {
    // negative buttons (left side)
    this.hideRejectButton = !this.lisOfActiveButtons.includes(Globals.STATUS_TEXT.REJECT);
  }

  handlePositiveButtonsForStatus() {
    this.hideSaveButton = !this.lisOfActiveButtons.includes(Globals.STATUS_TEXT.SAVE);

    // positive buttons (right side)
    const positiveButtonsForStatus = this.lisOfActiveButtons.filter(btn =>
      btn !== Globals.STATUS_TEXT.SAVE && btn !== Globals.STATUS_TEXT.CANCEL && btn !== Globals.STATUS_TEXT.REJECT);

    // if there are more than one positive buttons in JSON, the first one will be taken
    const btnToShow = positiveButtonsForStatus[0];
    if (btnToShow && positiveButtonsForStatus.length > 0) {
      this.noActivePositiveBtn = false;
      this.positiveStatusChange = btnToShow;
      this.positiveBtnLabel = Globals.STATUS_BUTTON_LABEL[btnToShow];
    } else {
      this.noActivePositiveBtn = true;
    }
  }

  handleDuplicateButton() {
    if (this.gridMeasureId) {
      const currRoles = this.sessionContext.getCurrUser().roles;
      const allowedRolesForDupicate = this.roleAccessHelper.getRoleAccessDefinitions().duplicateSection.duplicateRoles;
      allowedRolesForDupicate.forEach(allowedRole => {
        if (currRoles.includes(allowedRole)) {
          this.hideDuplicateButtonForRole = false;
          return;
        }
      });
    }
  }

  onClickEvents(action: string) {
    switch (action) {
      case Globals.STATUS_TEXT.QUIT:
        this.clickQuitButton.emit();
        break;
      case Globals.STATUS_TEXT.REJECT:
        this.clickRejectButton.emit();
        break;
      case Globals.STATUS_TEXT.SAVE:
        this.clickSaveButton.emit();
        break;
      case Globals.STATUS_TEXT.APPLY:
        this.clickApplyButton.emit();
        break;
      case Globals.STATUS_TEXT.FORAPPROVAL:
        this.clickForApprovalButton.emit();
        break;
      case Globals.STATUS_TEXT.APPROVE:
        this.clickApprovedButton.emit();
        break;
      case Globals.STATUS_TEXT.REQUEST:
        this.clickRequestButton.emit();
        break;
      case Globals.STATUS_TEXT.RELEASE:
        this.clickReleaseButton.emit();
        break;
      case Globals.STATUS_TEXT.ACTIVE:
        this.clickActivateButton.emit();
        break;
      case Globals.STATUS_TEXT.IN_WORK:
        this.clickInWorkButton.emit();
        break;
      case Globals.STATUS_TEXT.WORK_FINISH:
        this.clickWorkFinishButton.emit();
        break;
      case Globals.STATUS_TEXT.FINISH:
        this.clickFinishButton.emit();
        break;
      case Globals.STATUS_TEXT.CLOSE:
        this.clickCloseButton.emit();
        break;
      case Globals.STATUS_TEXT.DUPLICATE:
        this.clickDuplicateButton.emit();
        break;
    }
  }

}
