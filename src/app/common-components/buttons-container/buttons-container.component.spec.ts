/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { async, ComponentFixture, TestBed, fakeAsync } from '@angular/core/testing';
import { SimpleChange } from '@angular/core';
import { SessionContext } from '../../common/session-context';
import { ButtonsContainerComponent } from './buttons-container.component';
import { Globals } from '../../common/globals';
import { RoleAccessHelperService } from '../../services/jobs/role-access-helper.service';
import { ROLE_ACCESS } from '../../test-data/role-access-definition';

describe('ButtonsContainerComponent', () => {
  let component: ButtonsContainerComponent;
  let fixture: ComponentFixture<ButtonsContainerComponent>;
  let sessionContext: SessionContext;
  let roleAccessHelper: RoleAccessHelperService;

  beforeEach(async(() => {
    sessionContext = new SessionContext();
    roleAccessHelper = new RoleAccessHelperService();

    TestBed.configureTestingModule({
      declarations: [ButtonsContainerComponent],
      providers: [
        SessionContext,
        { provide: RoleAccessHelperService, useValue: roleAccessHelper }
      ]
    })
      .compileComponents().then(() => {
        fixture = TestBed.createComponent(ButtonsContainerComponent);
        component = fixture.componentInstance;
        component.hideDuplicateButtonForRole = false;
      });
  }));

  beforeEach(fakeAsync(() => {
    const roleAcess = ROLE_ACCESS;
    roleAccessHelper.init(roleAcess);
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  //////// negative buttons - left side //////////
  it('should react on quit button click', async(() => {
    spyOn(component, 'onClickEvents').and.callThrough();
    const button = fixture.debugElement.nativeElement.querySelector('button#quit');
    button.click();
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(component.onClickEvents).toHaveBeenCalledWith('quit');
    });

  }));

  it('should react on reject button click', async(() => {
    component.readOnlyForm = false;
    spyOn(component, 'onClickEvents').and.callThrough();
    const button = fixture.debugElement.nativeElement.querySelector('button#reject');
    button.click();
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(component.onClickEvents).toHaveBeenCalledWith('reject');
    });

  }));

  // duplicate
  it('should react on duplicate button click', async(() => {
    component.readOnlyForm = false;
    fixture.detectChanges();
    spyOn(component, 'onClickEvents').and.callThrough();
    const button = fixture.debugElement.nativeElement.querySelector('button#duplicate');
    button.click();
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(component.onClickEvents).toHaveBeenCalledWith('duplicate');
    });

  }));

  //////// positive buttons - right side //////////
  it('should react on save button click', async(() => {

    component.validForSave = true;
    component.validForm = true;
    component.readOnlyForm = false;
    fixture.detectChanges();
    spyOn(component, 'onClickEvents').and.callThrough();
    const button = fixture.debugElement.nativeElement.querySelector('button#save');
    button.click();
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(component.onClickEvents).toHaveBeenCalledWith('save');
    });

  }));

  it('should react on apply button click', async(() => {
    component.positiveStatusChange = 'apply';
    component.validForm = true;
    component.readOnlyForm = false;
    fixture.detectChanges();
    spyOn(component, 'onClickEvents').and.callThrough();
    const button = fixture.debugElement.nativeElement.querySelector('button#statuschange');
    button.click();
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(component.onClickEvents).toHaveBeenCalledWith('apply');
    });

  }));

  it('should react on forapproval button click', async(() => {
    this.gridMeasureStatusId = '1';
    component.positiveStatusChange = 'forapproval';
    component.validForm = true;
    component.readOnlyForm = false;
    fixture.detectChanges();
    spyOn(component, 'onClickEvents').and.callThrough();
    const button = fixture.debugElement.nativeElement.querySelector('button#statuschange');
    button.click();
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(component.onClickEvents).toHaveBeenCalledWith('forapproval');
    });

  }));

  it('should react on approve button click', async(() => {
    this.gridMeasureStatusId = '3';
    component.positiveStatusChange = 'approve';
    component.validForm = true;
    component.readOnlyForm = false;
    fixture.detectChanges();
    spyOn(component, 'onClickEvents').and.callThrough();
    const button = fixture.debugElement.nativeElement.querySelector('button#statuschange');
    button.click();
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(component.onClickEvents).toHaveBeenCalledWith('approve');
    });

  }));



  it('should react on request button click', async(() => {
    this.gridMeasureStatusId = '4';
    component.positiveStatusChange = 'request';
    component.validForm = true;
    component.readOnlyForm = false;
    fixture.detectChanges();
    spyOn(component, 'onClickEvents').and.callThrough();
    const button = fixture.debugElement.nativeElement.querySelector('button#statuschange');
    button.click();
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(component.onClickEvents).toHaveBeenCalledWith('request');
    });

  }));

  it('should react on release button click', async(() => {
    this.gridMeasureStatusId = '5';
    component.positiveStatusChange = 'release';
    component.validForm = true;
    component.readOnlyForm = false;
    fixture.detectChanges();
    spyOn(component, 'onClickEvents').and.callThrough();
    const button = fixture.debugElement.nativeElement.querySelector('button#statuschange');
    button.click();
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(component.onClickEvents).toHaveBeenCalledWith('release');
    });

  }));

  it('should react on activate button click', async(() => {
    this.gridMeasureStatusId = '6';
    component.positiveStatusChange = 'activate';
    component.validForm = true;
    component.readOnlyForm = false;
    fixture.detectChanges();
    spyOn(component, 'onClickEvents').and.callThrough();
    const button = fixture.debugElement.nativeElement.querySelector('button#statuschange');
    button.click();
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(component.onClickEvents).toHaveBeenCalledWith('activate');
    });

  }));

  it('should react on inwork button click', async(() => {
    this.gridMeasureStatusId = '7';
    component.positiveStatusChange = 'inwork';
    component.validForm = true;
    component.readOnlyForm = false;
    fixture.detectChanges();
    spyOn(component, 'onClickEvents').and.callThrough();
    const button = fixture.debugElement.nativeElement.querySelector('button#statuschange');
    button.click();
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(component.onClickEvents).toHaveBeenCalledWith('inwork');
    });

  }));

  it('should react on workfinish button click', async(() => {
    this.gridMeasureStatusId = '8';
    component.positiveStatusChange = 'workfinish';
    component.validForm = true;
    component.readOnlyForm = false;
    fixture.detectChanges();
    spyOn(component, 'onClickEvents').and.callThrough();
    const button = fixture.debugElement.nativeElement.querySelector('button#statuschange');
    button.click();
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(component.onClickEvents).toHaveBeenCalledWith('workfinish');
    });

  }));

  it('should react on finish button click', async(() => {
    this.gridMeasureStatusId = '9';
    component.positiveStatusChange = 'finish';
    component.validForm = true;
    component.readOnlyForm = false;
    fixture.detectChanges();
    spyOn(component, 'onClickEvents').and.callThrough();
    const button = fixture.debugElement.nativeElement.querySelector('button#statuschange');
    button.click();
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(component.onClickEvents).toHaveBeenCalledWith('finish');
    });

  }));

  it('should react on close button click', async(() => {
    this.gridMeasureStatusId = '10';
    component.positiveStatusChange = 'close';
    component.validForm = true;
    component.readOnlyForm = false;
    fixture.detectChanges();
    spyOn(component, 'onClickEvents').and.callThrough();
    const button = fixture.debugElement.nativeElement.querySelector('button#statuschange');
    button.click();
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(component.onClickEvents).toHaveBeenCalledWith('close');
    });

  }));

  it('should set form valid for save', () => {
    component.validForSave = false;
    fixture.detectChanges();
    component.ngOnChanges({
      isValidForSave: new SimpleChange(component.validForSave, true, true)
    });
    fixture.detectChanges();
    expect(component.validForSave).toBeTruthy();
  });

  it('should set form valid', () => {
    component.validForm = false;
    fixture.detectChanges();
    component.ngOnChanges({
      isValidForm: new SimpleChange(component.validForm, true, true)
    });
    fixture.detectChanges();
    expect(component.validForm).toBeTruthy();
  });

  it('should set form in readonly mode', () => {
    component.readOnlyForm = false;
    fixture.detectChanges();
    component.ngOnChanges({
      isReadOnlyForm: new SimpleChange(component.readOnlyForm, true, true)
    });
    fixture.detectChanges();
    expect(component.readOnlyForm).toBeTruthy();
  });

  it('should check all cases and deactivate all negative buttons - no buttons available in the list', async(() => {

    component.gridMeasureStatusId = '20';
    component.readOnlyForm = true;
    fixture.detectChanges();
    component.ngOnChanges({
      isReadOnlyForm: new SimpleChange(component.readOnlyForm, false, true)
    });
    fixture.detectChanges();
    spyOn(component, 'handleNegativeButtonsForStatus').and.callThrough();
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(component.hideRejectButton).toBeTruthy();
    });
  }));

  it('should show only quit button - no other buttons available in the list', async(() => {

    component.gridMeasureStatusId = undefined; // status = nothing
    component.readOnlyForm = true;
    fixture.detectChanges();
    component.ngOnChanges({
      isReadOnlyForm: new SimpleChange(component.readOnlyForm, false, true)
    });
    fixture.detectChanges();
    spyOn(component, 'getButtonsForStatus').and.callThrough();
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(component.gmStatus).toBe(Globals.STATUS.NEW);
    });
  }));

  it('should deactivate save button', async(() => {

    component.gridMeasureStatusId = '20';
    component.readOnlyForm = true;
    fixture.detectChanges();
    component.ngOnChanges({
      isReadOnlyForm: new SimpleChange(component.readOnlyForm, false, true)
    });
    fixture.detectChanges();
    spyOn(component, 'handlePositiveButtonsForStatus').and.callThrough();
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(component.hideSaveButton).toBeTruthy();
    });
  }));

  it('should show for apply button', async(() => {

    component.gridMeasureStatusId = '0';
    component.readOnlyForm = true;
    fixture.detectChanges();
    component.ngOnChanges({
      isReadOnlyForm: new SimpleChange(component.readOnlyForm, false, true)
    });
    fixture.detectChanges();
    spyOn(component, 'handlePositiveButtonsForStatus').and.callThrough();
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(component.positiveStatusChange).toBe('apply');
      expect(component.positiveBtnLabel).toBe(Globals.STATUS_BUTTON_LABEL['apply']);
    });
  }));

  it('should show for approval button', async(() => {

    component.gridMeasureStatusId = '1';
    component.readOnlyForm = true;
    fixture.detectChanges();
    component.ngOnChanges({
      isReadOnlyForm: new SimpleChange(component.readOnlyForm, false, true)
    });
    fixture.detectChanges();
    spyOn(component, 'handlePositiveButtonsForStatus').and.callThrough();
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(component.positiveStatusChange).toBe('forapproval');
      expect(component.positiveBtnLabel).toBe(Globals.STATUS_BUTTON_LABEL['forapproval']);
    });
  }));

  it('should show for approve button', async(() => {

    component.gridMeasureStatusId = '3';
    component.readOnlyForm = true;
    fixture.detectChanges();
    component.ngOnChanges({
      isReadOnlyForm: new SimpleChange(component.readOnlyForm, false, true)
    });
    fixture.detectChanges();
    spyOn(component, 'handlePositiveButtonsForStatus').and.callThrough();
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(component.positiveStatusChange).toBe('approve');
      expect(component.positiveBtnLabel).toBe(Globals.STATUS_BUTTON_LABEL['approve']);
    });
  }));

  it('should show for request button', async(() => {

    component.gridMeasureStatusId = '4';
    component.readOnlyForm = true;
    fixture.detectChanges();
    component.ngOnChanges({
      isReadOnlyForm: new SimpleChange(component.readOnlyForm, false, true)
    });
    fixture.detectChanges();
    spyOn(component, 'handlePositiveButtonsForStatus').and.callThrough();
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(component.positiveStatusChange).toBe('request');
      expect(component.positiveBtnLabel).toBe(Globals.STATUS_BUTTON_LABEL['request']);
    });
  }));

  it('should show for release button', async(() => {

    component.gridMeasureStatusId = '5';
    component.readOnlyForm = true;
    fixture.detectChanges();
    component.ngOnChanges({
      isReadOnlyForm: new SimpleChange(component.readOnlyForm, false, true)
    });
    fixture.detectChanges();
    spyOn(component, 'handlePositiveButtonsForStatus').and.callThrough();
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(component.positiveStatusChange).toBe('release');
      expect(component.positiveBtnLabel).toBe(Globals.STATUS_BUTTON_LABEL['release']);
    });
  }));

  it('should show for activate button', async(() => {

    component.gridMeasureStatusId = '6';
    component.readOnlyForm = true;
    fixture.detectChanges();
    component.ngOnChanges({
      isReadOnlyForm: new SimpleChange(component.readOnlyForm, false, true)
    });
    fixture.detectChanges();
    spyOn(component, 'handlePositiveButtonsForStatus').and.callThrough();
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(component.positiveStatusChange).toBe('activate');
      expect(component.positiveBtnLabel).toBe(Globals.STATUS_BUTTON_LABEL['activate']);
    });
  }));

  it('should show for inwork button', async(() => {

    component.gridMeasureStatusId = '7';
    component.readOnlyForm = true;
    fixture.detectChanges();
    component.ngOnChanges({
      isReadOnlyForm: new SimpleChange(component.readOnlyForm, false, true)
    });
    fixture.detectChanges();
    spyOn(component, 'handlePositiveButtonsForStatus').and.callThrough();
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(component.positiveStatusChange).toBe('inwork');
      expect(component.positiveBtnLabel).toBe(Globals.STATUS_BUTTON_LABEL['inwork']);
    });
  }));

  it('should show for workfinish button', async(() => {

    component.gridMeasureStatusId = '8';
    component.readOnlyForm = true;
    fixture.detectChanges();
    component.ngOnChanges({
      isReadOnlyForm: new SimpleChange(component.readOnlyForm, false, true)
    });
    fixture.detectChanges();
    spyOn(component, 'handlePositiveButtonsForStatus').and.callThrough();
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(component.positiveStatusChange).toBe('workfinish');
      expect(component.positiveBtnLabel).toBe(Globals.STATUS_BUTTON_LABEL['workfinish']);
    });
  }));

  it('should show for finish button', async(() => {

    component.gridMeasureStatusId = '9';
    component.readOnlyForm = true;
    fixture.detectChanges();
    component.ngOnChanges({
      isReadOnlyForm: new SimpleChange(component.readOnlyForm, false, true)
    });
    fixture.detectChanges();
    spyOn(component, 'handlePositiveButtonsForStatus').and.callThrough();
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(component.positiveStatusChange).toBe('finish');
      expect(component.positiveBtnLabel).toBe(Globals.STATUS_BUTTON_LABEL['finish']);
    });
  }));

  it('should show for close button', async(() => {

    component.gridMeasureStatusId = '10';
    component.readOnlyForm = true;
    fixture.detectChanges();
    component.ngOnChanges({
      isReadOnlyForm: new SimpleChange(component.readOnlyForm, false, true)
    });
    fixture.detectChanges();
    spyOn(component, 'handlePositiveButtonsForStatus').and.callThrough();
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(component.positiveStatusChange).toBe('close');
      expect(component.positiveBtnLabel).toBe(Globals.STATUS_BUTTON_LABEL['close']);
    });
  }));


});
