/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

import { TestBed } from '@angular/core/testing';
import { environment } from './environment.prod';


describe('Enviroment: Production', () => {


  beforeEach(() => {

    TestBed.configureTestingModule({
      declarations: [environment]
    });
  });

  it('should create an instance', () => {
    expect(environment.production).toBeTruthy();
  });
});

